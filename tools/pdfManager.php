<?php

class pdfManager extends tFPDF
{
	// A4 is 210 by  297

	var $showGuides = false;
	var $width = 297;
	var $height = 210;

	var $marginX = 5;
	var $marginY = 5;
	var $cellwidth = 0;
	var $cellheight = 0;

	var $pageNo = 0;
	var $pageName = "WorkSheet";
	var $pageType = 'worksheet';
	var	$bold = .5;
	var $normal = .2;
	var $runID = ' ';

	var $howManySquares = Utility::STANDARD_LINE_LENGTH; // e.g. 32
	var $howManyLines = Utility::STANDARD_LINE_QTY; // e.g. 4

	var $grade = '7'; // not used

	// Only 2 types:
	//  3 X 5  (Early Primary)
	//  6 X 6  (Others)

	var $howManyQuestionSets;
	var $howManyQuestionsPerSet;

	var $numbersSetOne;
	var $numbersSetTwo;
	var $numbersSetThree;
	var $numbersSetFour;
	var $numbersSetFive;
	var $numbersSetSix;

	var $ruleSetOne;
	var $ruleSetTwo;
	var $ruleSetThree;
	var $ruleSetFour;
	var $ruleSetFive;
	var $ruleSetSix;


	var $scambledletters;
	var $scambledletterArray;
	var $myLetterNumberArr;
	var $numberArrayAllSets;
	var $associatedletters;
	var $associatedletterArray;
	var $combinedLetterArray;
	var $inputLines;
	var $inputTextString;
	var $arrayValueCount;
	var $key;
	var $numberValue;
	var $currentY;
	var $NoOfStudents;
	var $headerimage;

	var $ruleHeaderArray;

	var $nobold;

	public function __construct($orientation='P', $unit='mm', $size='A4')
	{
		parent::__construct($orientation, $unit, $size);

		date_default_timezone_set('Australia/Sydney');

		$this->SetMargins(8.3, 10, 5.0);
		$this->SetFillColor(255);
		$this->SetTextColor(0);
		$this->SetDisplayMode (55, 'continuous');
        $this->SetDefaultFont();
		$this->SetFont($this->get_font(), '', 10);

		$this->SetAutoPageBreak(true , 1.25);

		$this->ruleHeaderArray = array();
	}

	function GetNumberOfQuestions()
	{
		return $this->howManyQuestionSets * $this->howManyQuestionsPerSet;
	}
	function SetInputFieldsforHeaders($seltyp, $selnum, $randomNum, $abstractCharArray, $cnt)
	{
		if (count($this->ruleHeaderArray) == 0)
		{
			for ($ix=0; $ix < $cnt; $ix++)
			{
				$this->ruleHeaderArray[] = "temp";
				//LogLine("Hdr: ".$seltyp[$ix], $this->ruleHeaderArray[$ix]);
			}
		}

		for ($ix=0; $ix < $cnt; $ix++)
		{
			$this->ruleHeaderArray[$ix] = Math_Logic_Set_Heading($seltyp[$ix], $selnum[$ix], $randomNum[$ix], $abstractCharArray[$ix]);

			//LogLine("Hdr: ".$seltyp[$ix], $this->ruleHeaderArray[$ix]);
		}
	}

	function SetCellWidthHeight($w, $h)
	{
		$this->cellwidth = $w;
		$this->cellheight = $h;
	}

    function get_font()
    {
        return "DejaVu";
    }

    function SetDefaultFont()
    {
        // Add a Unicode font (uses UTF-8)
           $this->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
	    //	$this->SetFont('DejaVu','',14);

        // $this->AddFont('bookman','','bookman-old-style.ttf',true);
    }

	function Header()
	{
		date_default_timezone_set('Australia/Sydney');

		$this->SetFont($this->get_font(),'',30);  // was B

		if ($this->showGuides)
		{
			$pos = 0;
			for ($i = $this->marginX; $i < $this->width; $i+=20)
			{
				$this->SetX($i);
				$this->Cell(10,10,'X',1,0,'C');
			}
		}

		$this->SetY(0);
		$this->SetX(0);

		$this->SetFont($this->get_font(),'',12); // was B

		if (!empty($this->headerimage))
		{
			$filepath = $this->headerimage;
			$this->Image('uploads/'.$filepath,0,0,130);
		}
		else
		{
			$this->Image('gfx/print/logo.png',0,0,40);

			$this->SetY(5);
			$this->SetX(90);
			$this->Cell(40, 10 ,'School and Class',0,0,'C');
		}

		$this->SetY(5);
		$this->SetX(155);
		if ($this->pageType=='worksheet')
        {
            // Needs to trim for UTF-8
            $temp = ' Name: '.trim($this->pageName).',  No. '.$this->pageNo.', Grade: '.$this->grade;
			$this->Cell(50,10,$temp,0,0,'C');
        }
		elseif ($this->pageType=='summary')
			$this->Cell(50,10,' Sheet Summary: '.$this->NoOfStudents .' students,  '.'Grade: '.$this->grade,0,0,'C');

		$this->SetY(5);
		$this->SetX(235);
		$this->Cell(50,10,'Date: '.date('d-m-Y'),0,0,'C');

		$starty = 25;

		if ($this->pageType=="summary")
		{
			$this->SetY($starty);
			$this->SetX(10);
			$this->Cell(30,8,'Worksheet No.',0,0,'L');
			$this->Cell(120,8,'Question/Answer',0,0,'C');
			$this->SetY($starty+10);
		}
		elseif  ($this->pageType=='worksheet')
		{
			$startAnswerY = ($this->howManyQuestionSets == 6 ? 200 : 185);

			$this->SetY($startAnswerY);
			$this->SetX(100);
			$this->Cell(50,10,'Your Answer:',0,0,'C');

			$this->SetLineWidth(0.2);
			$this->Line(110,$startAnswerY + 9,285,$startAnswerY + 9);
		}
	}

	function SetTitleHeadings($hdrstarty, $posix=0)
	{
		$cnt = count($this->ruleHeaderArray);
		for ($ix=0; $ix < $cnt; $ix++)
		{
			LogLine("Hdr-array: ", $this->ruleHeaderArray[$ix]);
		}

		$this->SetY($hdrstarty+7);
		$this->SetX(30);
		$this->Cell(50,10,$this->ruleHeaderArray[$posix],0,0,'C');

		$this->SetY($hdrstarty + 7);
		$this->SetX(120);
		$this->Cell(50,10,$this->ruleHeaderArray[$posix+1],0,0,'C');

		$this->SetY($hdrstarty + 7);
		$this->SetX(205);
		$this->Cell(50,10,$this->ruleHeaderArray[$posix+2],0,0,'C');
	}

	function Footer()
	{
		if ($this->showGuides)
		{
			$this->Ln(20);
			$pos = 0;
			for ($i = $this->marginX; $i <  $this->width; $i+=20)
			{
				$this->SetX($i);
				$this->Cell(10,10,'X',1,0,'C');
			}
		}
	}


		// **********************************************************
	//	START new PAGE
	// **********************************************************
	public function StartNewPage($run, $pageNumber=0, $studentname, $hdrimage, $sets, $questionPerSet)
	{
		$this->pageType='worksheet';
		$this->howManyQuestionSets = $sets;
		$this->howManyQuestionsPerSet = $questionPerSet;

		if ($pageNumber != 0)
		{
			$this->pageNo = $pageNumber;
		}

		if (!empty($studentname))
		{
			$this->pageName = $studentname;
		}

		$this->headerimage = $hdrimage;
		$this->runID = $run;

		$this->SetCellWidthHeight(15, 12);
		$this->SetFont($this->get_font(), '', 12);
		$this->AddPage('L');
	}

	// **********************************************************
	//	TEST Function
	//
	// Print a single row of 3 Sets of 5 for Testing
	// **********************************************************
	function PrintWorksheetPageMathsTest($QuestionAnswerArray, $inputSelectArray, $inputOfArray, $randomNumArray, $inputAbstractCharacterArray, $ix)
	{
		// $num3 = Utility::SELECTION_KEY_LENGTH * 10;
		// $den3 = 100;

		// $common_factor = Utility::gcd($num3, $den3);

		$this->CreateUniqueNumbers1($inputSelectArray, $inputOfArray,  $randomNumArray);
		$this->ApplyRules1($inputSelectArray, $inputOfArray,  $randomNumArray);

		//combine numbers into a combined set for printing on the current page

		$this->numberArrayAllSets = array_merge (
				(array)$this->ruleSetOne, (array)$this->ruleSetTwo, (array) $this->ruleSetThree );

		$this->CreateScrambledLetterArray(($this->runID =='1' ? "" : $QuestionAnswerArray[utilGetQuestionIx($this->pageNo)]));
		$this->AssociateNumbersAndLetters();

		$start = 15;
		if ($ix==1)
			$start = 55;
		else if ($ix==2)
			$start = 95;
		else if ($ix==3)
			$start = 135;

		$this->SetTitleHeadings($start);
		$this->SetY($start + 20);

		$this->MathSetLineAll(
			$this->numbersSetOne, $this->numbersSetTwo, $this->numbersSetThree,
			 $inputSelectArray,
			 $inputAbstractCharacterArray,
			  0 );

		$this->PrintSetNumbersA();



		// $this->PrintSetLetters("A", 0.75);
	}

	// **********************************************************
	//	Full Page Create / Print
	// **********************************************************
	function PrintWorksheetPage($QuestionAnswerArray, $inputSelectArray, $inputOfArray, $randomNumArray, $inputAbstractCharacterArray)
	{
		// Were ever you see only SelectArray and NumberOf Array this is the Early Primary way of doing things
		//  (The first version)
		// This works well but for the higher grades we need to expand to include the full 5 dropdowns possible
		// for each set

		$this->CreateUniqueNumbers1($inputSelectArray, $inputOfArray,  $randomNumArray);
		if ($this->howManyQuestionSets == 6)
			$this->CreateUniqueNumbers2($inputSelectArray, $inputOfArray,  $randomNumArray);

		$this->ApplyRules1($inputSelectArray, $inputOfArray,  $randomNumArray);
		if ($this->howManyQuestionSets == 6)
			$this->ApplyRules2($inputSelectArray, $inputOfArray,  $randomNumArray);

		//combine numbers into a combined set for printing on the current page

			// echo "<pre>-";
			// print_r($this->ruleSetOne);
			// print_r($this->ruleSetTwo);
			// print_r($this->ruleSetThree);
			// exit();

		if ($this->howManyQuestionSets == 3)
			$this->numberArrayAllSets = array_merge (
				(array)$this->ruleSetOne, (array)$this->ruleSetTwo, (array) $this->ruleSetThree );
		else
			$this->numberArrayAllSets = array_merge (
				(array)$this->ruleSetOne, (array)$this->ruleSetTwo, (array) $this->ruleSetThree,
				(array)$this->ruleSetFour, (array)$this->ruleSetFive, (array) $this->ruleSetSix );

		$this->CreateScrambledLetterArray(($this->runID =='1' ? "" : $QuestionAnswerArray[utilGetQuestionIx($this->pageNo)]));
		$this->AssociateNumbersAndLetters();

		$this->SetTitleHeadings(9);
		$this->SetY(29);

		$this->MathSetLineAll($this->numbersSetOne, $this->numbersSetTwo, $this->numbersSetThree,
			$inputSelectArray, $inputAbstractCharacterArray, 0 );

		$this->PrintSetNumbersA();
		$this->PrintSetLetters("A", 0.75);

		if ($this->howManyQuestionSets == 6)
		{
			$this->SetTitleHeadings(57, 3);
			$this->SetY(77);

			$this->MathSetLineAll($this->numbersSetFour, $this->numbersSetFive, $this->numbersSetSix,
				$inputSelectArray, $inputAbstractCharacterArray, 3 );

			$this->PrintSetNumbersB();
			$this->PrintSetLetters("B", 0.75);
		}

		$this->SetCellWidthHeight(8, 8);
		$this->SetFont($this->get_font(), '', 10);

		$this->OutputTextMessages($QuestionAnswerArray, $this->pageNo);
	}

	// **********************************************************
	//	The Summary Page
	// **********************************************************

	public function PrintSummaryPage($students, $questionsAnswers, $studentnames, $hdrimage)
	{
		$this->headerimage = $hdrimage;
		$this->pageType='summary';
		$this->NoOfStudents = $students;
		$this->SetCellWidthHeight(8, 8);
		$this->SetFont($this->get_font(), '', 10);
		$this->AddPage('L');

		// print one line for each student

		$questions = 0;
		$studentix = 0;

		for ($i = 0; $i < $students  ; $i++)
		{
			$num = $i+1;

			$this->NextLine();

			$this->SetX(10);
			$this->Cell(10,6,$num,1,0,'L');
			$this->MultiCell(200,6, $studentnames[$studentix], 1,'L');

			$this->SetX(20);
			$this->MultiCell(200,6,$questionsAnswers[$questions],1,'L');

			$this->SetX(20);
			$this->MultiCell(200,6,$questionsAnswers[$questions + 1],1,'L');
			$questions = $questions + 2;
			$studentix = $studentix + 1;

			$this->currentY = $this->GetY();

			if ($this->currentY > 200)
				$this->AddPage('L');
		}
	}


	//  Print Line of each Set

	function MathSetLineAll($arrayData1, $arrayData2, $arrayData3, $selArray, $absArray, $startIx)
	{
		$this->SetX(15);

		$this->MathSetLine($arrayData1, 1, $selArray[$startIx], $absArray[$startIx]);
		$this->Cell($this->cellwidth);

		$this->MathSetLine($arrayData2, 2, $selArray[$startIx+1], $absArray[$startIx+1]);
		$this->Cell($this->cellwidth);

		$this->MathSetLine($arrayData3, 3, $selArray[$startIx+2], $absArray[$startIx+2]);
		$this->NextLine();
	}

	//  Print a single Set Line

	function MathSetLine($arrayData, $line, $selKey, $abstractChar)
	{
		LogLine("MathSetLine-XTY-", "Start ".$line."<<>>".$selKey."<>".count($arrayData));

		$saveY = $this->GetY();

		for($i=0; $i<count($arrayData); $i++)
		{
			$txt = Math_Logic_Get_Formatted_Math_Question($arrayData[$i], $selKey, $abstractChar);

			// This is a test of multi line formula printing
			// ==============================================
			// if ($i == 2)
			// {
			// 	$this->Cell($this->cellwidth, $this->cellheight, "" , 1, 0, 'C', true);
			// 	$saveX = $this->GetX();
			// 	$this->SetX($this->GetX()-$this->cellwidth);
			// 	// Testing:
			// 	$txt = "3+".$txt."\n/123";
			// 	$this->MultiCell($this->cellwidth, $this->cellheight/2, $txt, 0,'C');

			// 	$this->SetXY($saveX, $saveY);
			// }
			// else
			// {

				// Normal single cell printing
				$this->Cell($this->cellwidth, $this->cellheight, $txt , 1, 0, 'C', true);

			//}


			//$pdf->Cell(.4);   //GIVES A DOUBLE LINE
		}
	}

	function SetDuplicateLineWidth($duplicateCount)
	{
		if ($duplicateCount > 1)   //make the duplicates bold
			{$this->SetLineWidth($this->bold);}
		else
			{ $this->SetLineWidth($this->normal);}
	}

	function NextLine($space=0)
	{
		if ($space==0)
			$this->Ln();  // the default line
		else
			$this->Ln($space);  // in Units

		$this->SetX(15);
		$this->SetLineWidth($this->normal);
		$this->SetTextColor(0);
	}

	public function SetGrade($gradein)
	{
		$this->grade = $gradein;
	}


	// **********************************************************
	//	create 3 SETS OF TEN UNIQUE NUMBERS
	// **********************************************************
	function CreateUniqueNumbers1($seltyp, $selnum, $randomNums)
	{
		$this->CreateUniqueNumbersWorker("A",
			$seltyp[0], $selnum[0], $randomNums[0],
			$seltyp[1], $selnum[1], $randomNums[1],
			$seltyp[2], $selnum[2], $randomNums[2]);
	}
	function CreateUniqueNumbers2($seltyp, $selnum, $randomNums)
	{
		$this->CreateUniqueNumbersWorker("B",
			$seltyp[3], $selnum[3], $randomNums[3],
			$seltyp[4], $selnum[4], $randomNums[4],
			$seltyp[5], $selnum[5], $randomNums[5]);
	}


	// **********************************************************
	//	create SETS OF TEN UNIQUE NUMBERS
	// **********************************************************
	function CreateUniqueNumbersWorker($line,
		$seltyp1, $selnum1, $rndNums1,
		$seltyp2, $selnum2, $rndNums2,
		$seltyp3, $selnum3, $rndNums3)
	{
		$inputSetOne = new number_set($this->howManyQuestionsPerSet);
		$inputSetTwo = new number_set($this->howManyQuestionsPerSet);
		$inputSetThree = new number_set($this->howManyQuestionsPerSet);

		$inputSetOne->set_unique_number_array($seltyp1, $selnum1, $rndNums1);
		if ($line == "A")
			$this->numbersSetOne = $inputSetOne->get_unique_number_array($this->runID);
		else
			$this->numbersSetFour = $inputSetOne->get_unique_number_array($this->runID);

		$inputSetTwo->set_unique_number_array($seltyp2, $selnum2, $rndNums2);
		if ($line == "A")
			$this->numbersSetTwo = $inputSetTwo->get_unique_number_array($this->runID);
		else
			$this->numbersSetFive = $inputSetTwo->get_unique_number_array($this->runID);

		$inputSetThree->set_unique_number_array($seltyp3, $selnum3, $rndNums3);
		if ($line == "A")
			$this->numbersSetThree = $inputSetThree->get_unique_number_array($this->runID);
		else
			$this->numbersSetSix = $inputSetThree->get_unique_number_array($this->runID);
	}

	// **********************************************************
	//	APPLY RULES
	// **********************************************************

	function ApplyRules1($seltyp, $selnum, $randomNums)
	{
		$this->ApplyRulesWorkerA($seltyp[0], $selnum[0], $seltyp[1], $selnum[1], $seltyp[2], $selnum[2], $randomNums[0], $randomNums[1], $randomNums[2]);
	}
	function ApplyRules2($seltyp, $selnum, $randomNums)
	{
		$this->ApplyRulesWorkerB($seltyp[3], $selnum[3], $seltyp[4], $selnum[4], $seltyp[5], $selnum[5], $randomNums[3], $randomNums[4], $randomNums[5]);
	}

	function ApplyRulesWorkerA($seltyp1, $selnum1, $seltyp2, $selnum2, $seltyp3, $selnum3, $randomNum1, $randomNum2, $randomNum3)
	{
		$tempRules = new number_rules();
		$tempRules->set_rule_answers($this->numbersSetOne, $seltyp1, $selnum1, $randomNum1);
		$this->ruleSetOne = $tempRules->get_rule_answers($this->runID);

		$tempRules = new number_rules();
		$tempRules->set_rule_answers($this->numbersSetTwo, $seltyp2, $selnum2, $randomNum2);
		$this->ruleSetTwo = $tempRules->get_rule_answers($this->runID);

		$tempRules = new number_rules();
		$tempRules->set_rule_answers($this->numbersSetThree, $seltyp3, $selnum3, $randomNum3);
		$this->ruleSetThree = $tempRules->get_rule_answers($this->runID);
	}

	function ApplyRulesWorkerB($seltyp1, $selnum1, $seltyp2, $selnum2, $seltyp3, $selnum3, $randomNum1, $randomNum2, $randomNum3)
	{
		$tempRules = new number_rules();
		$tempRules->set_rule_answers($this->numbersSetFour, $seltyp1, $selnum1, $randomNum1);
		$this->ruleSetFour = $tempRules->get_rule_answers($this->runID);

		$tempRules = new number_rules();
		$tempRules->set_rule_answers($this->numbersSetFive, $seltyp2, $selnum2, $randomNum2);
		$this->ruleSetFive = $tempRules->get_rule_answers($this->runID);

		$tempRules = new number_rules();
		$tempRules->set_rule_answers($this->numbersSetSix, $seltyp3, $selnum3, $randomNum3);
		$this->ruleSetSix = $tempRules->get_rule_answers($this->runID);
	}

	// **********************************************************
	//	SCRAMBLED LETTER ARRAY
	// **********************************************************

	function CreateScrambledLetterArray($questionString)
	{
		// LogLine("CreateScrambledLetterArray", $questionString);

		$this->scambledletters = new letters();
		$this->scambledletters->set_letters($questionString, $this->GetNumberOfQuestions());
		$this->scambledletterArray = $this->scambledletters->get_letters($this->runID);
	}

	// **********************************************************
	//	ASSOCIATE NUMBERS AND LETTERS
	// **********************************************************
	function AssociateNumbersAndLetters()
	{
		$this->nobold = (!empty($_POST['literacy']) && $_POST['literacy'] == "01");

		$this->myLetterNumberArr = new combine_letters_nums($this->scambledletterArray, $this->numberArrayAllSets, $this->nobold);
		$this->combinedLetterArray = $this->myLetterNumberArr->get_array();
	}

	function PrintSetNumbersA()
	{
		$this->PrintSetNumbers($this->ruleSetOne, 0.75);
		$this->Cell($this->cellwidth);
		$this->PrintSetNumbers($this->ruleSetTwo, 0.75);
		$this->Cell($this->cellwidth);
		$this->PrintSetNumbers($this->ruleSetThree, 0.75);
		$this->NextLine();
	}

	function PrintSetNumbersB()
	{
		$this->PrintSetNumbers($this->ruleSetFour, 0.75);
		$this->Cell($this->cellwidth);
		$this->PrintSetNumbers($this->ruleSetFive, 0.75);
		$this->Cell($this->cellwidth);
		$this->PrintSetNumbers($this->ruleSetSix, 0.75);
		$this->NextLine();
	}

	function PrintSetNumbers($numbervalSet, $shrink){

		for($i=0; $i<count($numbervalSet); $i++)
		{
			if ($this->runID == '3') {
				$this->Cell($this->cellwidth, $this->cellheight * $shrink,' ', 1, 0, 'C', true);
			}
			else
			{
				$txt = 	trim($numbervalSet[$i]);

				LogEntry("null",$txt);

                $this->Cell($this->cellwidth, $this->cellheight * $shrink, $txt, 1, 0, 'C', true);
			}
		}
	}

	function PrintSetLetters($line, $shrink)
	{
		// Only print one complete row. Either A first row or B second row
		$j = 1;
		$start = ($line == "A" ? 0 : $this->howManyQuestionsPerSet * Utility::STANDARD_SETS_PER_ROW);
		$end = ($line == "A" ? $this->howManyQuestionsPerSet * Utility::STANDARD_SETS_PER_ROW : 9999);

		foreach ($this->combinedLetterArray as $key=>$values)
		 {
			if ($j > $start)
			{
				///$txt = iconv('utf-8', 'cp1252', trim($values->letter));
				$txt = trim($values->letter);

				// iconv(mb_detect_encoding($text, mb_detect_order(), true), "UTF-8", $text);

				LogEntry("[LET]",$txt);

				$this->Cell($this->cellwidth, $this->cellheight * $shrink, $txt, 1, 0, 'C', true);

				if ($this->howManyQuestionsPerSet == 10 &&
					$j % 10 == 0)
					$this->Cell($this->cellwidth);

				if ($this->howManyQuestionsPerSet == 5 &&
					$j % 5 == 0)
					$this->Cell($this->cellwidth);
			}

			$j++;

			if ($j > $end)
				break;
		}
	}

	// **********************************************************
	//	INPUT TEXT MESSAGE LINES
	// **********************************************************

	function OutputTextMessages($questionAnswerArr, $pageNumb)
	{
		$this->inputLines = new text_input();
		$this->inputLines->set_input($questionAnswerArr, $pageNumb, $this->howManySquares, $this->howManyLines);

		$this->NextLine($this->cellheight * 2.2);

		// get duplicate value count for each number and put values in an array

		$this->arrayValueCount = array();

		if ($this->howManyQuestionSets == 100)  // should be 3
			(array_count_values($this->numberArrayAllSets));

		$space = ($this->howManyQuestionSets == 6 ? 1.45 : 2.5);

		for($ix=0; $ix < $this->howManyLines; $ix++)
		{
			$this->writeOneLine($ix, $this->howManySquares);

			$this->NextLine($this->cellheight * $space);
		}
	}

	function writeOneLine($lineNum, $numOfBoxes)
    {
		$this->inputTextString = $this->inputLines->get_input($lineNum, $this->runID);
		$this->inputTextString = trim ($this->inputTextString);

		LogLine("writeOneLine ".$this->runID." ".$lineNum." ".$numOfBoxes, $this->inputTextString);

		for($i=0;$i<$numOfBoxes; $i++)
		{

			$txt = mb_substr($this->inputTextString,$i,1,"UTF-8");

			LogEntry("null",$txt);

            if ($this->runID == '3')
            {
				$found = false;             // check if the text input is included in valid chars set
				foreach ($this->combinedLetterArray as $key=>$values)
                {
					if ($this->nobold)
						{
                            if ( mb_strtolower($txt) == mb_strtolower($values->letter) )
								$found = true;
						}
					else
						{
							if ( mb_strtolower($txt) ==  mb_strtolower($key) )
								$found = true;
						}
				}

				//set square to blank unless char not in 30 char set

				$this->Cell($this->cellwidth, $this->cellheight, ($found ? ' ' : $txt), 1, 0, 'C', true);
			}
			else
			{
				$this->Cell($this->cellwidth, $this->cellheight, $txt, 1, 0, 'C', true);
			}
		}

		$this->NextLine();

		for($i=0;$i<$numOfBoxes; $i++)
		{
			$this->key = mb_substr($this->inputTextString, $i, 1, "UTF-8");
			$this->numberValue = $this->myLetterNumberArr->get_number($this->key);

			$txt = trim($this->numberValue);

			// Answer is always n the correct format for printing.  No need for...
			// $txt = Math_Logic_Get_Formatted_Math_Question($this->numberValue, $selKey);

			LogEntry("key<>num",$this->key."<>".$txt);

			//$txt = iconv('utf-8', 'cp1252', trim($this->numberValue));

			if ($this->nobold || $this->howManyQuestionSets == 3)
				$this->SetDuplicateLineWidth(1);
			else
			{
				if (in_array($this->numberValue, $this->arrayValueCount))
					$this->SetDuplicateLineWidth($this->arrayValueCount[$this->numberValue]);
			}

			// // Testing
			// if ($i % 10 == 0)
			// 	$this->Cell($this->cellwidth, $this->cellheight, '√'.$txt , 1, 0, 'C', true);
			// else if ($i % 5 == 0)
			// 	$this->Cell($this->cellwidth, $this->cellheight, '1/'.$txt , 1, 0, 'C', true);
			// else

				$this->Cell($this->cellwidth, $this->cellheight, $txt , 1, 0, 'C', true);
		}
	}
}
?>

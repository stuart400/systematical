echo on

set todir="D:\vs_web\CodebreakerPHP_Front_End\"

echo ==========================================================================
echo ==== %todir% 
echo ==========================================================================

copy *.ini %todir%\*.*
copy *.php %todir%\*.*

copy classes\*.* %todir%\classes\*.*
copy tools\*.* %todir%\tools\*.*

pause
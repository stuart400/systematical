echo on

set todir="D:\vs_web\CodebreakerPHP_Front_End\"

echo ==========================================================================
echo ==== %todir% 
echo ==========================================================================

copy *.ini %todir%\*.*
copy *.php %todir%\*.*

xcopy classes %todir%\classes /s
xcopy css %todir%\css /s
xcopy gfx %todir%\gfx /s
xcopy img %todir%\img /s
xcopy tools %todir%\tools /s
xcopy inputfiles %todir%\inputfiles /s

pause
<?php

require_once('BOM/Util.php');
require_once('BOM/Handler.php');
require_once('BOM/StreamFilter.php');

use duncan3dc\Bom\Util;

///
//   Create HTML controls 
//
///

function GetFileText($uploadfile, $type)
{
	$old = getcwd();

	chdir('uploads');

	$curDir = getcwd();
	$myFile = $curDir.'/'.$uploadfile.$type.'_up.txt';
	
	$topicLines  = file($myFile, FILE_SKIP_EMPTY_LINES);

	chdir($old);

	return $topicLines;
}

function get_question_text_control($fileid)
{
	$part1 = "";
	$txt1 = "";
	$txt2 = "";
	if (!is_null($fileid) && strlen($fileid) > 1 )
	{
		$data = GetFileText($fileid, "question");

		$ix = 1;
		foreach ($data as $line_num => $line) {

			$line = Util::removeBom($line);

			if (!is_null($line) && mb_strlen($line) > 2)
			{
				$line = StripLeadingNumber($line);

				$parts = formatQuestions ($line); 
				$afterLen = GetCharacterLengthArray($parts); 

				$bef = mb_substr($line, 0, $afterLen);
				$aft = mb_substr($line, $afterLen);

				$txt1 .=  $ix.". ".$bef; 
				$txt1 .=  "<span style=\"color:red\">".$aft."</span><br/>";

				$txt2 .=  $ix.". ".trim($line)."\n"; 

				$ix++;
			}
        }

		//$txt1 = "<span style=\"color:red;\">Hello</span>";
		//$txt2 = "Hello";
		$part1 =  "<div style=\" overflow:auto; height:100px;\">".$txt1."</div>";	
	}

	return "<textarea rows =\"10\" cols=\"100\" name=\"question\" id=\"question\">".$txt2."</textarea>".$part1;
}

function get_student_text_control($fileid)
{
	$txt = "";
	if (!is_null($fileid) && strlen($fileid) > 1)
	{
		$data = GetFileText($fileid, "name");

		$ix = 1;
		foreach ($data as $line_num => $line) {

			$line = Util::removeBom($line);

			if (!is_null($line) && mb_strlen($line) > 2)
			{
				$line = StripLeadingNumber($line);				
				$txt .=  $ix.". ".trim($line)."\n"; 
				$ix++;
			}
        }
	}
	return "<textarea rows =\"10\" cols=\"100\" name=\"studentnames\" id=\"studentnames\">".$txt."</textarea> ";
}

function get_header_text($all)
{
$txt1 = '&nbsp;
<span style="font-size: 14.0pt;margin-left: -7px;"><b>S</b>end <b>y</b>our <b>st</b>udents <b>e</b>ducational <b>m</b>essages <b>a</b>nd <b>t</b>asks <b>i</b>n <b>c</b>ode <b>a</b>nd <b>l</b>anguage</span></p>
<p><span style="font-size:13.0pt"></span></p>
<p><b><span style="font-size:13.0pt; ">A creative way for teachers to</span></b><b><span lang="EN-US" style="font-size:13.0pt;color:black"> combine maths and literacy into a single fun-sheet.</span></b><span class="text-warning1"><b><span lang="EN-US" style="color:black"><o:p></o:p></span></b></span></p>
<p><span style="font-size:12.0pt; ">All students enjoy solving puzzles. When they are personal, they are delighted.</span>';

$txt2 = 
'<p><span style="font-size:12.0pt; ">Systematical enables you to easily create delightful “fun-sheets”, where students complete some maths to reveal a code used to decipher their message.
<o:p></o:p></span></p>
<p><span style="font-size:12.0pt; ">These fun-sheets provide an easy and self-checking way to have your students practise maths techniques with the added incentive of them also using important literacy skills.
<o:p></o:p></span></p>
<p><span style="font-size:12.0pt; ">Each fun-sheet is unique and by catering both the maths and the message to specific individual needs, Systematical will enhance your relationship with each of your students.
</span><span style="font-size:12.0pt; ;color:windowtext"><o:p></o:p></span></p>
<p><span style="font-size:12.0pt; ">Systematical fun-sheets are generated as a pdf document. They can be printed for class, homework, excursions and any creative way you may wish to explore learning activities and co-curricular events with your
 students, including drop-everything-and-read programs.<o:p></o:p></span></p>
<p><span style="font-size:12.0pt; ">Messages you write to your students can be written in English as well as other languages.<o:p></o:p></span></p>
<p><span style="font-size:12.0pt; ">Follow the instructions on the left to create these unique fun-sheets for your students.<o:p></o:p></span></p>
	';

	if ($all == "morehelp")
		return $txt1."</p>".$txt2;
	else
//		return $txt1."&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"index.php?more=1\">more ...</a></p>";  //<input type="submit" name="submitbtn" value="GO!" 
		return $txt1."&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"submit\" name=\"morehelp\" value=\"...\"/></p>";  //<input type="submit" name="morehelp" value="..."/> 

	//return "";
	 //return '<p style="padding-left:15px;">Welcome to the first personalised maths and literacy worksheet 
	 //that is fun and educational for all your students. 
	 //<br/><strong>One worksheet to practise three Rs.</strong><br/><br/></p>';
}

function get_header_links($name, $runID='0')
{
	if ($name=="questions" || $runID > '9')
		$cell1 = "<img alt='logo' src='../gfx/header/h1_notext.jpg'>";
	else
		$cell1 = "<img alt='logo' src='../gfx/header/h1_withtext.jpg'>";

//	$cell2 = "<a href='http://codebreaker.adsgeo.com.au/home/about.aspx'>Login</a><br/><a href='mailto:info@thecodebreaker.com.au'>Contact Us</a>";
	$cell2 = "<a href='mailto:info@thecodebreaker.com.au'>Contact Us</a>";
	return "<table><tr align='center'><td>".$cell1."</td><td align='left' valign='top'>".$cell2."</td></tr></table>";
}
function get_footer_text()
{
	// return "<p align="."center "."style="."padding-left:15px;"."><b>The maths and literacy options are endless with The Code Breaker!</b><br/>No matter the topic, subject of maths and literacy level. <br/>This worksheet makes Maths FUN.</p>";

	$cell1 = "<p></p>"; // "<p align="."center "."style="."padding-left:70px;"."><b>The maths and literacy options are endless with The Code Breaker!</b><br/>No matter the topic, subject of maths and literacy level. <br/>This worksheet makes Maths FUN.</p>";
	$cell2 = "<p></p>";
	
	return "<table><tr valign='bottom'><td>".$cell2."</td><td align='center'>".$cell1."</td></tr></table>";

}

function group_listbox($name, $select='00') 
{ 
	$sequence = get_select_key_offset($name);
	$selix = get_default_index($sequence, $select);
	$middle = array();
	$prefix = "<select onchange=\"myFunction()\" name='".$name."' id='".$name."' ><option value='00'>Select</option>";
	$i = 0;
	$middle[$i++] = "<option ".get_default($selix, $i)."value='01'>Early Primary</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='02'>Primary</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='03'>Secondary</option>";
	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;
}

function students_listbox($name, $select='00') 
{ 
	$sequence = get_select_key_offset($name);
	$selix = get_default_index($sequence, $select);	
	$middle = array();
	$prefix = "<select name='".$name."'><option value='0'>Select</option>";
	for ($i = 0; $i < 30; $i++)
	{
		$num = $i+1;
		if ($selix == $num)
			$middle[$i] = "<option selected='selected' value='".$num."'>".$num."</option>";
		else
			$middle[$i] = "<option value='".$num."'>".$num."</option>";
		
	}
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;
}
///
function ten_questions_type_listbox($name, $select='00') 
{ 	
	$sequence = get_select_key_offset($name);
	$selix = get_default_index($sequence, $select);
	$middle = array();
	$prefix = "<select name='".$name."'><option value='0'>Select</option>";
	$i = 0;
	$middle[$i++] = "<option ".get_default($selix, $i)."value='01Addition'>Addition</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='02Subtraction'>Subtraction</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='03Multiplication'>Multiplication</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='04Division'>Division</option>";

	if (!IsEarlyPrimary($select))
	{
		$middle[$i++] = "<option ".get_default($selix, $i)."value='052_Step_Maths'>2 Step Maths</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)."value='06Squaring'>Squaring</option>";
	}
	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;
}
///
///
function GetQuestionTable($selectInfo='00',$number=0) 
{ 
	// Early primary stays the same

	if (IsEarlyPrimary($selectInfo))
	{
		return '<tr height="15px">
					<td align="right" valign="center"> 
						'.ten_questions_type_listbox('01que_1', $selectInfo).' 
					</td>
					<td> 
						'.ten_questions_number_listbox('01num_1', $selectInfo).'
					</td>
				</tr>
				<tr height="15px">
					<td align="right" valign="center"> 
					'.ten_questions_type_listbox('01que_2', $selectInfo).'
					</td>
					<td> 
					'.ten_questions_number_listbox('01num_2', $selectInfo).'
					</td>
				</tr>
				<tr height="15px">
					<td align="right" valign="center"> 
					'.ten_questions_type_listbox('01que_3', $selectInfo).'
					</td>
					<td> 
					'.ten_questions_number_listbox('01num_3', $selectInfo).'
					</td>
				</tr>
			';
	}
	if (IsPrimary($selectInfo))
	{
		return  '<tr height="15px">
				<td colspan="2">Primary selection</td>
			</tr>
			<tr>
				<td valign="center" colspan="2" class="primary_selection_td"> 
				'.getPrimarySelectionFields('primary_selection', $selectInfo,$number).'
				</td>
			</tr>
			<tr class="OperationRow" >
				<td colspan="2" >Operation</td>
			</tr>
			<tr>
				<td class="OperationRowValue" colspan="2" >
				'.getOperationalData('operation',$selectInfo,$number).'
				</td>
			</tr>
			<tr class="subOperationalRowvalue">'.getOperationalSelectionData('operation_2',$selectInfo,$number).'</tr>';

	}
	if (isSecondary($selectInfo))
	{
		return  '<tr height="15px">
				<td colspan="2" >Secondary selection</td>
			</tr>
			<tr>
				<td colspan="2" class="primary_selection_td" > 
				'.getSecondarySelectionFields('primary_selection', $selectInfo,$number).'
				</td>
			</tr>
			<tr class="OperationRowSecondary">
				<td colspan="2" >Operation</td>
			</tr>
			<tr>
				<td colspan="2" class="OperationRowValueSecondary">
				'.getSecondaryOperationalData('operation',$selectInfo,$number).'
				</td>
			</tr>
			<tr class="subOperationalRowvalueSecondary">'.getOperationalSelectionDataSecondary('operation_2',$selectInfo,$number).'</tr>';
	}
		// New custom selections for Other Groups
return '<tr height="15px">
			<td align="right" valign="center"> 
			</td>
			<td> 
			</td>
		</tr>
		<tr height="15px">
			<td align="right" valign="center"> 
			</td>
			<td> 
			</td>
		</tr>
		<tr height="15px">
			<td align="right" valign="center"> 
			</td>
			<td> 
			</td>
		</tr>
			';
}

function getOperationalSelectionDataSecondary($name,$selectInfo,$number,$primaryFlag=false,$primary_selection_id=0,$operationalFlag=false,$operation_id=0) {

	// $flag=false;
	// if ($number==6) {
	// 	$flag=true;
	// }
	$originalNumber=$number;

	if(!$operationalFlag) $operation_id=getSelectionValue("operation",$selectInfo,$number); 
	if(!$primaryFlag) $primary_selection_id=getSelectionValue("primary_selection",$selectInfo,$number); 
	if(empty($selix)) $selix=0;
	if ($operationalFlag==false) { 
		$sequence = get_select_key_offset($name);
		if($number!=1) $number=($number-1)*2;
		if($number!=1) $sequence=$sequence+$number;
		$selix = get_default_index($sequence, $selectInfo);		
	}

	if($primary_selection_id=="01" && ($operation_id=="01" ||$operation_id=="02" || $operation_id=="03"))
	{ 
		return getSecondOperationalSelectionSecondary("operation_2",$selectInfo,$originalNumber,$primary_selection_id,$operation_id).getAbstractFunctionDropDown("abstract",$selectInfo,$originalNumber);
	}
	else {
		return '';
	}

}

function getSecondOperationalSelectionSecondary($name,$selectInfo,$number,$primary_selection_id,$operation_id) {

	$selix=0;
	$tmpNumber=$number;
	if(!empty($selectInfo)) {
		$sequence = get_select_key_offset($name);
		if($number!=1) $number=($number-1)*2;
		if($number!=1) $sequence=$sequence+$number;
		$selix = get_default_index($sequence, $selectInfo);
	}
	
	$prefix ='<tr class="secondOperationTitle" ><td colspan="2">';

	$prefix.="Second Operation";
	// Operation

	$prefix.='</td></tr><tr class="secondOperation"><td colspan="2">';
	$prefix.="<select name='".$name."[]' id='".$name."'";
	if($tmpNumber==0) $prefix.="style='width: 150%;max-width: 158px;'";  
	$prefix.=">";
	$i=0;
	if ($operation_id=="01") {

		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtract then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Multiply then add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Multiply then subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Divide then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square root then multiply</option>";
	}
	else if($operation_id=="02") {
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add then divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtract then divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Divide then add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Divide then subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Divide then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square root then divide</option>";
	}
	else if($operation_id=="03") {
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add then divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtract then divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Divide then add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Divide then subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Divide then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square root then divide</option>";
	}

	$suffix = "</select></td></tr>";


	return $prefix.join($middle).$suffix;	

}


function getSecondaryOperationalData($name,$selectInfo,$number,$primaryFlag=false,$primary_selection_id=0) {


	if(!$primaryFlag) $primary_selection_id=getSelectionValue("primary_selection",$selectInfo,$number);
	if ($primaryFlag==false) {
		$sequence = get_select_key_offset($name);
		if($number!=1) $number=($number-1)*2;
		if($number!=1) $sequence=$sequence+$number;
		$selix = get_default_index($sequence, $selectInfo);
	}
	if(empty($selix)) $selix=0;
	$middle = array();
	$prefix = "<select name='".$name."[]' id='operation_secondary'><option value=''>Select</option>";
	$i = 0;
	if (empty($primary_selection_id)) {
		
	}
	elseif($primary_selection_id=="01"){

		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Include multiplication</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Include division</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Include squaring or square rooting</option>";
	}
	else if($primary_selection_id=="02") {

		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add unit fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Add proper fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Subtract unit fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Subtract proper fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Multiply fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Divide fractions</option>";
	}
	else if($primary_selection_id=="03") {
		
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Adding decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtracting decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Multiplying  decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Dividing  decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Squaring  decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square rooting  decimal fractions</option>";
	}
	else if($primary_selection_id=="04") {
		
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Given 5 or 20%, what is 100%?</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Given 30, 40 or 60%, what is 100%?</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' > Given 70, 80 or 90%, what is 100%?</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Given 12 ½  or 37 ½ %, what is 100%?</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Given 66 2/3 or 75%, what is 100%?</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Given 62 ½  or 87 ½ %, what is 100%?</option>";
	}
	else if($primary_selection_id=="05") {
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Simplifying surds</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Complete surds</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Adding surds</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Subtracting surds</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Multiplying surds</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Dividing surds</option>";	
	}
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;

}

function getSecondarySelectionFields($name,$selectInfo,$number) {

	$sequence = get_select_key_offset($name);

	if($number!=1) $number=($number-1)*2;
	if($number!=1) $sequence=$sequence+$number;
	$selix = get_default_index($sequence, $selectInfo);


	$middle = array();
	$prefix = "<select name='".$name."[]' id='secondary_selection'><option value=''>Select</option>";
	$i = 0;
	$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Integers: Substitutions</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Fractions</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Decimals</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Percentages</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Surds</option>";

	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;

}

function getSelectionStyle($selectInfo) {
	// $style='';

	// if ($selectInfo=="00") 
	// 	$style="style='display:none'";
	// else 
	// 	$style="style='display:block'";
	// return $style;
}

function getOperationalSelectionData($name,$selectInfo,$number,$primaryFlag=false,$primary_selection_id=0,$operationalFlag=false,$operation_id=0) {


	if(!$operationalFlag) $operation_id=getSelectionValue("operation",$selectInfo,$number); 
	if ($operationalFlag==false) { 
		$sequence = get_select_key_offset($name);
		$selix = get_default_index($sequence, $selectInfo);		
	}
	if(!$primaryFlag) $primary_selection_id=getSelectionValue("primary_selection",$selectInfo,$number);
	


	if(empty($selix)) $selix=0;


	if($primary_selection_id=="01" && ($operation_id=="01" ||$operation_id=="02" || $operation_id=="03" || $operation_id=="04" ||$operation_id=="05" || $operation_id=="06"))
	{ 



		return getAbstractFunctionDropDown("abstract",$selectInfo,$number);
		// ."<input type='hidden' name='operation_2[]' id='hiddenOperationField' value='00' >";
	}
	else if($primary_selection_id=="02" && ($operation_id=="01" || $operation_id=="02" ||$operation_id=="03")) {



		return getSecondOperationalSelection("operation_2",$selectInfo,$number,$primary_selection_id,$operation_id).getAbstractFunctionDropDown("abstract",$selectInfo,$number);	
	}
	else {
		// return '<input type="hidden" name="abstract[]" id="hiddenAbstractField" value="00" ><input type="hidden" name="operation_2[]" id="hiddenOperationField" value="00" >';
	}
}

// function getCheckOperationalSelectionData($name,$selectInfo,$primaryFlag=false,$primary_selection_id=0,$operationalFlag=false,$operation_id=0) { 

// 	if(!$operationalFlag) $operation_id=getSelectionValue("operation",$selectInfo); 
// 	if ($operationalFlag==false) { 
// 		$sequence = get_select_key_offset($name);
// 		$selix = get_default_index($sequence, $selectInfo);		
// 	}

// 	if(!$primaryFlag) $primary_selection_id=getSelectionValue("primary_selection",$selectInfo); 
// 	if(empty($selix)) $selix=0;


// 	if($primary_selection_id=="01" && ($operation_id=="01" ||$operation_id=="02" || $operation_id=="03" || $operation_id=="04" ||$operation_id=="05" || $operation_id=="06"))
// 	{ 
// 		return "<input type='hidden' name='operation_2[]' id='hiddenOperationField' value='00' >";
// 	}
// 	else if($primary_selection_id=="02" && $operation_id=="01" || $operation_id=="02" ||$operation_id=="03") {
// 	}
// 	else {
// 		return '<input type="hidden" name="abstract[]" id="hiddenAbstractField" value="00" ><input type="hidden" name="operation_2[]" id="hiddenOperationField" value="00" >';
// 	}
// }

function getSecondOperationalSelection($name,$selectInfo,$number,$primary_selection_id,$operation_id) {

	$selix=0;
	if(!empty($selectInfo)) {
		$sequence = get_select_key_offset($name);
		if($number!=1) $number=($number-1)*2;
		if($number!=1) $sequence=$sequence+$number;
		$selix = get_default_index($sequence, $selectInfo);
	}
	
	$prefix ="<tr class='secondOperationTitle'><td colspan='2'>";

	$prefix.="Second Operation";
	// Operation

	$prefix.="</td></tr><tr class='secondOperation' ><td colspan='2' ><select name='".$name."[]' id='".$name."'>";
	$i=0;
	if ($operation_id=="01") {

		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtract then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Multiply then add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Multiply then subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Divide then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square root then multiply</option>";
	}
	else if($operation_id=="02") {
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add then divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtract then divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Divide then add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Divide then subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Divide then multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square root then divide</option>";
	}
	else if($operation_id=="03") {
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Square then add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Square then subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Square then divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Square root  then add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Square root  then subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square root then divide</option>";
	}

	$suffix = "</select></td></tr>";


	return $prefix.join($middle).$suffix;	

}

function getAbstractFunctionDropDown($name,$selectInfo,$number=0) {

	$selix=0;
	if(!empty($selectInfo)) {	

		$sequence = get_select_key_offset($name);
		if($number!=1) $number=($number-1)*2;
		if($number!=1) $sequence=$sequence+$number;
		$selix = get_default_index($sequence, $selectInfo);
	}

	$prefix ='<tr class="abstractSelect""><td colspan="2">';

	$prefix.="Abstract Function:";
	// Operation
	$prefix.='</td></tr><tr class="abstractSelectTitle" ><td colspan="2" >';
	$prefix.="<select name='".$name."[]' id='".$name."'";
	if($number==0) $prefix.="style='width: 150%;max-width: 158px;'";  
	$prefix.=" >";
	$i=0;

	$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Yes</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >No</option>";

	$suffix = "</select></td></tr>";
	return $prefix.join($middle).$suffix;

}


function getOperationalData($name,$selectInfo,$number,$primaryFlag=false,$primary_selection_id=0) {

	if(!$primaryFlag){
		$primary_selection_id=getSelectionValue("primary_selection",$selectInfo,$number);	
	} 
	if ($primaryFlag==false) {


		$sequence = get_select_key_offset($name);
		if($number!=1) $number=($number-1)*2;
		if($number!=1) $sequence=$sequence+$number;

		$selix = get_default_index($sequence, $selectInfo);
		
	}
	if(empty($selix)) $selix=0;
	$middle = array();
	$prefix = "<select name='".$name."[]' id='operation'><option value=''>Select</option>";
	$i = 0;
	if (empty($primary_selection_id)) {
		
	}
	elseif($primary_selection_id=="01"){

		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtract</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Multiply</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Divide</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Square</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square root</option>";
	}
	else if($primary_selection_id=="02") {

		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Including multiplication</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Including division</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Including squaring or square-rooting</option>";
	}
	else if($primary_selection_id=="03") {
		
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Add unit fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Add proper fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Subtract unit fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Subtract proper fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Multiply fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Divide fractions</option>";
	}
	else if($primary_selection_id=="04") {
		
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Adding decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Subtracting decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Multiplying  decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Dividing  decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Squaring  decimal fractions</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Square rooting  decimal fractions</option>";
	}
	else if($primary_selection_id=="05") {
		$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Find 10%</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Find 50%</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Find 25%</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='04' > Find 33 1/3%</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Increase by 10%</option>";
		$middle[$i++] = "<option ".get_default($selix, $i)." value='06' >Decrease by  10%</option>";	
	}
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;

}

function getSelectionValue($name,$selectInfo,$number=0) {


	$sequence = get_select_key_offset($name);
	if($number!=1) $number=($number-1)*2;
	if($number!=1) $sequence=$sequence+$number;
	return $selix = get_default_index($sequence, $selectInfo);

}

function getPrimarySelectionFields($name, $selectInfo,$number)  {

	$sequence = get_select_key_offset($name);


	if($number!=1) $number=($number-1)*2;
	if($number!=1) $sequence=$sequence+$number;


	$selix = get_default_index($sequence, $selectInfo);
	// if ($number==6) {
	// 	echo $sequence;
	// 	exit();
	// }


	$middle = array();
	$prefix = "<select name='".$name."[]' id='primary_selection'><option value=''>Select</option>";
	$i = 0;
	$middle[$i++] = "<option ".get_default($selix, $i)." value='01' >Whole number: One operation</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='02' >Whole number: Two operations</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='03' >Fractions</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='04' >Decimals</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='05' >Percentages</option>";

	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;

}

///
function ten_questions_number_listbox($name, $select='00') 
{ 
	$sequence = get_select_key_offset($name);
	$selix = get_default_index($sequence, $select);
	$middle = array();
	$prefix = "<select name='".$name."'><option value='0'>Of</option>";
	for ($i = 0; $i < 12; $i++)
	{
		$num = $i+1;
		if ($selix == $num)
			$middle[$i] = "<option selected='selected' value='".$num."'>".$num."</option>";
		else
			$middle[$i] = "<option value='".$num."'>".$num."</option>";
	}
	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;
}

///
function literacy_level($name, $select='00') 
{ 
	$sequence = get_select_key_offset($name);
	$selix = get_default_index($sequence, $select);
	$middle = array();
	$prefix = "<select name='".$name."'><option value='0'>Please Select</option>";
	$i = 0;
	$middle[$i++] = "<option ".get_default($selix, $i)."value='01'>Simple</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='02'>Composite</option>";
	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;
}


function topic_listbox($name, $select='00') 
{
	$sequence = get_select_key_offset($name);
	$selix = get_default_index($sequence, $select);
	$middle = array();
	$prefix = "<select name='".$name."'><option value='0'>Select code topic</option>";
	$i = 0;
	$middle[$i++] = "<option ".get_default($selix, $i)." value='01Facts'>Famous Places</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='02Maths'>Mathematics</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='03English'>English Phrases</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='04History'>Australian History</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='05Geography'>Geography</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='06Riddles'>Riddles</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)." value='07Spelling'>Punctuation & Spelling</option>";
	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;
}
function get_default_index($key, $select,$break=false) 
{ 
	$rt = -1;

	if (strlen($select) > $key)
	{

		// if ($break==true) {
		// 	echo $key;
		// 	exit();
		// }	
		$rt = substr($select, $key, Utility::SELECTION_KEY_LENGTH);		
	}


	return (int) $rt;
}

function get_default($selix, $index) 
{ 
	$rt = " ";	
	if ($selix > -1)
	{
		if ((int)$selix == $index)
		{
			$rt = "selected='selected'";
		}
	}
	return $rt;
}
function get_errorMessage($selix) 
{ 
	$rt = "";	
	$message1 = 'You have not entered a file name';
	$message2 = 'The file you attempted to upload is not allowed, must be txt.  Please try again.';
	$message3 = 'The file you attempted to upload is too large.  Please try again.';
	$message4 = 'Error has occured.  Please contact your administrator';
	$message5 = 'You have not entered any input text for the Questions!';

	$message11 = "Complete Step 1 before proceeding...";
	$message12 = "Now select other options...";
	$message13 = "The individualised worksheets you requested have been created.";
	$message14 = "Your questions are to long for the page. Please revise...";

	$rt = "<h4 id=\"errormsg\" style=\"color:green;\"> ... </h4>";
	
	if ($selix == 1)
		$rt = "<h4 id=\"errormsg\" style=\"color:red;\">$message1</h4>";
	
	if ($selix == 2)
		$rt = "<h4 id=\"errormsg\" style=\"color:red;\">$message2</h4>";
	
	if ($selix == 3)
		$rt = "<h4 id=\"errormsg\" style=\"color:red;\">$message3</h4>";
	
	if ($selix == 4)
		$rt = "<h4 id=\"errormsg\" style=\"color:red;\">$message4</h4>";
	
	if ($selix == 5)
		$rt = "<h4 id=\"errormsg\" style=\"color:red; width: 847px\">$message5</h4>";

	if ($selix == 11)
		$rt = "<h4 id=\"errormsg\" style=\"color:green;\">$message11</h4>";

	if ($selix == 12)
		$rt = "<h4 id=\"errormsg\" style=\"color:green;\">$message12</h4>";

	if ($selix == 13)
		$rt = "<h4 id=\"errormsg\" style=\"color:green;\">$message13</h4>";
	
		if ($selix == 14)
		$rt = "<h4 id=\"errormsg\" style=\"color:red;\">$message14</h4>";
	return $rt;
}

//
// Old stuff not used 
//

function grade_listbox($name, $select='00', $sequence) 
{ 
	// Hidden at the moment
	
	$selix = get_default_index($sequence, $select);
	$middle = array();
	$prefix = "<select name='".$name."' style='visibility:hidden;'><option value='0'>Select</option>";
	$i = 0;
	$middle[$i++] = "<option ".get_default($selix, $i)."value='01'>Kindergarten</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='02'>Year 1</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='03'>Year 2</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='04'>Year 3</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='05'>Year 4</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='06'>Year 5</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='07'>Year 6</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='08'>Year 7</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='09'>Year 8</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='10'>Year 9</option>";
	$middle[$i++] = "<option ".get_default($selix, $i)."value='11'>Year 10</option>";
	
	$suffix = "</select>";
	
	return $prefix.join($middle).$suffix;
}

?>

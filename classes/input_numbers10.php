<?php

class input_numbers10 
{	
	var $uniqueNumber = array();
	var $isUnique = true;

	function set_unique_number_array($sizeOfArray, $randomNum, $lowerRandomNum, $seltype, $selnum) 
	{			
		for($i=0;$i<$sizeOfArray; $i++)
		{
			$isUnique = true;
			$randomNumber = rand($lowerRandomNum, $randomNum); 			
			for ($j = 0; $j < $i; $j++) 
			{  //check previous nums for uniqueness
				if ($randomNumber==$this->uniqueNumber[$j])
				{
					$isUnique = false;
				}
			}
			
			while (!$isUnique) 
			{
				$isUnique = true;
				$randomNumber = rand($lowerRandomNum, $randomNum); 				
				for ($j = 0; $j < $i; $j++) 
				{  //check previous nums for uniqueness
					if ($randomNumber==$this->uniqueNumber[$j])
					{
						$isUnique = false;
					}
				}
			}

			$this->uniqueNumber[$i] = $randomNumber; 	
		}
		if ($seltype == 'Division') {
			for($i=0;$i<10; $i++)
				$this->uniqueNumber[$i] = $this->uniqueNumber[$i] * $selnum;  
		}  
		
	}
	
	function get_unique_number_array($func) 
	{ 
		if ($func == '1')
		{
			for($ix=0; $ix < count(	$this->uniqueNumber); $ix++)
			{
				$this->uniqueNumber[$ix] = ' ';
			}		
		}

		return $this->uniqueNumber;            
	}              
}
?>
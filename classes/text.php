<?php
class text_input {

	var $textInput = Array();

	function set_input($inputStringArray, $pageno, $numOfSquares, $numOfLetters) {

		$inputString = $inputStringArray[utilGetQuestionIx($pageno)];

		$this->textInput = formatQuestions($inputString, $numOfSquares, $numOfLetters);
	}

	function get_input($i, $func)
	{
		if (array_key_exists($i, $this->textInput))
		{
			if ($func=='1') // NOT used
			{
				$this->textInput[$i] = ' ';
			}

			return $this->textInput[$i];
		}
		else
			return " ";
	}
}
?>
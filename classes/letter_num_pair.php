<?php
class letter_num_pair {

	var $number;
	var $letter;

	public function __construct($num, $let)
	{
		$this->number = $num;
		$this->letter = $let;
	}

	public function get_letter_val()
	{
		return $this->letter;
	}
	public function get_number_val()
	{
		return $this->number;
	}

}
?>
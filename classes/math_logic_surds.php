<?php

// 
// Math Logic for Surds should all be here.
//

//
//     echo "<pre>-";
// 	print_r($selnum."-");
// 	print_r($value."-");
// 	print_r($left."-");
// 	print_r($right."-");
// 	print_r($bits_l);
// 	print_r($bits_r);
// 	print_r($rt."-");
//    exit();
        
// 
//
// Constants
//
class SURDS
{
    const Complete_List = [    
        '√2','√3','√5','√7',	
        '√8','√12','√18','√20',	     '√24',
        '√27','√28','√32','√40',     '√44',	
        '√45','√48','√50','√54',     '√63',	
        '√72','√75','√80', '√90',    '√96',  '√98',  '√99',  '√108',
        '√125','√128','√150','√162', '√175', '√180', '√200', '√242',
        '√288','√300','√500',
        '√160','√250','√360','√490', '√640','√810','√1000'
    ];

    const Simple_List = [
        '1√2','1√3','1√5','1√7',	
        '2√2','2√3','3√2','2√5',   '2√6',
        '3√3','2√7','4√2','2√10',  '2√11',
        '3√5','4√3','5√2','3√6',   '3√7',
        '6√2','5√3','4√5','3√10',  '4√6', '7√2','3√11','6√3',
        '5√5','8√2','5√6','9√2',   '5√7','6√5','10√2','11√2',  
        '12√2','10√3','10√5',
        '4√10','5√10','6√10','7√10' ,'8√10','9√10','10√10'
    ];

    const Group_0_List = [ 
        '√8','√18','√32','√50','√72','√98','√128','√162', '√200', '√242', '√288'
    ];
    const Group_1_List = [ 
        '√3','√12','√27','√48','√75','√108','√300'
    ];
    const Group_2_List = [
        '√5','√20','√45','√80','√125','√180','√500'        
    ];
}

//
function Math_Logic_Surds_Get_RandomNumberTopLine($seltyp)
{
	// seltyp is the selType. e.g. 11 - 14', 110101 - 360601
	// $seltyp is the final code. See routine above
    // For Surds this is the Top Line.

    switch ($seltyp)
    {
        case "350301": 
        case "350401": 
            return Math_Logic_Surds_Get_Group_Random(rand(0, 2));  // 3 lists to choose from !

        case "350501": 
            return Math_Logic_Surds_Get_Group_Random(rand(0, 1));  // 2 lists to choose from !

        case "350601": 
            // randomly multiply: 
            // - any surd in the √2 column by √12 or √48, or 
            // - any surd in the √3 column by √8 or √32 or √128. 

            $rt = "999";

            while (true)
            {
                $col = rand(0, 1);
                if ($col==0)
                    $list = ['√12', '√48'];
                else 
                    $list = ['√8', '√132', '√128'];

                $rtl = Math_Logic_Surds_Get_Group_Random($col);
                $rtr = $list[rand(0, count($list)-1)];

                $ixl = array_search($rtl, SURDS::Complete_List);
                $ixr = array_search($rtr, SURDS::Complete_List);            

                $rt = Math_Logic_Surds_Multiply(SURDS::Simple_List[$ixl],  SURDS::Simple_List[$ixr]);
                if (mb_strpos($rt,'√'))
                    break;
            }
            return $rt;
    }

	return 996;
}

function Math_Logic_Surds_Get_RandomNumber($seltyp, $topNum, $topRandomNum)
{
	// This is the number used in the set',  5 per set
    // The value in the top number is used for validation
    // For Surds this is the RIGHT side.

    switch ($seltyp)
    {
        case "350101": 
            $range = count(SURDS::Complete_List);
            $ix = rand(4, $range-1);  // SKIP the first 3 they are to make the maths work!!!
            return SURDS::Complete_List[$ix]; 

        case "350201": 
            $range = count(SURDS::Simple_List);
            $ix = rand(4, $range-1);
            $rt = SURDS::Simple_List[$ix]; 
            return $rt;

        case "350301": 
        case "350401": 

            $list = 0;
            // Based on the top Num Which List ?

            $ix = array_search($topNum, SURDS::Group_1_List);
            if ($ix !== FALSE)
                $list = 1;
            else 
            {
                $ix = array_search($topNum, SURDS::Group_2_List);
                if ($ix !== FALSE)
                    $list = 2;
            }

            LogLine("Math_Logic_Surds_Get_RandomNumber".$list, $topNum);

            $rt = '999';
            while (true)
            {
                $rt =  Math_Logic_Surds_Get_Group_Random($list);
                if ($rt != $topNum)
                    break;
            }
            return $rt;

        case "350501": 
            return Math_Logic_Surds_Get_Group_Random(rand(0, 1));  // 2 lists to choose from !

        case "350601": 
            // randomly assign only the four surds which when simplified have coefficient of 2 and 4 from the  √2 or √3 columns. 
            // The fifth choice can be randomly √2 or √3

            $list = ['√2', '√8', '√3', '√32', '√12', '√48'];
            return $list[rand(0, count($list)-1)];            

    }

	return 997;
}

function Math_Logic_Surds_Calc_Answer($value, $seltyp, $selnum, $randomNum)
{
    // For Surds this is the Right side.

 //   LogLine("Math_Logic_Surds_Calc_Answer".$value, $selnum);

    switch ($seltyp)
    {
        case "350101": 
            $ix = array_search($value, SURDS::Complete_List);
            return SURDS::Simple_List[$ix]; 

        case "350201": 
            $ix = array_search($value, SURDS::Simple_List);
            return SURDS::Complete_List[$ix]; 

        case "350301": 
        case "350401": 
        case "350501": 
            $ixl = array_search($selnum, SURDS::Complete_List);
            $ixr = array_search($value, SURDS::Complete_List);            

            if ($seltyp == "350301") 
                return Math_Logic_Surds_Add(SURDS::Simple_List[$ixl],  SURDS::Simple_List[$ixr], $selnum, $value);
            else if ($seltyp == "350401") 
                return Math_Logic_Surds_Subtract(SURDS::Simple_List[$ixl],  SURDS::Simple_List[$ixr]);
            else if ($seltyp == "350501") 
                return Math_Logic_Surds_Multiply(SURDS::Simple_List[$ixl],  SURDS::Simple_List[$ixr]);

            case "350601": 
                $ixr = array_search($value, SURDS::Complete_List);            
                return Math_Logic_Surds_Divide($selnum,  SURDS::Simple_List[$ixr]);        
        }
        
	return 998;
}

function Math_Logic_Surds_Get_Heading_Value($seltyp, $selnum)
{
    switch ($seltyp)
    {
        case "350601": 
            // Convert back to entire surd
            $bits_l = Math_Logic_Surds_Split_Simple($selnum);
            if ($bits_l[0] == '1')
                return '√'.sprintf("%1d",$bits_l[1]);
            else
                return '√'.sprintf("%1d",($bits_l[0] * $bits_l[0] * $bits_l[1]));

    }

	return $selnum;
}

// =============================================
// Workers: To be used from the class only
// =============================================

function Math_Logic_Surds_Get_Group_Random($list)
{
    switch ($list)
    {
        case 0; 
            $range = count(SURDS::Group_0_List);
            $ix = rand(0, $range-1);
            return SURDS::Group_0_List[$ix]; 
        case 1; 
            $range = count(SURDS::Group_1_List);
            $ix = rand(0, $range-1);
            return SURDS::Group_1_List[$ix]; 
        case 2; 
            $range = count(SURDS::Group_2_List);
            $ix = rand(0, $range-1);
            return SURDS::Group_2_List[$ix]; 
    }
    return 997;
}

function Math_Logic_Surds_Add($left, $right, $selnum, $value)
{    
    // Only like surds can be added or subtracted.

    $bits_l = Math_Logic_Surds_Split_Simple($left);
    $bits_r = Math_Logic_Surds_Split_Simple($right);

    $rt = sprintf("%1d",$bits_l[0] + $bits_r[0]).'√'.sprintf("%1d",$bits_l[1]);
    
    return $rt;        
}

function Math_Logic_Surds_Subtract($left, $right)
{
    $bits_l = Math_Logic_Surds_Split_Simple($left);
    $bits_r = Math_Logic_Surds_Split_Simple($right);

    // Only like surds can be added or subtracted.
    $rt = ($bits_r[0] - $bits_l[0]);  // reversed the order
    if ($rt == 0)
        return ('0');
    else if ($rt == 1)
        return ('√'.$bits_l[1]);
    else if ($rt == -1)
        return ('-'.'√'.$bits_l[1]);
    else
        return ($rt.'√'.$bits_l[1]);

}

function Math_Logic_Surds_Multiply($left, $right)
{
    $bits_l = Math_Logic_Surds_Split_Simple($left);
    $bits_r = Math_Logic_Surds_Split_Simple($right);

    // When multiplying surds, the rule of a√b × c√d = ac√(bd). 
    // Note1: √b × √b = b.
    // Note2:  5√3 x 2√3 = 30.

    $rt_l = $bits_l[0] * $bits_r[0];
    $rt_r = $bits_l[1] * $bits_r[1];

    if ($bits_l[1] == $bits_r[1])
         return $rt_l * $bits_r[1];
     else
        return ($rt_l.'√'.$rt_r);
}

function Math_Logic_Surds_Divide($left, $right)
{
    $bits_l = Math_Logic_Surds_Split_Simple($left);
    $bits_r = Math_Logic_Surds_Split_Simple($right);

    // When dividing surds, the rule of a√b ÷ c√d = (a/c)√(b/d). 
    // We'll pick specific surds so the numbers, a/c and b/d give whole numbers.

    // Note1: √b × √b = b.
    // Note2:  5√3 x 2√3 = 30.

    $rt_l = $bits_l[0] / $bits_r[0];
    $rt_r = $bits_l[1] / $bits_r[1];

    if ($bits_l[1] == $bits_r[1])
        return $rt_l;
    else
        return displaySurd($rt_l, $rt_r);
}

// =========================================================
// Function to display Surds
// =========================================================
function displaySurd($rt_l, $rt_r) 
{ 
	if ($rt_l == "0")
		return "0";
	else if ($rt_l == "1" )
		return '√'.$rt_r;
    else if ($rt_l != intval($rt_l))
    {
        $ans1 = 1 / $rt_l;
        if ($ans1 == intval($ans1))
        {
            return '√'.$rt_r."/".$ans1;
        }
        else
        {
            $ans2 = intval($rt_l * 100);

            $den3 = $ans2;
            $num3 = 100;

            Utility::lowest($den3, $num3);

            if ($num3 == "1")
                return $den3.'√'.$rt_r;
            else
                return $den3.'√'.$rt_r."/".$num3;
        }
    }
	else
        return ($rt_l.'√'.$rt_r);
} 

function Math_Logic_Surds_Split_Simple($surd)
{
    // Must be Simplified (Only like surds can be added or subtracted.) and have
    // the same radicand.

    $pos = mb_strpos($surd,'√');
    return [
        mb_substr($surd,0,$pos), 
        mb_substr($surd, $pos+1)
    ];
}

?>
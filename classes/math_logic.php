<?php

// 
// Math Logic should all be here.
//

//
//     echo "<pre>-";
// 	print_r($selnum."-");
// 	print_r($value."-");
// 	print_r($left."-");
// 	print_r($right."-");
// 	print_r($bits_l);
// 	print_r($bits_r);
// 	print_r($rt."-");
//    exit();
       
//

//
function Math_Logic_Get_RandomNumberTopLine($seltyp)
{
	// seltyp is the selType. e.g. 11 - 14, 110101 - 360601
	// $seltyp is the final code. See routine above
	// Fractions = numerator

	if ($seltyp < 15) // early primary
		return ($seltyp == '11' || $seltyp == '12' ? rand(1, 12) : rand(2, 12) ); 

	else if ($seltyp > '210000' && $seltyp < '219999')
	{
		if ($seltyp == '210101' || $seltyp == '210102')
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 11, 99); 
		if ($seltyp == '210201' || $seltyp == '210202')
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 11, 90); 
		if ($seltyp == '210301' || $seltyp == '210302')
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12); 
		if ($seltyp == '210401' || $seltyp == '210402')
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12); 
	}
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	else if ($seltyp > '22010100' && $seltyp < '22019999')
	{
		switch ($seltyp)
		{
			case "22010101": 
			case "22010102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 6); 
			case "22010201": 
			case "22010202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 10); 
			case "22010301": 
			case "22010302" :
			case "22010401": 
			case "22010402" : 
			case "22010501": 
			case "22010502" :
			case "22010601": 
			case "22010602" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12); 
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22020000' && $seltyp < '22029999')
	{
		switch ($seltyp)
		{
			case "22020101": 
			case "22020102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 6); 
			case "22020201": 
			case "22020202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 10); 
			case "22020301": 
			case "22020302" :
			case "22020401": 
			case "22020402" : 
			case "22020501": 
			case "22020502" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12);
			case "22020601": 
			case "22020602" : return 2; // even squares 
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22030000' && $seltyp < '22039999')
	{
		switch ($seltyp)
		{
			case "22030101": 
			case "22030102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12); 
			case "22030201": 
			case "22030202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 15); 
			case "22030301": 
			case "22030302" : return 2;
			case "22030401": 
			case "22030402" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12);
			case "22030501": 
			case "22030502" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 5);
			case "22030601": 
			case "22030602" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12);
		}
	}	
	else if ($seltyp > '230000' && $seltyp < '239999')
	{
		// 02 and 04 - must be simplest for (FRACTIONS) 
		// e.g. If DENO is even, Numerator must be odd etc...

		switch ($seltyp)
		{
			case "230101": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12); 
			case "230201": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 6);  
			case "230301": return Math_Logic_Get_RandomNumber_Valid($seltyp, 9, 12); 
			case "230401": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 3); 
			case "230501": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 7); 
			case "230601": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 7); 
		}
	}
	else if ($seltyp > '240000' && $seltyp < '249999') // DECIMALS - topline
	{
		switch ($seltyp)
		{
			case "240101": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 10, 500, 10);  // Div by 10
			case "240201": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 11, 200, 10);  // 20 - 100 for question 
			case "240301": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 1, 12, 10); 
			case "240401": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 1, 12, 10); 
			case "240501": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 10, 120, 100); 
			case "240601": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 10, 120, 100); 
		}
	}
	else if ($seltyp > '250000' && $seltyp < '259999') // PERCENT
	{
		switch ($seltyp)
		{
			case "250101": return 10; 
			case "250201": return 50;
			case "250301": return 25;
			case "250401": return 33.3333;
			case "250501": return 1.1; 
			case "250601": return 0.9;
		}
	}
		// SECONDARY: Two Op: + - *, /, SQ, SQ root || THEN Multiply
		else if ($seltyp > '31010000' && $seltyp < '31019999')
		{
			switch ($seltyp)
			{
				case "31010101": 
				case "31010102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 6); 
				case "31010201": 
				case "31010202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 6); 
				case "31010301": 
				case "31010302" :
				case "31010401": 
				case "31010402" : 
				case "31010501": 
				case "31010502" :
				case "31010601": 
				case "31010602" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12); 
			}
		}	
		// SECONDARY: Two Op: + - *, /, SQ, SQ root || THEN DIV
		else if ($seltyp > '31020000' && $seltyp < '31029999')
		{
			switch ($seltyp)
			{
				case "31020101": 
				case "31020102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 6); 
				case "31020201": 
				case "31020202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 10); 
				case "31020301": 
				case "31020302" :
				case "31020401": 
				case "31020402" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12);

				case "31020501": 
				case "31020502" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12);
				
				case "31020601": 
				case "31020602" : return 2; // even squares 
			}
		}	
		// SECONDARY: Two Op: + - *, /, SQ, SQ root || THEN DIV
		else if ($seltyp > '31030000' && $seltyp < '31039999')
		{
			switch ($seltyp)
			{
				case "31030101": 
				case "31030102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12); 
				case "31030201": 
				case "31030202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12); 
				case "31030301": 
				case "31030302" : return 2;  // or 4
				case "31030401": 
				case "31030402" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12);
				case "31030501": 
				case "31030502" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12);
				case "31030601": 
				case "31030602" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12);
			}
		}	
	else if ($seltyp > '320000' && $seltyp < '329999') // Fractions (SECONDARY)
	{
		switch ($seltyp)
		{
			case "320101": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12);
			case "320201": return Math_Logic_Get_RandomNumber_Valid($seltyp, 3, 12);
			case "320301": return Math_Logic_Get_RandomNumber_Valid($seltyp, 3, 12);  // Denom 3 - 11
			case "320401": return Math_Logic_Get_RandomNumber_Valid($seltyp, 3, 12); // Must be proper fractions. vet Random
			case "320501": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 9); // ditto
			case "320601": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 9); // ditto
		}
	}
	else if ($seltyp > '330000' && $seltyp < '339999') // DECIMALS
	{
		switch ($seltyp)
		{
			case "330101": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 10, 500, 10);  // Div by 10
			case "330201": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 11, 200, 10);  // 20 - 100 for question 
			case "330301": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 1, 12, 10); 
			case "330401": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 1, 12, 10); 
			case "330501": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -120, 120, 100); 
			case "330601": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 10, 120, 100); 
		}
	}
	else if ($seltyp > '340000' && $seltyp < '349999') // Percentages (SECONDARY)
	{
		switch ($seltyp)
		{
			case "340101": 
				return 	Math_Logic_Get_RandomPercentage($seltyp, Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 2));

			case "340201": 
			case "340301": 
				return 	Math_Logic_Get_RandomPercentage($seltyp, Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 3));

			case "340401": 
			case "340501": 
			case "340601": 
				return 	Math_Logic_Get_RandomPercentage($seltyp, Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 2));
		}
	}
	else if ($seltyp > '350000' && $seltyp < '359999') // Surds (SECONDARY)
	{
		return Math_Logic_Surds_Get_RandomNumberTopLine($seltyp);
	}

	return Math_Logic_Get_RandomNumber_Valid($seltyp, 11, 99); 
}


function Math_Logic_Get_SecondRandomNumberTopLine($seltyp, $topline)
{
	// seltyp is the selType. e.g. 11 - 14, 110101 - 360601
	// $seltyp is the final code. See routine above

	// Primary: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	if ($seltyp > '22010100' && $seltyp < '22019999')
	{
		switch ($seltyp)
		{			
			case "22010101": 
			case "22010101": return rand(1, 7);
			case "22010201": 
			case "22010202": return rand(1, 7);
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22020000' && $seltyp < '22029999')
	{
		switch ($seltyp)
		{
			case "22020101": 
			case "22020102": 
			case "22020401": 
			case "22020402" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 7); 
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22030000' && $seltyp < '22039999')
	{
		switch ($seltyp)
		{
			case "22030101": 
			case "22030102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12); 
		}	
	}
	else if ($seltyp > '230000' && $seltyp < '239999')
		{
			// 02 and 04 - must be simplest for (FRACTIONS) 
			// Must be Proper
		 	// Fractions = denominator

		$cnt = 0;
		while($cnt < 10)
		{
			$rt = rand(1, 12);
			switch ($seltyp)
			{			
				case "230201": $rt = Math_Logic_Get_RandomNumber_Valid($seltyp, $topline+1, 9);  break;			
				case "230401": $rt =  Math_Logic_Get_RandomNumber_Valid($seltyp, 9, 9);  break;
				case "230501": $rt =  Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 9); break;
				case "230601": $rt =  Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 9); break;
			}

			if ($rt > $topline)
				break;
			$cnt++;
		}
		return $rt;
	}
	else if ($seltyp == '31010501' || $seltyp == '31010502' ||
			 $seltyp == '31020501' || $seltyp == '31020502')
	{
		$cnt = 0;
		while($cnt < 10)
		{
			if ($seltyp == '31010501' || $seltyp == '31010502')
				$rt = rand(2, 12);
			else
				$rt = rand(1, 12);

			if ($rt != $topline)
				break;
			$cnt++;
		}
		return $rt;
	}
	else if ($seltyp == '31010601' || $seltyp == '31010602')
		return rand(2, 12);
		
	else if ($seltyp > '320000' && $seltyp < '329999') // Fractions (SECONDARY)
	{
		$cnt = 0;
		while (true)
		{
			$rt = rand(1, 12);

			switch ($seltyp)
			{
				case "320101": 
				case "320301": 
					return $rt;

				case "320201": 
				case "320401": 
				case "320501": 
				case "320601": 

					if ($topline != $rt)
						return $rt;
			}
			$cnt++;
			if ($cnt > 20)
				return $rt;
}
	}

	return rand(1, 12);
}

function Math_Logic_Get_RandomPercentage($seltyp, $key)
{
	$choices = array( 5, 20);
	switch ($seltyp)
	{
		case "340101": $choices = array( 5, 20); break;
		case "340201": $choices = array( 30, 40, 60); break;
		case "340301": $choices = array( 70, 80, 90); break;
		case "340401": $choices = array( 12.5, 37.5); break;
		case "340501": $choices = array( 66.66, 75); break;
		case "340601": $choices = array( 62.5, 87.5); break;
	}
	if (array_key_exists($key-1, $choices))
		return $choices[$key-1];
	else
		return $choices[0];	
}

function Math_Logic_Get_RandomNumber($seltyp, $topNum, $topRandomNum, $evenPosition)
{
	// This is the number used in the set,  5 per set
	// The value in the top number is used for validation

	if ($seltyp < 15) // early primary
	{
		$cnt = 0;
		while (true)
		{
			if ($seltyp == '11')
				$result =  rand(1, 10);
			else if ($seltyp == '12')
				$result = rand($topNum, 24); // sub must be larger than top num
			else 
				$result = rand(1, 12);

			if (Math_Logic_Is_RandomNumber_Valid($seltyp, $result, false, $topNum, $topRandomNum) || $cnt > 10)
				return $result;
			$cnt++;
		}
	}
	else if ($seltyp > '210000' && $seltyp < '219999')
	{
		if ($seltyp == '210101' || $seltyp == '210102')
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 11, 99, false, $topNum, $topRandomNum); 		
		if ($seltyp == '210201' || $seltyp == '210202') // Subtraction rules - start with topnum ! (keep positive)
			return Math_Logic_Get_RandomNumber_Valid($seltyp, $topNum, 99, false, $topNum, $topRandomNum); 
		if ($seltyp == '210301' || $seltyp == '210302') // mul
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
		if ($seltyp == '210401' || $seltyp == '210402') // Div
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false, $topNum, $topRandomNum); 
		if ($seltyp == '210501' || $seltyp == '210502') // sq
			return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
		if ($seltyp == '210601' || $seltyp == '210602')
		{ 
			$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
			return ($rt * $rt); // sq
		}
	}
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	else if ($seltyp > '22010100' && $seltyp < '22019999')
	{
		switch ($seltyp)
		{
			case "22010101": 
			case "22010102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 6, false, $topNum, $topRandomNum); 

			case "22010201": 
			case "22010202" :
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, $topNum, 12+$topNum, false, $topNum, $topRandomNum); 
				return $rt;

			case "22010301": 
			case "22010302" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
			case "22010401": 
			case "22010402" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 6, 12, false, $topNum, $topRandomNum); 
			case "22010501": 
			case "22010502" : return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum);
			case "22010601": 
			case "22010602" : 
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
				return ($rt * $rt); // sq	 
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22020000' && $seltyp < '22029999')
	{
		// These guys are all factors !
		switch ($seltyp)
		{
			case "22020101": 
			case "22020102" :
				$rt = 0;
				while ($rt < 1) // Greater than 0
				{
					$rt = ( ($topRandomNum * Math_Logic_Get_RandomNumber_Valid($seltyp, $topRandomNum, 12, false, $topNum, $topRandomNum)) - $topNum); 
				}
				return $rt;

			case "22020201": 
			case "22020202" :
				$rt = 0;
				while ($rt < 1) // Greater than 0
				{
					$rt = ($topNum +  ($topRandomNum * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum))); 
				}
				return $rt;

			case "22020301": 
			case "22020302" : // Div then Add
					$rt = 0;
					while ($rt < 1) // Greater than 0
					{
						$rt = (($topNum * Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false, $topNum, $topRandomNum))  ); 
					}
					return $rt;
	
			case "22020401": 
			case "22020402" :   // Div Than Sub
				while (true) // Greater than 0
				{
					$work = Math_Logic_Get_RandomNumber_Valid($seltyp, $topRandomNum, 12, false, $topNum, $topRandomNum);
					$rt = ($topRandomNum + ($topNum * $work));
					if ($work > $topRandomNum)
					{
						$rt = ($topNum * $work);
						break; 
					}
				}
				return $rt;

			case "22020501": 
			case "22020502" : 
				$work = Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false, $topNum, $topRandomNum);
				return ($topNum * $work); // Factor

			case "22020601": 
			case "22020602" : 
				while (true)
				{
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
					$rt = ($rt * $rt);
					if ($rt % 2 == 0)
						break;
				}
				return ($rt) ; // sqroot * topnum
		}
	}
	// Primary: Two Op: + - *, /, SQ, SQ root || WITH SQUARES
	else if ($seltyp > '22030100' && $seltyp < '22039999')
	{
		switch ($seltyp)
		{
			case "22030101": 
			case "22030102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
			case "22030201": 
			case "22030202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, 6, 12, false, $topNum, $topRandomNum); 
			
			case "22030301": 
			case "22030302" : 
			while (true) // must be even
			{
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false, $topNum, $topRandomNum); 
				if ($rt % 2 == 0)
					break;
			}
			return ($rt);

			case "22030401": 
			case "22030402" : 
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
				return ($rt * $rt); // sq	 

			case "22030501": 
			case "22030502" : 
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 5, 12, false, $topNum, $topRandomNum); 
				return ($rt * $rt); // sq	 

			case "22030601": 
			case "22030602" : 
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
				return ($rt * $rt); // sq	 
		}
	}
	else if ($seltyp > '230000' && $seltyp < '239999') // Fractions
	{
		switch ($seltyp)
		{
			case "230101": 
				return "1"."F".Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false, $topNum, $topRandomNum);  
			
			case "230201": 
				$cnt = 0;
				while ($cnt < 10)
				{
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 2, $topRandomNum-1, false);  
					$denom = Math_Logic_Get_RandomNumber_Valid($seltyp, $rt+1, 9, false);
					if ($denom != $topRandomNum)
						return $rt."F".$denom;
					$cnt++;
				}

			case "230301": 
				return "1"."F".Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false, $topNum, $topRandomNum); 

			case "230401": 
				$topdecimal = ($topNum / $topRandomNum );
				$cnt = 0;
				while ($cnt < 10)
				{
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 3, false);  
					$rt2 = Math_Logic_Get_RandomNumber_Valid($seltyp, $rt+1, 12, false);
					if (($rt / $rt2) > $topdecimal)  // must not be negative answer
						return $rt."F".$rt2;	
					$cnt++;
				}
				return $rt."F".$rt2;
				
				// 		echo "<pre>-";
			// print_r($topNum.'-');
			// print_r($seltyp.'-');
			// print_r($num.'-');
			// print_r($topRandomNum.'-');
			// print_r($ans.'-');
			// exit();	
				

			case "230501": 
			case "230601": 

				$cnt = 0;
				$rt = 0;
				$rt2 = 0;
				while ($cnt < 10)
				{
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 9, false);  
					$rt2 = Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false);
					if ($rt != $rt2)
						break;
					$cnt++;
				}
				return $rt."F".$rt2;
		}
	}
	else if ($seltyp > '240000' && $seltyp < '249999') // Decimals
	{
		switch ($seltyp)
		{
			case "240101": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 10, 500, 10, false); 
			case "240201":
				$cnt = 0;  
				while (true)
				{
					$rt =  Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 201, 1000, 10, false);
					$cnt++;
					if ($rt < $topNum ||
						$cnt > 20)
						break;
				}
				return $rt;

			case "240301": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 1, 12, 10, false); 

			case "240401": 

					$rt = (rand(1,12) * $topNum) / 10;
					// $cnt = 0;  
					// while (true)
					// {
					// 	$rt = Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 10, 20, 100, false); 
					// 	$cnt++;
					// 	$ans1 = sprintf("%01.2f", $rt / $topNum);
					// 	$ans2 = sprintf("%01.2f", round($rt / $topNum, 2));

					// 	// LogLine("XXYX ".$ans1, $ans2);

					// 	if ($ans1 == $ans2 ||
					// 	 	$cnt > 20)
					// 		break;
					// }
					return $rt;
				
			case "240501": 
				$rt = Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 1, 12, 10, false); 
				return $rt;

			case "240601": 
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
				return ($rt * $rt) / 100; // sq	 / 100
		}		
	}
	else if ($seltyp > '250000' && $seltyp < '259999') // percentages
	{
		switch ($seltyp)
		{
			case "250101": return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 99, false); 
			case "250201": return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 99, false); 
			case "250301": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 98, false); 
			case "250401": return Math_Logic_Get_RandomNumber_Valid($seltyp, 3, 99, false);  // must be factor of 3
			case "250501": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 98, false); 
			case "250601": return Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 98, false); 
		}
	}



		// Primary: Two Op: + - *, /, SQ, SQ root || THEN Multiply
		else if ($seltyp > '31010100' && $seltyp < '31019999')
		{
			switch ($seltyp)
			{
				case "31010101": 
				case "31010102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, -13, 6, false, $topNum, $topRandomNum); 
	
				case "31010201": 
				case "31010202" :
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -6, 12, false, $topNum, $topRandomNum); 
					return $rt;
	
				case "31010301": 
				case "31010302" : return Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
				case "31010401": 
				case "31010402" : return Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
				case "31010501": 
				case "31010502" : return Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum);
				case "31010601": 
				case "31010602" : 
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
					return ($rt * $rt); // sq	 
			}
		}	
		// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
		else if ($seltyp > '31020000' && $seltyp < '31029999')
		{
			// These guys are all factors !
			switch ($seltyp)
			{
				case "31020101": 
				case "31020102" :
						return ( ($topRandomNum * Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum)) - $topNum); 
	
				case "31020201": 
				case "31020202" :
						return ($topNum +  ($topRandomNum * Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum))); 
	
				case "31020301": 
				case "31020302" : // Div then Add
						return (($topNum * Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum))  ); 
		
				case "31020401": 
				case "31020402" :   // Div Than Sub - factor
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum);
						if ($rt != 1 && $rt != -1)
						break;
					}
					return ($rt * $topNum ) ; // factor

				case "31020501": 
				case "31020502" : 
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum);
						if ($rt != 1 && $rt != -1)
							break;
					}
					return ($rt * $topNum ) ; // factor

				case "31020601": 
				case "31020602" : 
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
						$rt = ($rt * $rt);
						if ($rt % 2 == 0)
							break;
					}
					return ($rt) ; // sqroot / 2
			}
		}
		// Primary: Two Op: + - *, /, SQ, SQ root || WITH SQUARES
		else if ($seltyp > '31030100' && $seltyp < '31039999')
		{
			switch ($seltyp)
			{
				case "31030101": 
				case "31030102" :return Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
				case "31030201": 
				case "31030202" :return Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
				
				case "31030301": 
				case "31030302" : 
				while (true) // must be even
				{
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
					if ($rt % 2 == 0)
						break;
				}
				return ($rt);
	
				case "31030401": 
				case "31030402" : 
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false, $topNum, $topRandomNum); 
					return ($rt * $rt); // sq	 
	
				case "31030501": 
				case "31030502" : 
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 2, 12, false, $topNum, $topRandomNum); 
					return ($rt * $rt); // sq	 
	
				case "31030601": 
				case "31030602" : 
					$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
					return ($rt * $rt); // sq	 
			}
		}
		else if ($seltyp > '320000' && $seltyp < '329999') // Fractions (SECONDARY)
		{
			$rt = "";
			$rt2 = "";

			// These must be proper fractions
			switch ($seltyp)
			{
				case "320101": 
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false); 
						if ($rt != 1 && $rt != -1)
							break;
					}
					return storeFraction(($rt < 0 ? -1 : 1), abs($rt));
				
				case "320201": 
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, $topRandomNum-1, false);  // second rand must be > this 
						$rt2 = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false);
						if ($rt != 1 && $rt != -1 && $rt2 != 1 && $rt2 != -1 && abs($rt) != abs($rt2))
							break;
					}
					return storeFraction($rt, $rt2); 
	
				case "320301": 
					while (true)
					{
						$rt =  Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false); 
						if ($rt != 1 && $rt != -1)
							break;
					}
					return storeFraction(($rt < 0 ? -1 : 1), abs($rt));
					

				case "320401": 
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 3, false);  // second rand must be > this 
						$rt2 = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false);
						if ($rt != 1 && $rt != -1 && $rt2 != 1 && $rt2 != -1 && abs($rt) != abs($rt2))
							break;
					}
					return storeFraction($rt, $rt2); 
	
				case "320501": 
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 9, false);  // second rand must be > this 
						$rt2 = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false);
						if ($rt != 1 && $rt != -1 && $rt2 != 1 && $rt2 != -1 && abs($rt) != abs($rt2))
							break;
					}
					return storeFraction($rt, $rt2); 
	
				case "320601": 
					while (true)
					{
						$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 9, false);  // second rand must be > this 
						$rt2 = Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false);
						if ($rt != 1 && $rt != -1 && $rt2 != 1 && $rt2 != -1 && abs($rt) != abs($rt2))
							break;
					}
					return storeFraction($rt, $rt2); 
			}
		}
	else if ($seltyp > '330000' && $seltyp < '339999') // Decimals
	{
		switch ($seltyp)
		{
			case "330101": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -500, 500, 10, false); 
			case "330201":
				$cnt = 0;  
				while (true)
				{
					$rt =  Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -800, 1000, 10, false);
					$cnt++;
					if ($rt < $topNum ||
						$cnt > 20)
						break;
				}
				return $rt;

			case "330301": 
				if ($evenPosition)
					return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -12, 12, 10, false); 
				else
					return Math_Logic_Get_RandomNumber_Valid($seltyp, -12, 12, false);

			case "330401": 
					$cnt = 0;  
					$ans1 = "";
					while (true)
					{
						//
						// 1st, 3rd and 5th numbers in the grid are to be random 1 dec place number between -1.2 and 1.2 
						//     (but not 1.0 or 0), that are then multiplied by the dividing decimal.
						// The 2nd and 4th numbers in the grid are whole +/- numbers from 2-12 also multiplied by the dividing decimal.
						//
						 
						$noDecimals = false;

						if ($evenPosition)
						{
							 $rt =  Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -120, 120, 100, false); 
							 $noDecimals = ( intval(($rt / $topNum) * 10) == ($rt / $topNum)*10);
						}
						else
						{
							 $rt =  Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -120, 120, 10, false); 
							 $noDecimals = ( intval($rt / $topNum) == ($rt / $topNum));							 
						}
						$cnt++;

						if ($cnt > 50 || $noDecimals)
							break;
					}
					return $rt;
				
			case "330501": return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -12, 12, 10, false); 

			case "330601": 
				$rt = Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
				return ($rt * $rt) / 100; // sq	 / 100
		}		
	}
	
	else if ($seltyp > '340000' && $seltyp < '349999') // Percentages (SECONDARY)
	{
		$choices = array(1,2);
		switch ($seltyp)
		{
			case "340101": 
				if ($topNum == '20')
					$choices = array(
						Math_Logic_Get_RandomNumber_Valid($seltyp, 11, 99, false),
						Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, 1, 99, 10, false));
				else
					return Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, -499, 499, 10, false);
				break;

			case "340201": 
				if ($topNum == '40')
					return (8 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false)) / 10;
				else
					$choices = array(
						(6 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false)),
						(6 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false)) / 10);
				break;

			case "340301": 
				if ($topNum == '70')
					$choices = array(
						7 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						7 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				else if ($topNum == '80')
					$choices = array(
						8 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						8 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				else
					$choices = array(
						9 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						9 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				break;

			case "340401": 
				if ($topNum < '20')
					$choices = array(
						Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				else
					$choices = array(
						3 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						3 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				break;

			case "340501": 
				if ($topNum > '70')
					$choices = array(
						3 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						3 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				else
					$choices = array(
						2 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						2 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				break;

			case "340601": 
				if ($topNum < '70')
					$choices = array(
						5 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						5 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				else
					$choices = array(
						7 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false),
						7 * Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false) / 10);
				break;

			}

		return $choices[$evenPosition ? 1 : 0];
	}
	else if ($seltyp > '350000' && $seltyp < '359999') // Surds (SECONDARY)
	{
		return Math_Logic_Surds_Get_RandomNumber($seltyp, $topNum, $topRandomNum);
	}

	return Math_Logic_Get_RandomNumber_Valid($seltyp, 1, 12, false, $topNum, $topRandomNum); 
}

function Math_Logic_Get_RandomDecimalNumber_Valid($seltyp, $lower, $range, $divisor, $topline=true, $topNum=0, $topRandomNum=0)
{
	// Divisor is 10, 100, 1000 etc.
	$cnt = 0;
	$result = rand($lower, $range);
	while (!Math_Logic_Is_RandomNumber_Valid($seltyp, $result, $topline, $topNum, $topRandomNum) &&
			$cnt < 10)
	{
		$result = rand($lower, $range);
		$cnt++;
	}
	$rt = $result / $divisor;
	if ($rt == intval($result / $divisor))
		return $rt.".".rand(1,2);  // add some more decimals - stop at 2 for 12 time table
	else
		return $rt;
}

function Math_Logic_Get_RandomNumber_Valid($seltyp, $lower, $range, $topline=true, $topNum=0, $topRandomNum=0)
{
	$cnt = 0;
	$result = rand($lower, $range);
	while (!Math_Logic_Is_RandomNumber_Valid($seltyp, $result, $topline, $topNum, $topRandomNum) &&
			$cnt < 10)
	{
		$result = rand($lower, $range);
		$cnt++;
	}
	return $result;
}

function Math_Logic_Is_RandomNumber_Valid($seltyp, $num, $topline, $topNum, $topRandomNum)
{
	// LogLine("XXXYXX ".$seltyp." ".$num, $topNum);

	// $seltyp is the Select=full key
	if ($topline)
	{
		if ($seltyp > '210101' &&  $seltyp < '219999')
		{
			if ($seltyp == '210101' || $seltyp == '210102')
				return ($num % 10 == 0) == false; 
			if ($seltyp == '210201' || $seltyp == '210202')
				return ($num % 10 == 0) == false; 
		}	
		else if ($seltyp > '240101' &&  $seltyp < '249999')
		{
			switch ($seltyp)
			{
				case '240101': 	return ($num % 10 == 0) == false; 
				case '240201': 	return ($num % 10 == 0) == false; 
				case '240301': 	return ($num % 10 == 0) == false; 
				case '240401': 	return ($num % 10 == 0) == false; 
				case '240501': 	return true;
				case '240601': 	return true;
			}
		}	
	}
	else
	{
		// $seltyp is the Select=full key
		if ($seltyp == 12)  // sub = early - ans between 1 and 10
		{
			$ans = Math_Logic_Calc_Answer($topNum, $seltyp, $num, 0);
			return ($ans > 0 && $ans < 11);
		}
		else if ($seltyp == 14)  // div = early - ans between 1 and 12
		{
			$ans = Math_Logic_Calc_Answer($topNum, $seltyp, $num, 0);
			return ($ans > 0 && $ans < 13);
		}
		else if ($seltyp == '210601' || $seltyp == '210602')
		{
			return ($num != 1); 
		}
		else if ($seltyp > '230100' &&  $seltyp < '230199')
		{
			return ($num != $topNum);
		}
		else if ($seltyp > '230300' &&  $seltyp < '230399')
		{
			return ($num < $topNum);
		}
		else if ($seltyp > '230400' &&  $seltyp < '230499')
		{
			return ($num != 0); // NOT zero
		}
		else if ($seltyp > '240000' &&  $seltyp < '249999')
		{
			switch ($seltyp)
			{
				case '240101': 	return ($num % 10 == 0 || $num % 1 == 0) == false; 
				case '240201': 	return ($num % 10 == 0 || $num % 1 == 0) == false; 
				case '240301': 	return ($num % 1 == 0) == false; 
				case '240401': 	return ($num % 10 == 0 || $num % 1 == 0) == false; 
				case '240501': 	return ($num % 1 == 0) == false; 
				case '240601': 	return ($num != 1 && $num != 10);
			}
		}
		else if ($seltyp == '250301')
		{
			return ($num % 2 == 0); // factor of 2
		}			
		else if ($seltyp == '250401')
		{
			//LogLine("XXXYXX ".$num, ($num % 3 == 0));
			return ($num % 3 == 0); // factor of 3
		}			
	}
	return ($num != 0); // NOT zero
}

function Math_Logic_Set_Heading($seltyp, $selnum, $randomNum, $abstractChar)
{
	$ruleHead = "Evaluate";

		// Fractions.
	if (($seltyp > '320000' && $seltyp < '329999' ) ||
		($seltyp > '330000' && $seltyp < '339999' ))
	{
		$seltyp = TranslateSelectionIfCommon($seltyp);  // some are common
	}
	
		
	if ($seltyp < '15')
	{
		if ( $seltyp == '11')
			$ruleHead = "Add ".$selnum." to these numbers";
		if ( $seltyp == '12')
			$ruleHead = "Subtract ".$selnum." from these numbers";
		if ( $seltyp == '13')
			$ruleHead = "Multiply these numbers by ".$selnum;
		if ($seltyp == '14')
			$ruleHead = "Divide these numbers by ".$selnum;
		
	}	
	// Primary: One Op: + - *, /, SQ, SQ root
	else if ($seltyp > '210100' && $seltyp < '210700')
	{
		if ($seltyp == '210101')
			$ruleHead = "Add ".$selnum." to";
		if ($seltyp == '210201')
			$ruleHead = "Subtract ".$selnum." from";
		if ($seltyp == '210301' )
			$ruleHead = "Multiply by ".$selnum;
		if ($seltyp == '210401')
			$ruleHead = "Divide by ".$selnum;
		if ($seltyp == '210501')
			$ruleHead = "Square";
		if ($seltyp == '210601')
			$ruleHead = "Square Root of";

		if ($seltyp == '210102')
			$ruleHead = $abstractChar." means + ".$selnum." so calculate:";
		if ($seltyp == '210202')
			$ruleHead = $abstractChar." means - ".$selnum." so calculate:";
		if ($seltyp == '210302')
			$ruleHead = $abstractChar." means x ".$selnum." so calculate:";
		if ($seltyp == '210402')
			$ruleHead = $abstractChar." means ÷ ".$selnum." so calculate:";
		if ($seltyp == '210502')
			$ruleHead = "² means square so calculate:";
		if ($seltyp == '210602')
			$ruleHead = "√ means square root so calculate:";
	}
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	else if ($seltyp > '22010100' && $seltyp < '22019999')
	{
		if ($seltyp == '22010101')
			$ruleHead = "Add ".$selnum." then multiply by ".$randomNum;
		if ($seltyp == '22010102')  // ".$abstractChar."
			$ruleHead = "If ".$abstractChar." means + ".$selnum." then X ".$randomNum." Find:";
		if ($seltyp == '22010201')
			$ruleHead = "Subtract ".$selnum." then multiply by ".$randomNum;
		if ($seltyp == '22010202')
			$ruleHead = "If ".$abstractChar." means - ".$selnum." then X ".$randomNum." Find:";
		if ($seltyp == '22010301')
			$ruleHead = "Multiply by ".$selnum." then add ".$randomNum;
		if ($seltyp == '22010302')
			$ruleHead = "If ".$abstractChar." means X ".$selnum." then + ".$randomNum." Find:";

		if ($seltyp == '22010401')
			$ruleHead = "Multiply by ".$selnum." then subtract ".$randomNum;
		if ($seltyp == '22010402')
			$ruleHead = "If ".$abstractChar." means X ".$selnum." then - ".$randomNum." Find:";
		if ($seltyp == '22010501')
			$ruleHead = "Divide by ".$selnum." then multiply by ".$randomNum;
		if ($seltyp == '22010502')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then X ".$randomNum." Find:";
		if ($seltyp == '22010601')
			$ruleHead = "Square root then multiply by ".$selnum;
		if ($seltyp == '22010602')
			$ruleHead = "If ".$abstractChar." means square root then X ".$selnum." Find:";
	}
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22020100' && $seltyp < '22029999')
	{
		if ($seltyp == '22020101')
			$ruleHead = "Add ".$selnum." then divide by ".$randomNum;
		if ($seltyp == '22020102')
			$ruleHead = "If ".$abstractChar." means + ".$selnum." then ÷ ".$randomNum." Find:";
		if ($seltyp == '22020201')
			$ruleHead = "Subtract ".$selnum." then divide by ".$randomNum;
		if ($seltyp == '22020202')
			$ruleHead = "If ".$abstractChar." means - ".$selnum." then ÷ ".$randomNum." Find:";
		if ($seltyp == '22020301')
			$ruleHead = "Divide by ".$selnum." then add ".$randomNum;
		if ($seltyp == '22020302')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then + ".$randomNum." Find:";

		if ($seltyp == '22020401')
			$ruleHead = "Divide by ".$selnum." then subtract ".$randomNum;
		if ($seltyp == '22020402')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then - ".$randomNum." Find:";
		if ($seltyp == '22020501')
			$ruleHead = "Divide by ".$selnum." then multiply by ".$randomNum;
		if ($seltyp == '22020502')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then X ".$randomNum." Find:";
		if ($seltyp == '22020601')
			$ruleHead = "Square root then divide by ".$selnum;
		if ($seltyp == '22020602')
			$ruleHead = "If ".$abstractChar." means square root then ÷ ".$selnum." Find:";
	}

	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22030100' && $seltyp < '22039999')
	{
		if ($seltyp == '22030101')
			$ruleHead = "Square then add ".$selnum;
		if ($seltyp == '22030102')
			$ruleHead = "If ".$abstractChar." means square then add ".$selnum." Find:";
		if ($seltyp == '22030201')
			$ruleHead = "Square then subtract ".$selnum;
		if ($seltyp == '22030202')
			$ruleHead = "If ".$abstractChar." means square than subtract ".$selnum." Find:";
		if ($seltyp == '22030301')
			$ruleHead = "Square then divide ".$selnum;
		if ($seltyp == '22030302')
			$ruleHead = "If ".$abstractChar." means square than divide ".$selnum." Find:";

		if ($seltyp == '22030401')
			$ruleHead = "Square root then add ".$selnum;
		if ($seltyp == '22030402')
			$ruleHead = "If ".$abstractChar." means square root then + ".$selnum." Find:";
		if ($seltyp == '22030501')
			$ruleHead = "Square root then subtract ".$selnum;
		if ($seltyp == '22030502')
			$ruleHead = "If ".$abstractChar." means square root then - ".$selnum." Find:";
		if ($seltyp == '22030601')
			$ruleHead = "Square root then multiply ".$selnum;
		if ($seltyp == '22030602')
			$ruleHead = "If ".$abstractChar." means square root then X ".$selnum." Find:";
	}
	// Fractions.
	else if ($seltyp > '230000' && $seltyp < '239999')
	{
		if ($seltyp == '230101')
			$ruleHead = "Add 1/".$selnum." to these fractions";
		if ($seltyp == '230201')
			$ruleHead = "Add ".$selnum."/".$randomNum." to these fractions";
		if ($seltyp == '230301' )
			$ruleHead = "Subtract 1/".$selnum." from these fractions";
		if ($seltyp == '230401')
			$ruleHead = "Subtract ".$selnum."/".$randomNum." from these fractions";
		if ($seltyp == '230501')
			$ruleHead = "Multiply these fractions by ".$selnum."/".$randomNum;
		if ($seltyp == '230601')
			$ruleHead = "Divide these fractions by ".$selnum."/".$randomNum;
	}
	// Decimals
	else if ($seltyp > '240000' && $seltyp < '249999')
	{
		if ($seltyp == '240101')
			$ruleHead = "Add ".$selnum." to these numbers";
		if ($seltyp == '240201')
			$ruleHead = "Subtract ".$selnum." from these numbers";
		if ($seltyp == '240301' )
			$ruleHead = "Multiply these numbers by ".$selnum;
		if ($seltyp == '240401')
			$ruleHead = "Divide these numbers by ".$selnum;
		if ($seltyp == '240501')
			$ruleHead = "Square these numbers";
		if ($seltyp == '240601')
			$ruleHead = "Square root these numbers";
	}
	// Percentages
	else if ($seltyp > '250000' && $seltyp < '250700')
	{
		if ($seltyp == '250101')
			$ruleHead = "Find ".$selnum."% of these numbers";
		if ($seltyp == '250201')
			$ruleHead = "Find ".$selnum."% of these numbers";
		if ($seltyp == '250301' )
			$ruleHead = "Find ".$selnum."% of these numbers";
		if ($seltyp == '250401')
			$ruleHead = "Find 33 1/3% of these numbers";
		if ($seltyp == '250501')
			$ruleHead = "Increase these by 10%";
		if ($seltyp == '250601')
			$ruleHead = "Decrease these by 10%";
	}



	// SECONDARY: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	else if ($seltyp > '31010100' && $seltyp < '31019999')
	{
		if ($seltyp == '31010101')
			$ruleHead = "Find ".$randomNum."(A + ".$selnum.") given A is";
		if ($seltyp == '31010102')
			$ruleHead = "If ".$abstractChar." means + ".$selnum." then X ".$randomNum." Find:";
		if ($seltyp == '31010201')
			$ruleHead = "Find ".$randomNum."(B - ".$selnum.") given B is";
		if ($seltyp == '31010202')
			$ruleHead = "If ".$abstractChar." means - ".$selnum." then x ".$randomNum." Find:";
		if ($seltyp == '31010301')
			$ruleHead = "Find ".$selnum."C + ".$randomNum." given C is";
		if ($seltyp == '31010302')
			$ruleHead = "If ".$abstractChar." means x ".$selnum." then + ".$randomNum." Find:";

		if ($seltyp == '31010401')
			$ruleHead = "Find ".$selnum."Z - ".$randomNum." given Z is";
		if ($seltyp == '31010402')
			$ruleHead = "If ".$abstractChar." means X ".$selnum." then - ".$randomNum." Find:";
		if ($seltyp == '31010501')
			$ruleHead = "Find ".$randomNum."Y/".$selnum."  given Y is";
		if ($seltyp == '31010502')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then x ".$randomNum." Find:";
		if ($seltyp == '31010601')
			$ruleHead = "Find ".$selnum."√Z given Z is";
		if ($seltyp == '31010602')
			$ruleHead = "If ".$abstractChar." means square root then x ".$selnum." Find:";
	}
	// SECONDARY: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '31020100' && $seltyp < '31029999')
	{
		if ($seltyp == '31020101')
			$ruleHead = "Find (A + ".$selnum.")/".$randomNum." given A is";
		if ($seltyp == '31020102')
			$ruleHead = "If ".$abstractChar." means + ".$selnum." then ÷ ".$randomNum." Find:";
		if ($seltyp == '31020201')
			$ruleHead = "Find (B - ".$selnum.")/".$randomNum." given B is";
		if ($seltyp == '31020202')
			$ruleHead = "If ".$abstractChar." means - ".$selnum." then ÷ ".$randomNum." Find:";
		if ($seltyp == '31020301')
			$ruleHead = "Find (C/".$selnum.") + ".$randomNum." given C is";
		if ($seltyp == '31020302')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then + ".$randomNum." Find:";

		if ($seltyp == '31020401')
			$ruleHead = "Find (D/".$selnum.") - ".$randomNum." given D is";
		if ($seltyp == '31020402')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then - ".$randomNum." Find:";
		if ($seltyp == '31020501')
			$ruleHead = "Find ".$randomNum."E/".$selnum." given E is";
		if ($seltyp == '31020502')
			$ruleHead = "If ".$abstractChar." means ÷ ".$selnum." then X ".$randomNum." Find:";
		if ($seltyp == '31020601')
			$ruleHead = "Find √Z/".$selnum." given Z is";
		if ($seltyp == '31020602')
			$ruleHead = "If ".$abstractChar." means square root then ÷ ".$selnum." Find:";
	}

	// SECONDARY: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '31030100' && $seltyp < '31039999')
	{
		if ($seltyp == '31030101')
			$ruleHead = "Find A² + ".$selnum." given A is";
		if ($seltyp == '31030102')
			$ruleHead = "If ".$abstractChar." means square then add ".$selnum." Find:";
		if ($seltyp == '31030201')
			$ruleHead = "Find B² - ".$selnum." given B is";
		if ($seltyp == '31030202')
			$ruleHead = "If ".$abstractChar." means square than subtract ".$selnum." Find:";
		if ($seltyp == '31030301')
			$ruleHead = "Find C²/".$selnum." given C is";
		if ($seltyp == '31030302')
			$ruleHead = "If ".$abstractChar." means square than divide ".$selnum." Find:";

		if ($seltyp == '31030401')
			$ruleHead = "Find √Z + ".$selnum." given Z is";
		if ($seltyp == '31030402')
			$ruleHead = "If ".$abstractChar." means square root then + ".$selnum." Find:";
		if ($seltyp == '31030501')
			$ruleHead = "Find √Y - ".$selnum." given Y is";
		if ($seltyp == '31030502')
			$ruleHead = "If ".$abstractChar." means square root then - ".$selnum." Find:";
		if ($seltyp == '31030601')
			$ruleHead = "Find ".$selnum."√R given R is";
		if ($seltyp == '31030602')
			$ruleHead = "If ".$abstractChar." means square root then X ".$selnum." Find:";
	}

	// Percentages
	else if ($seltyp > '340000' && $seltyp < '349999')
	{
		switch ($seltyp)
		{
			case '340101':
			case '340201':
			case '340301':
			case '340401':
			case '340501':
			case '340601':
				if ($selnum > 66 && $selnum < 67)
					$ruleHead = "What is 100% if 66 2/3% =";
				else
					$ruleHead = "What is 100% if ".$selnum."% =";
				break;
		}
	}
	// Surds
	else if ($seltyp > '350000' && $seltyp < '359999')
	{
		switch ($seltyp)
		{
			case '350101':
				$ruleHead = "Simplify these surds";
				break;
			case '350201':
				$ruleHead = "Convert to complete surds";
				break;
			case '350301':
				$ruleHead = "Add ".$selnum." to these surds:";
				break;
			case '350401':
				$ruleHead = "Subtract ".$selnum." from these surds:";
				break;
			case '350501':
				$ruleHead = "Multiply ".$selnum." by these surds:";
				break;
			case '350601':
				$rt = Math_Logic_Surds_Get_Heading_Value($seltyp, $selnum);
				$ruleHead = "Divide ".$rt." by these surds:";
				break;
		}
	}
	
		
	if (array_key_exists ('testcase', $GLOBALS))
	{
		if ($GLOBALS['testcase'] == true && strlen($ruleHead) < 28)
			return $ruleHead."  (".substr($seltyp,0,strlen($seltyp)-2).")";
			
	}
	return $ruleHead;
}

function Math_Logic_Is_Factor($seltyp)
{
	$seltyp = TranslateSelectionIfCommon($seltyp);  // some are common

	// In a division the number must be a factor of the answer
	// So the displayed number = (randNum * SelNum). 
	if ($seltyp == '14')
		return true;
	else if ($seltyp == '210401' || $seltyp == '210402')
		return true;
	else if ($seltyp == '22010501' || $seltyp == '22010502')
		return true;

	//else if ($seltyp == '240401')
	//	return true;
	// 2 step needs to be differently
	// else if ($seltyp == '22020101' || $seltyp == '22020102' ||
	// 		$seltyp == '22020201' || $seltyp == '22020202' ||
	// 		$seltyp == '22020301' || $seltyp == '22020302' ||
	// 		 $seltyp == '22020401' || $seltyp == '22020402' ||
	// 		 $seltyp == '22020501' || $seltyp == '22020502')
	// 	return true;
	
	return false;
}

function Math_Logic_Get_Formatted_Math_Question($value, $seltyp, $abstractChar)
{	
	$txt = trim($value);
	 if ($seltyp < 15)
		return $txt;

	$seltyp = TranslateSelectionIfCommon($seltyp);  // some are common

	switch ($seltyp)
	{
		// One Op Primary
		case "210102": 
		case "210202": 
		case "210302": 
		case "210402": return $txt.$abstractChar;

		case "210502": return $txt."²"; // Square as suffix
		case "210602": return "√".$txt; // Square Root as prefix

		// Two Op Primary / Mult
		case "22010102" :  
		case "22010202" : 
		case "22010302" : 
		case "22010402" :  
		case "22010502" : 
		case "22010602" : return $txt.$abstractChar;

		// Two Op Primary / DIV
		case "22020102" : 
		case "22020202" : 
		case "22020302" : 
		case "22020402" : 
		case "22020502" : 
		case "22020602" : return  $txt.$abstractChar;

		// Two Op Primary / SQUARE
		case "22030102" :
		case "22030202" :
		case "22030302" : 
		case "22030402" : 
		case "22030502" :  
		case "22030602" : return $txt.$abstractChar;	
	}

	//	LogEntry("null", $txt );

	// Special notation for fractions "23201" etc

	if (strpos($txt, "F"))
		return Math_Logic_Get_Formatted_Answer($txt, "F");

	return $txt;
}

function Math_Logic_Get_Formatted_Answer($value, $format)
{
	if ($format == "F" &&
		strpos($value, "F"))  // Fractions
	{
		$ix = strpos($value, "F");
		return substr($value,0,$ix)."/".substr($value,$ix+1);
	}
	return $value;
}

function Math_Logic_Get_Decimal_Answer_From_Formatted_Value($value, $format)
{
	if ($format == "F" &&
		strpos($value, "F"))  // Fractions
	{
		$ix = strpos($value, "F");
		return (substr($value,0,$ix) / substr($value,$ix+1));
	}
	return $value;
}

function Math_Logic_Get_Denominator($value, &$neg)
{
	if (strpos($value, "F"))  // Fractions
	{
		$ix = strpos($value, "F");

		if (strpos($value, "-"))
			$neg = true;

		return substr($value,$ix+1);
	}
	return $value;
}

function Math_Logic_Split_Formatted_Answer($value, $format, &$num, &$denom)
{
	if ($format == "F" &&
		strpos($value, "F"))  // Fractions
	{
		$ix = strpos($value, "F");

		$num = substr($value,0,$ix);
		$denom = substr($value,$ix+1);
	}
}

function TranslateSelectionIfCommon($seltyp)
{
	// Secondary -step (Integers) same as Primary 2 step
	// ==================================================

	if ($seltyp < '15')
		return $seltyp;
	else if (substr($seltyp,0,4) == '3101')
		return "2201".substr($seltyp,4);
	else if (substr($seltyp,0,4) == '3102')
		return "2202".substr($seltyp,4);
	else if (substr($seltyp,0,4) == '3103')
		return "2203".substr($seltyp,4);

	else if (substr($seltyp,0,2) == '32') // Fractions (SECONDARY)
		return "23".substr($seltyp,2); // Primary

	else if (substr($seltyp,0,2) == '33') // Decimals Fractions (SECONDARY)
		return "24".substr($seltyp,2); // Primary

	return $seltyp;
}

function Math_Logic_Get_Internal_Code($seltyp, $opt2, $opt3, $abstract)
{
	if ($seltyp < '15')
		return $seltyp;  // not required - Early Primary

	$pre = $seltyp;
	$abs = ($abstract == "01" ? "02" : "01");  // Reverse: e.g. 01 = Y 02 = N

	// Take the first 4 digits. e.g. "2102"  Make it 02 so that the > is meet below...
	$seltyp = TranslateSelectionIfCommon($seltyp."02");  // some are common. Dummy for conversion
		
	// Primary: One Op: + - *, /, SQ, SQ root WITH Abstract
	if ($seltyp > '2101' && $seltyp < '2199')
	{
		return $pre.$opt2.$abs;
	}
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	else if ($seltyp > '2201' && $seltyp < '2299')
	{
		return $pre.$opt2.$opt3.$abs;
	}
	// Fractions.
	else if ($seltyp > '2300' && $seltyp < '2399')
	{
		return $pre.$opt2."01"; // No Abstract
	}
	// Decimals
	else if ($seltyp > '2400' && $seltyp < '2499')
	{
		return $pre.$opt2."01";// No Abstract
	}
	// Percentages
	else if ($seltyp > '2500' && $seltyp < '2599')
	{
		return $pre.$opt2."01"; // No Abstract
	}

	// SECONDARY: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	else if ($seltyp > '3101' && $seltyp < '3199')
	{
		return $pre.$opt2.$opt3.$abs;
	}
	// Percentages
	else if ($seltyp > '3400' && $seltyp < '3499')
	{
		return  $pre.$opt2."01"; // No Abstract
	}
	// Surds
	else if ($seltyp > '3500' && $seltyp < '3599')
	{
		return  $pre.$opt2."01"; // No Abstract
	}
	return  $pre.$opt2."01"; // No Abstract
}

function Math_Logic_Calc_Answer($value, $seltyp, $selnum, $randomNum)
{
	$seltyp = TranslateSelectionIfCommon($seltyp);  // some are common

	// Value returned may be a formula !;
	// These are Early Primary, + - * and /
	if ($seltyp < '15')
	{
		switch ($seltyp)
		{
			case "11": return $value + $selnum;
			case "12": return $value - $selnum;
			case "13": return $value * $selnum;
			case "14": return $value / $selnum;				
		}
	}
	// Primary: One Op: + - *, /, SQ, SQ root
	else if ($seltyp > '210000' && $seltyp < '219999')
	{
		switch ($seltyp)
		{
			case "210101": 
			case "210102": 	return $value + $selnum;
			case "210201": 
			case "210202": 	return $value - $selnum;
			case "210301": 
			case "210302": 	return $value * $selnum;

			case "210401": 
			case "210402": return ($selnum == 0 ? 0 : $value / $selnum); // div by 0 test

			case "210501": 
			case "210502": return ($value * $value);
			case "210601": 
			case "210602": return ($value == 0 ? 0 : sqrt($value)); // div by 0 test
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN Multiply
	else if ($seltyp > '22010100' && $seltyp < '22019999')
	{
		switch ($seltyp)
		{
			case "22010101": 
			case "22010102" : return ($value + $selnum) * $randomNum; 
			case "22010201": 
			case "22010202" : return ($value - $selnum) * $randomNum; 
			case "22010301": 
			case "22010302" : return ($value * $selnum) + $randomNum; 
			case "22010401": 
			case "22010402" : return ($value * $selnum) - $randomNum; 
			case "22010501": 
			case "22010502" : return ($value / $selnum) * $randomNum; 
			case "22010601": 
			case "22010602" : return sqrt($value) * $selnum; 
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN DIV
	else if ($seltyp > '22020100' && $seltyp < '22029999')
	{
		switch ($seltyp)
		{
			case "22020101": 
			case "22020102" : return ($value + $selnum) / $randomNum; 
			case "22020201": 
			case "22020202" : return ($value - $selnum) / $randomNum; 
			case "22020301": 
			case "22020302" : return ($value / $selnum) + $randomNum; 
			case "22020401": 
			case "22020402" : return ($value / $selnum) - $randomNum; 
			case "22020501": 
			case "22020502" : return ($value / $selnum) * $randomNum; 
			case "22020601": 
			case "22020602" : return sqrt($value) / $selnum; 
		}
	}	
	// Primary: Two Op: + - *, /, SQ, SQ root || THEN SQUARE
	else if ($seltyp > '22030100' && $seltyp < '22039999')
	{
		switch ($seltyp)
		{
			case "22030101": 
			case "22030102" : return ($value * $value) + $selnum; 
			case "22030201": 
			case "22030202" : return ($value * $value) - $selnum; 
			case "22030301": 
			case "22030302" : return ($value * $value) / $selnum; 
			case "22030401": 
			case "22030402" : return sqrt($value) + $selnum; 
			case "22030501": 
			case "22030502" : return sqrt($value) - $selnum; 
			case "22030601": 
			case "22030602" : return sqrt($value) * $selnum; 
		}
	}	
	
	// Fractions - Also 3200000 - Secondary
	else if ($seltyp > '230000' && $seltyp < '239999')
	{
		switch ($seltyp)
		{
			case "230101": 
			{
				$den2; $num2; 
				$den3; $num3; 
	  			Math_Logic_Split_Formatted_Answer($value, "F", $num2, $den2);
				 addFraction("1", $selnum, $num2, $den2, $num3, $den3); 
				 return displayFraction($num3, $den3);
			}

			case "230201": 
			{
				$den2; $num2; 
				$den3; $num3; 
				Math_Logic_Split_Formatted_Answer($value, "F", $num2, $den2);
				addFraction($selnum, $randomNum, $num2, $den2, $num3, $den3); 
				return displayFraction($num3, $den3);
			}

			case "230301": 
			{
				$den2; $num2; 
				$den3; $num3; 
	  			Math_Logic_Split_Formatted_Answer($value, "F", $num2, $den2);
				addFraction( $num2, $den2, "1", $selnum, $num3, $den3, false);  // SUBTRACT
				return displayFraction($num3, $den3);
			}

			case "230401":
			{
				$den2; $num2; 
				$den3; $num3; 
				Math_Logic_Split_Formatted_Answer($value, "F", $num2, $den2);
				addFraction($num2, $den2, $selnum, $randomNum, $num3, $den3, false); // SUBTRACT
				return displayFraction($num3, $den3);
			}
			case "230501":
			{
				$den2; $num2; 
				$den3; $num3; 
				Math_Logic_Split_Formatted_Answer($value, "F", $num2, $den2);
				multFraction($num2, $den2, $selnum, $randomNum, $num3, $den3);
				return displayFraction($num3, $den3);
			}
			case "230601": 
			{
				// For div switch Num and Denom (i.e. reciprical)
				$den2; $num2; 
				$den3; $num3; 
				Math_Logic_Split_Formatted_Answer($value, "F", $num2, $den2);
				multFraction($num2, $den2, $randomNum, $selnum, $num3, $den3);
				return displayFraction($num3, $den3);
			}
		}
		//	LogEntry("null", $txt );
	}		
	// Primary: One Op: + - *, /, SQ, SQ root - DECIMALS
	else if ($seltyp > '240000' && $seltyp < '249999')
	{
		switch ($seltyp)
		{
			case "240101": return $value + $selnum;
			case "240201": return $value - $selnum;
			case "240301": return $value * $selnum;
			case "240401": return ($selnum == 0 ? 0 : round($value / $selnum, 3)); // div by 0 test
			case "240501": return ($value * $value);
			case "240601": return ($value == 0 ? 0 : sqrt($value)); // div by 0 test
		}
	}		
	else if ($seltyp > '250000' && $seltyp < '259999')
	{
		switch ($seltyp)
		{
			case "250101": return round(($value * $selnum) / 100, 3);
			case "250201": return round(($value * $selnum) / 100, 3);
			case "250301": return round(($value * $selnum) / 100, 3);
			case "250401": return round(($value * $selnum) / 100, 3);
			case "250501": return round(($value * $selnum), 3);
			case "250601": return round(($value * $selnum), 3);
		}
	}		
	else if ($seltyp > '340000' && $seltyp < '349999')
	{
		if ($seltyp == '340501')
			return round($value * 100 / $selnum, 1);
		else
			return round($value * 100 / $selnum, 2);
	}
	else if ($seltyp > '350000' && $seltyp < '359999') // Surds (SECONDARY)
	{
		return Math_Logic_Surds_Calc_Answer($value, $seltyp, $selnum, $randomNum);
	}

	return 999;
}

 

// =========================================================
// Function to display fractions
// =========================================================
function displayFraction($num3, $den3) 
{ 
	if ($den3 == "0")
		return "0";
	else if ($den3 == "1" || $den3 == "-1")
		return $num3;
	else if ($den3 == "-1")
		return "-".abs($num3);
	else if ($den3 < 0)
		return "-".abs($num3)."/".abs($den3);
	else
	 	return $num3."/".$den3;

} 

// =========================================================
// Function to store fractions
// =========================================================
function storeFraction($num3, $den3) 
{ 
	if ($den3 < 0 && $num3 > 0)
		return "-".abs($num3)."F".abs($den3);
	else if ($den3 < 0 && $num3 < 0 )
		return abs($num3)."F".abs($den3);
	else
	 	return $num3."F".$den3;
} 

// =========================================================
// Function to add/subtract two fractions 
// =========================================================
function addFraction($num1, $den1, $num2, $den2, &$num3, &$den3, $addNow=true) 
{ 
	// $den1 and $den2 may be signed fractions
	// Used for 1/6. e.g. 1/n style functions

	$neg1 = false;
	$neg2 = false;

	$den1 = Math_Logic_Get_Denominator($den1, $neg1);
	$den2 = Math_Logic_Get_Denominator($den2, $neg2);

	if ($neg1)
		$num1 = abs($num1) * -1;

	if ($neg2)
		$num2 = abs($num2) * -1;
		
	$den3 = Utility::gcd($den1, $den2); 

	if ($den1 == 0 || $den2 == 0 || $den3 == 0)
	{
		$num3=0;
		$den3=0;
		return;
	}
	       
	// Denominator of final 
	// fraction obtained finding 
	// LCM of den1 and den2 
	// LCM * GCD = a * b 
	$den3 = ($den1 * $den2) / $den3; 

	// Changing the fractions to 
	// have same denominator Numerator 
	// of the final fraction obtained 
	if ($addNow)
		$num3 = ($num1) * ($den3 / $den1) + 
				($num2) * ($den3 / $den2); 
	else
		$num3 = ($num1) * ($den3 / $den1) - 
				($num2) * ($den3 / $den2); 

	// Calling function to convert 
	// final fraction into it's 
	// simplest form 

	Utility::lowest($den3, $num3); 
} 

// =========================================================
// Function to mult/divide two fractions 
// =========================================================
function multFraction($num1, $den1, $num2, $den2, &$num3, &$den3) 
{ 
	// fraction obtained finding 
	// num * num / Den * Den
	// DIVISION: Reverse the num and DEN i.e. Reciprical of  
	$den3 = ($den1 * $den2); 
	$num3 = ($num1 * $num2); 

	// Calling function to convert 
	// final fraction into it's 
	// simplest form 
	Utility::lowest($den3, $num3); 
} 

// ========================================================
// Test Cases ONLY
// ========================================================

function Math_Logic_Set_TestCase($group, $ix, $arraynum, $topline=0)
{
	if ($arraynum == 1)	 // The correct Select code
	{
		return Math_Logic_Get_Correct_Key_For_TestCase($group, $ix);
	}
	else if ($arraynum == 2) //  == 2	The top line Random Number  TESTING
	{
		return Math_Logic_Get_RandomNumberTopLine(Math_Logic_Get_Correct_Key_For_TestCase($group, $ix));
	}
	else if ($arraynum == 3) //  == 3	The top line Random Number  LIVE
	{		
		return Math_Logic_Get_RandomNumberTopLine($ix);
	}
	else if ($arraynum == 4) //  == 4	The top line SECOND Random Number  LIVE
	{		
		return Math_Logic_Get_SecondRandomNumberTopLine($ix, $topline);
	}
	return "99";
}

function Math_Logic_Get_Correct_Key_For_TestCase($group, $ix)
{
	if ($group=="02") //  Primary 
	{
		if ($ix > 2100 && $ix < 2199)// one op
		{
			$prefix = "21";
			if ($ix < 2107)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 2100))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 2106))."02";
		}
		else if ($ix > 2300 && $ix < 2399) // fractions
		{
			$prefix = "23";
			return $prefix.sprintf("%02d", ($ix - 2300))."01";
		}
		else if ($ix > 2400 && $ix < 2499) // Decimals
		{
			$prefix = "24";
			return $prefix.sprintf("%02d", ($ix - 2400))."01";
		}
		else if ($ix > 2500 && $ix < 2599) // percents - 2501 to 2512
		{
			$prefix = "25";
			if ($ix < 2507)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 2500))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 2506))."02";				
		}
		else if ($ix > 220100 && $ix < 220199)// 2 Op - Mult
		{
			$prefix = "2201";
			if ($ix < 220107)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 220100))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 220106))."02";
		}
		else if ($ix > 220200 && $ix < 220299)// 2 Op - DIV
		{
			$prefix = "2202";
			if ($ix < 220207)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 220200))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 220206))."02";
		}
		else if ($ix > 220300 && $ix < 220399)// 2 Op - SQUARE
		{
			$prefix = "2203";
			if ($ix < 220307)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 220300))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 220306))."02";
		}
		return "01";
	}
	else if ($group=="03") //  Secondary Kids
	{
		if ($ix > 310100 && $ix < 310199) 
		{
			$prefix = "3101";
			if ($ix < 310107)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 310100))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 310106))."02";
		}
		else if ($ix > 310200 && $ix < 310299) 
		{
			$prefix = "3102";
			if ($ix < 310207)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 310200))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 310206))."02";
		}
		else if ($ix > 310300 && $ix < 310399) 
		{
			$prefix = "3103";
			if ($ix < 310307)  // 12 test cases/ 6 ops plus abstract
				return $prefix.sprintf("%02d", ($ix - 310300))."01";
			else
				return $prefix.sprintf("%02d", ($ix - 310306))."02";
		}
		else if ($ix > '3200' && $ix < '3299') // Fractions (SECONDARY)
		{
			$prefix = "32";
			return $prefix.sprintf("%02d", ($ix - 3200))."01";
		}
		else if ($ix > '3300' && $ix < '3399') // Decimals (SECONDARY)
		{
			$prefix = "33";
			return $prefix.sprintf("%02d", ($ix - 3300))."01";
		}
		else if ($ix > '3400' && $ix < '3499') // Percentages (SECONDARY)
		{
			$prefix = "34";
			return $prefix.sprintf("%02d", ($ix - 3400))."01";
		}
		else if ($ix > '3500' && $ix < '3599') // Surds (SECONDARY)
		{
			$prefix = "35";
			return $prefix.sprintf("%02d", ($ix - 3500))."01";
		}
	}
	else if ($group=="01") // early Primary (11 to 27; e.g. 16 tests) 
	{
			if ($ix < 15)
				return "11";
			else if ($ix < 19)
				return "12";
			else if ($ix < 23)
				return "13";
			else 
				return "14";
	}
	return "99"; // error
}

?>
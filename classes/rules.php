<?php
class rules {
	
	var $rule = array();	
	
	function set_rule_answers($inputArray, $seltyp, $SelNum, $randomNum) {
				
		for($i=0;$i<count($inputArray); $i++)
		{
			
			$this->rule[$i] = $inputArray[$i] * $inputArray[$i];
			
			if ($seltyp == 'Addition')
				$this->rule[$i] = $inputArray[$i] + $SelNum;
			if ($seltyp == 'Multiplication')
				$this->rule[$i] = $inputArray[$i] * $SelNum;
			if ($seltyp == 'Subtraction')
				$this->rule[$i] = $inputArray[$i] - $SelNum;
			if ($seltyp == '2_Step_Maths')
				$this->rule[$i] = $inputArray[$i] * $SelNum + $randomNum; 	
			if ($seltyp == 'Squaring')
				$this->rule[$i] = $inputArray[$i] * $inputArray[$i] + $SelNum;
			if ($seltyp == 'Division')
				$this->rule[$i] = $inputArray[$i] / $SelNum;
			
		}
	}

	function get_rule_answers($func) {  
		
		if ($func=='1')
		{
			for($i=0;$i<count($this->rule); $i++)
			{
				$this->rule[$i] = ' ';
			}			
		}
		return $this->rule;   		
	}              
}

?>
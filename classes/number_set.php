<?php

class number_set
{
	var $uniqueNumber = array();
	var $isUnique = true;
	var $count = 0;

	public function __construct($cnt=10)
	{
		$this->count = $cnt;
	}

	function set_unique_number_array($seltyp, $selnum, $selRandomNum)
	{
		//LogEntry("set numbers set: ".$this->count." seltype ", $seltype);

		for($i=0; $i<$this->count; $i++)
		{
			$isUnique = false;
			$cnt = 0;

			while (!$isUnique && $cnt < 20)
			{
				$isUnique = true;
				$randomNumber = Math_Logic_Get_RandomNumber($seltyp, $selnum, $selRandomNum, ($i % 2 == 0));
				for ($j = 0; $j < $i; $j++)
				{
					//check previous nums for uniqueness
					// 3 cases:
					//1: string equals e.g. "2" = "2"
					//2: Same but diff sign e.g.  '-1' = '1'
					//3: Fraction samw when simplified e.g.  '2/4' = '3/6'

					if ($randomNumber==$this->uniqueNumber[$j])
					{
						$isUnique = false;
						$cnt++;
					}
					else if (is_int($randomNumber))
					{
						$rt1 = abs($randomNumber);
						$rt2 = abs($this->uniqueNumber[$j]);
						if ($rt1 == $rt2)
						{
							$isUnique = false;
							$cnt++;
						}
					}
					else if ($seltyp == "230401" || $seltyp == "230601")
					{
						$decimal1 = intval(Math_Logic_Get_Decimal_Answer_From_Formatted_Value($randomNumber, "F") * 1000.00);
						$decimal2 = intval(Math_Logic_Get_Decimal_Answer_From_Formatted_Value($this->uniqueNumber[$j], "F") * 1000.00);

						if ($decimal1 == $decimal2)
						{
							$isUnique = false;
							$cnt++;
						}
						// if ($seltyp == "230401")
						// {
						// 	LogLine("number_set_01: ".$seltyp." ".($isUnique?"-UNIQ-":"-SAME-"),
						// 		$randomNumber." ".$this->uniqueNumber[$j]." ".$decimal1." ".$decimal2);
						// }
					}

					if (!$isUnique)
						break;
				}
			}

			$this->uniqueNumber[$i] = $randomNumber;
		}

		if (Math_Logic_Is_Factor($seltyp)) // e.g. division
		{
			for($i=0;$i<$this->count; $i++)
			{
				$this->uniqueNumber[$i] = $this->uniqueNumber[$i] * $selnum;
			}
		}

	}

	function get_unique_number_array($func)
	{
		if ($func == '1') // Not used
		{
			for($ix=0; $ix < count(	$this->uniqueNumber); $ix++)
			{
				$this->uniqueNumber[$ix] = ' ';
			}
		}

		return $this->uniqueNumber;
	}
}
?>
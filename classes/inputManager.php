<?php

require_once('BOM/Util.php');
require_once('BOM/Handler.php');
require_once('BOM/StreamFilter.php');

use duncan3dc\Bom\Util;

class InputManager
{
	var $newCode = true;

	var $runID = ' ';
	var $students = 1;
	var $groupparm = 0;
	var $grade = 0;
	var $currentPage = 0;

	// There are 7 possible selections: divide into 2 groups
	// So the most you can have for any one set is 5.
	// Each selection will be carried in the select= GET parm
	// When there are 6 Sets, Each set having 7 possible selection of 2 bytes:
	//   Select=0000 etc this will be 84 bytes plus 6 at the start and 4 at the end for other fields
	// See: Utility::get_literacy_offset() for the layout

	// 2 for: Early Primary selection arrays
	// ======================================

	var $inputSelectArray;  // Add /Sub etc - This is condensed: e.g. Group 01, Select 02 = 12, Early primary example
	var $inputSelectArraySave;  // Before condensing
	var $inputOfArray;  // Number from 0 - 12 etc

	var $inputAbstractCharacterArray;  // 1 char per set

	// 5 for: New selection arrays (for the expanded Math options)
	// ======================================

	var $inputOption1Array;  // Add Sub
	var $inputOption2Array;  // Number if needed
	var $inputOption3Array; // Number added by saturncube
	var $inputAbstractArray; // Y/N for each set
	var $inputExtra1Array;  // tba. See Math.pDF
	var $inputExtra2Array; // tba

	// General Purpose random numbers. Use this to pick random choices and values for the maths

	var $randomNumArray;

	var $literacyLevel = "0";
	var $inputTopic;
	var $topicName;

	// For processing the topics
	var $topicLines;
	var $questionArray;
	var $answerArray;
	var $namesArray;

    // getting the question characters
    var $questionString;


	function FindTopicName()
	{
		$topic = substr($this->topicName,2);
		return $topic;
	}

	function GetNoOfPages()
	{
		return $this->students;
	}

	function GetGroupParm()
	{
		return $this->groupparm;
	}

	function IsGroupParmSet()
	{
		return ($this->groupparm != 0);
	}

	function IsEarlyPrimary()
	{
		return $this->IsGroupParmSet() && $this->groupparm=="01";
	}
	function GetTableRowStyle($i=0)
	{
		// echo "<h1>".$this->IsGroupParmSet()."</h1>";
		// exit();

		$style='';
			$style.="style='";
			if($this->IsGroupParmSet())
			{
				if($i==1)
					$style.="display:table;width:100%;";
				elseif($i==0)
					$style.="display:table;";
				else
					$style.="display:none;width:100%;";
			}
			else if(!$this->IsGroupParmSet()) {
				$style.="display:none;";

			}
			else {
				$style.="display:table;";
			}
			$style.="'";
		return $style;
	}

	function GetTableRowClass($classIn)
	{
		if ($this->IsGroupParmSet() && $classIn=="left2")
			return "class=\"".$classIn."group".$this->groupparm."\"";
		else
			return "class=\"".$classIn.($this->IsGroupParmSet() ? "" : "help")."\"";
	}

	function GetGrade()
	{
		if ($this->grade == '1')
			return "Kindergarden";
		else
			return ($this->grade - 1);
		return $this->grade;
	}

	function SetNames($namesfile)
	{
		$old = getcwd();

		if ($namesfile == "")
		{
			if ($this->students < 1)
				$this->students = '6';

			$ix = 0;
			while ($ix < $this->students)
			{
				$this->namesArray[$ix] = "Student: . . . . . . . . . . ";
				$ix = $ix + 1;
			}
		}
		else
		{
			chdir('uploads');
			$this->namesArray = file($namesfile,FILE_SKIP_EMPTY_LINES);
			$this->students = count($this->namesArray);
		}


		chdir($old);
	}

	function ResetParameters($run)
	{
		$this->runID = $run;
		$this->currentPage = 0;
	}

	function GetCurrentPage()
	{
		return $this->currentPage;
	}

	function IsMorePages()
	{
		if ($this->currentPage < $this->students)
			return true;
		else
			return false;
	}

	function GetPageNumber()
	{
		$this->currentPage++;
		return $this->currentPage;
	}

	function GetStudentName()
	{
		return $this->namesArray[$this->currentPage-1];
	}

    function getQuestionString()
	{
		return $this->questionString;
	}

	function Get_Option_Number($fulltext)
	{
		if (strlen($fulltext) > Utility::SELECTION_KEY_LENGTH)
			return substr($fulltext, 0, Utility::SELECTION_KEY_LENGTH);
		else
			return $fulltext;
	}

	function Get_Option_Name($fulltext)
	{
		LogLine("GetOption",$fulltext);
		if (strlen($fulltext) >  Utility::SELECTION_KEY_LENGTH)
			return substr($fulltext, Utility::SELECTION_KEY_LENGTH);
		else
			return $fulltext;
	}

	function GetQuestionsAnswers($uploadfile, $studentNums)
	{
        $this->questionString = "";

		$old = getcwd();

		LogLine("GetQuestionsAnswers <<>>".$this->topicName."<<>>", $uploadfile);

		if ($uploadfile == "")
		{
			chdir('inputfiles');

			// dft values
			if ($this->topicName == '0' || $this->topicName == '00' || $this->topicName == '99' || $this->topicName == '04')
				$this->topicName = '04History';

			@$this->topicLines = file($this->Get_Option_Name($this->topicName) . '.txt', FILE_SKIP_EMPTY_LINES);
		}
		else
		{
			chdir('uploads');
			$test = getcwd();

			$this->topicLines = file($uploadfile,FILE_SKIP_EMPTY_LINES);
		}

		$counter = 0;
		$line_num = 0;
        $qaformat = 0;  // question then answer: must have 1. in the first 4 chars
        $ixq = 0;
        $ixa = 0;

		chdir($old);

		// print_r($this->topicLines);
		// exit();

		if(!empty($this->topicLines)){

			foreach ($this->topicLines as $line_num => $line) {

				$line = Util::removeBom($line);

				$first4chars = mb_substr($line,0,4);
				$pos = mb_strpos($first4chars, ".");
				if ($pos)
	            {
	                $ixq++;
	            }
	            else{
	                $ixa++;
	            }
	        }
		}

        if ($ixa == $ixq)
            $qaformat = 1;
        if(!empty($this->topicLines)){

	        foreach ($this->topicLines as $line_num => $line) {

				$line = Util::removeBom($line);

				$first4chars = mb_substr($line,0,4);

				$pos = mb_strpos($first4chars, ".");
				if ($pos) {

					$newstring = mb_substr($line, $pos + 1);
					$question = trim ($newstring);

					$this->questionArray[$counter] = $question;

					$counter = $counter + 1;
				}
				else {
					$line = trim ($line);
					if (mb_strlen($line) > 0) {
						$answer = $line;

						$this->questionArray[$counter] = $answer;

						$counter = $counter + 1;
					}
				}
			}
		}
		//*********************************************************
		// now check if the number of students is greater than the number of Q and As.
		//*********************************************************
		$originalsizeofarray = sizeof($this->questionArray);
		$sizeofarray = sizeof($this->questionArray);
		$newcounter = 0;
		$noQandAforStudents = intval($studentNums)* 2;

		while ($noQandAforStudents > $counter):
			$this->questionArray[$sizeofarray] = $this->questionArray[$newcounter];
			$sizeofarray ++;
			$newcounter ++;
			$counter ++;
			if ($newcounter >= $originalsizeofarray)
				$newcounter = 0;
		endwhile;

		return $this->questionArray;   // Array containing all questions and answers

	}


	//======================================
	/// Parameter SET and GET
	//======================================

	function SetParametersFromGet($run, $sessionData, $UploadFile, $GroupParmPosted="")
	{
		// SetDefaults is always called once
		// $sessionData is the select=00000... etc

		LogLine("at select READ", $sessionData);

		$this->SetAllGetFields($sessionData);

		return $this->StartFinal($UploadFile);
	}

	function SetParametersFromPost($run, $sessionData, $UploadFile, $setRandomDefaults=false)
	{
		// SetDefaults is always called once
		// $sessionData is the POST Array

		$this->SetAllPostFields($sessionData);

		if ($setRandomDefaults)
			$this->SetRandomValues();


		return $this->StartFinal($UploadFile);



	}

	function StartFinal($UploadFile)
	{



		// Other Default values

		if ($this->groupparm == '0')
			$this->groupparm = '00';

		if ($this->grade == '0' && $this->groupparm != '00')
		{
			// grade is no longer used
			if ($this->groupparm == '01')
				$this->grade = '02';
			if ($this->groupparm == '02')
				$this->grade = '04';
			if ($this->groupparm == '03')
				$this->grade = '08';
		}

		$this->SetDefaults(false);

		if (!$UploadFile)
		{
			if ($this->topicName == '0')
				$this->topicName = '04'; // History
			if ($this->literacyLevel == '0')
				$this->literacyLevel = '01';
		}


		return $this->GetSelectionsQueryString();
	}

	function Start($run)
	{
		$this->grade = '01'; // not used

		date_default_timezone_set('Australia/Sydney');

		$this->inputSelectArray = array();
		$this->inputSelectArraySave = array();
		$this->inputOfArray = array();
		$this->inputOption1Array = array();
		$this->inputOption2Array = array();
		$this->inputOption3Array = array();
		$this->inputAbstractArray = array();
		$this->inputExtra1Array = array();
		$this->inputExtra2Array = array();

		$this->randomNumArray = array();
		$this->inputAbstractCharacterArray = array();

		$this->ResetParameters($run);
	}

	function SetAllGetFields($sessionData)
	{
		if (strlen($sessionData) > 5)
		{
			$ix=0;

			$this->grade = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
			$ix+= Utility::SELECTION_KEY_LENGTH;
			$this->groupparm = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
			$ix+= Utility::SELECTION_KEY_LENGTH;
			$this->students = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
			$ix+= Utility::SELECTION_KEY_LENGTH;

			if ($this->groupparm=="01")
			{
				for($i=0;$i<UTILITY::SELECTION_TABLE_LEN; $i++)
				{
					$this->inputSelectArraySave[]  = $this->inputSelectArray[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputOfArray[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputOption1Array[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputOption2Array[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
					// echo $ix."<br>";

					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputAbstractArray[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputExtra1Array[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputExtra2Array[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
					$ix+= Utility::SELECTION_KEY_LENGTH;
				}
				$this->literacylevel = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				$ix+= Utility::SELECTION_KEY_LENGTH;
				$this->topicName = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				$ix+= Utility::SELECTION_KEY_LENGTH;
			}
			else if ($this->groupparm=="02" || $this->groupparm=="03")
			{
				foreach ($this->inputOption1Array as $key => $value) {

					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputOption1Array[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				}
				foreach ($this->inputOption2Array as $key => $value) {

					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputOption2Array[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				}
				foreach ($this->inputOption3Array as $key => $value) {

					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputOption3Array[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				}
				foreach ($this->inputAbstractArray as $key => $value) {

					$ix+= Utility::SELECTION_KEY_LENGTH;
					$this->inputAbstractArray[]  = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				}
				$this->literacylevel = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				$ix+= Utility::SELECTION_KEY_LENGTH;
				$this->topicName = substr($sessionData, $ix, Utility::SELECTION_KEY_LENGTH);
				$ix+= Utility::SELECTION_KEY_LENGTH;
			}
		}
	}

	function GetPostParm($sessionData, $key)
	{
		//LogLine("GetPostParm-1", $key);

		for ($ix=0; $ix < count($sessionData); $ix++)
		{
			if ($sessionData[$ix] == $key)
			{
				//LogLine("GetPostParm-2", $sessionData[$ix+1]);

				return $sessionData[$ix+1];
			}
		}
		return "99";
	}

	function RestoreAllInputs()
	{
		$end = count($this->inputSelectArray);
		if (count($this->inputSelectArraySave) == $end)
		{
			$i = 0;
			for($i=0; $i<$end; $i++)
			{
				$this->inputSelectArray[$i] = $this->inputSelectArraySave[$i];
			}
		}
	}

	function CondenseAllInputs()
	{
		if ($this->groupparm == "01") // early primary
		{
			// Only 3 sets: Question and number: 11, 12 ,13 or 14
			$end = count($this->inputSelectArray);
			$i = 0;
			for($i=0; $i<$end; $i++)
			{
				$save = $this->inputSelectArray[$i];

				if (strlen($this->inputSelectArray[$i]) > 1)
				{
					$this->inputSelectArray[$i] = '1'.substr($this->inputSelectArray[$i],1,1);

					LogLine("CONDENSE-2 ".$save, $this->inputSelectArray[$i] );
				}
			}
		}
		else
		{
			$grp = substr($this->groupparm,1,1);
			$end = count($this->inputOption1Array); // should be 6
			$i = 0;

			// echo "<pre>-".$end;
			// print_r($this->inputOption1Array);
			//exit();

			// print_r($this->inputOption2Array);
			// print_r($this->inputOption3Array);
			// print_r($this->inputAbstractArray);

			for($i=0; $i<$end; $i++)
			{
				$opt1 = $grp.substr($this->inputOption1Array[$i],1,1);

				$this->inputSelectArray[$i] = Math_Logic_Get_Internal_Code(
					$opt1,
					$this->inputOption2Array[$i],
					$this->inputOption3Array[$i],
					$this->inputAbstractArray[$i]);

				//print_r($this->inputSelectArray[$i]);
				//echo "-XXX-";
			}
		}
	}

	function SetAllPostFields($sessionData)
	{
		$this->SetDefaults(false);

		if (count($sessionData) > 5)
		{
			$this->grade = $this->GetPostParm($sessionData, "grade");
			$this->groupparm = $this->GetPostParm($sessionData, "groupparm");
			$this->students = $this->GetPostParm($sessionData, "student");
			$this->literacyLevel = $this->GetPostParm($sessionData, "literacy");
			$this->topicName = $this->GetPostParm($sessionData, "topic");

			if ($this->groupparm == "01") // early primary
			{
				// Only 3 sets: Question and number
				// '01que_1' '01num_1', 2 and 3
				$i = 0;
				for($i=0; $i<3; $i++)
				{
					$data = $this->GetPostParm($sessionData, "01que_".sprintf("%01d", $i+1));
					if ($data != "99")
						$this->inputSelectArray[$i] = $data;

					$data = $this->GetPostParm($sessionData, "01num_".sprintf("%01d", $i+1));
					if ($data != "99")
						$this->inputOfArray[$i] = $data;

				}
			}
			else if ($this->groupparm == "02")
			{
				$primary_selection = $this->GetPostParm($sessionData, "primary_selection");
				$operation = $this->GetPostParm($sessionData, "operation");
				$operation_2 = $this->GetPostParm($sessionData, "operation_2");
				$abstract = $this->GetPostParm($sessionData, "abstract");

				$i=0;
				$k=0;

				if ($primary_selection!="99") {

					foreach ($primary_selection as $key => $value) {
						$this->inputOption1Array[$key]=$primary_selection[$key];
						// if($primary_selection[$key]=="02"){
						// 	$this->inputOption2Array[$key]=$operation_2[$k];
						// 	$k++;
						// }
						// else{
							// $this->inputOption2Array[$key]=$operation[$key];
						// }

						if($primary_selection[$key]=="02"){
							if(empty($operation_2[$k])) $operation_2[$k]="00";
							$this->inputOption3Array[$key]=$operation_2[$k];
							$k++;
						}
						else{
							$this->inputOption3Array[$key]="00";
						}

						$this->inputOption2Array[$key]=$operation[$key];

						if($primary_selection[$key]=="01" || $primary_selection[$key]=="02") {
							if(empty($abstract[$i])) $abstract[$i]='00';
							$this->inputAbstractArray[$key]=$abstract[$i];
							$i++;
						}
						else{
							$this->inputAbstractArray[$key]="00";
						}
					}
				}
				else {
					for ($i=0; $i <Utility::SELECTION_TABLE_LEN_PRIMARY ; $i++) {
						$this->inputOption1Array[$i]="00";
						$this->inputOption2Array[$i]="00";
						$this->inputOption3Array[$i]="00";
						$this->inputAbstractArray[$i]="00";
					}
				}

			}
			else if($this->groupparm == "03")
			{
				$primary_selection = $this->GetPostParm($sessionData, "primary_selection");
				$operation = $this->GetPostParm($sessionData, "operation");
				$operation_2 = $this->GetPostParm($sessionData, "operation_2");
				$abstract = $this->GetPostParm($sessionData, "abstract");

				$i=0;
				$k=0;

				if ($primary_selection!="99") {

					foreach ($primary_selection as $key => $value) {

						$this->inputOption1Array[$key]=$primary_selection[$key];
						// if($primary_selection[$key]=="02"){
						// 	$this->inputOption2Array[$key]=$operation_2[$k];
						// 	$k++;
						// }
						// else{
							// $this->inputOption2Array[$key]=$operation[$key];
						// }

						if($primary_selection[$key]=="01"){
							if(empty($operation_2[$k])) $operation_2[$k]="00";
							$this->inputOption3Array[$key]=$operation_2[$k];
							$this->inputAbstractArray[$key]=$abstract[$k];
							$k++;
						}
						else{
							$this->inputOption3Array[$key]="00";
							$this->inputAbstractArray[$key]="00";
						}
						$this->inputOption2Array[$key]=$operation[$key];
					}
				}
				else {
					for ($i=0; $i <Utility::SELECTION_TABLE_LEN_PRIMARY ; $i++) {
						$this->inputOption1Array[$i]="00";
						$this->inputOption2Array[$i]="00";
						$this->inputOption3Array[$i]="00";
						$this->inputAbstractArray[$i]="00";
					}
				}
			}
		}
	}

	function SetRandomNumbers($setQty)
	{

        //echo "<pre>-";
        //print_r($this->inputSelectArray);
        //print_r($this->inputOfArray);
        //print_r($this->randomNumArray);

        //exit();

        $i = 0;
		for($i=0; $i<3; $i++)
		{
            if (!$this->IsEarlyPrimary())
                $this->inputOfArray[$i] = Math_Logic_Set_TestCase($this->groupparm, $this->inputSelectArray[$i], 3); // NOTE 3 = Live

			$this->randomNumArray[$i]  = Math_Logic_Set_TestCase($this->groupparm, $this->inputSelectArray[$i], 4, $this->inputOfArray[$i]); // NOTE 4 = Live
		}

		if ($setQty == "6")
		{
			for($i=3; $i<6; $i++)
			{
				$this->inputOfArray[$i] = Math_Logic_Set_TestCase($this->groupparm, $this->inputSelectArray[$i], 3); // NOTE 3 = Live
				$this->randomNumArray[$i]  = Math_Logic_Set_TestCase($this->groupparm, $this->inputSelectArray[$i], 4, $this->inputOfArray[$i]); // NOTE 4 = Live
			}
		}

         //echo "<pre>-";
         //print_r($this->inputSelectArray);
         //print_r($this->inputOfArray);
         //print_r($this->randomNumArray);

         //exit();
	}

	function ChooseAbstractCharacters()
	{
		// Must be Unique
		$i = 0;
		$cnt = 0;
		for($i=0; $i<6; $i++)
		{
			if ($i == 0)
				$this->inputAbstractCharacterArray[$i] = utility_get_abstract_item();
			else
			{
				while (true)
				{
					$hit = false;
					$item = utility_get_abstract_item();
					for($ix=0; $ix<$cnt; $ix++)
					{
						if ($this->inputAbstractCharacterArray[$ix] == $item)
						{
							$hit = true;
							break;
						}
					}
					if (!$hit)
					{
						$this->inputAbstractCharacterArray[$i] = $item;
						break;
					}
				}
			}

		}
	}

	function SetTestRandomValues($ix)
	{
		// Only 3 sets when testing
		$hit = false;
		$i = 0;
		for($i=0; $i<3; $i++)
		{
			$this->inputSelectArray[$i] = Math_Logic_Set_TestCase($this->groupparm, $ix, 1);
			$this->inputOfArray[$i] = Math_Logic_Set_TestCase($this->groupparm, $ix, 2); // NOTE 2 = Test
			$this->randomNumArray[$i]  = Math_Logic_Set_TestCase($this->groupparm, $this->inputSelectArray[$i], 4, $this->inputOfArray[$i]); // NOTE 4 = Live

			if ($this->inputSelectArray[$i]=='230401')
				$hit = true;
		}

		// if ($hit)
		// {
		// echo "<pre>-";
		// print_r($this->inputSelectArray);
		// print_r($this->inputOfArray);
		// print_r($this->randomNumArray);
		// exit();
		// }
	}


	function SetRandomValues()
	{
		LogLine("SetRandomValues - START", $this->GetSelectionsQueryString());

		srand($this->make_seed());

		if ($this->groupparm == "01") // early primary
		{
            //// Only 3 sets: Question and number
            $i = 0;
            for($i=0; $i<3; $i++)
            {
                if ($this->inputSelectArray[$i] == '99' || $this->inputSelectArray[$i] == '00')
                    $this->inputSelectArray[$i] = sprintf("%01d", rand(1, 4));

                if ($this->inputOfArray[$i] = '99' || $this->inputOfArray[$i] = '00')
                    $this->inputOfArray[$i] = sprintf("%01d", rand(1, 12));
            }
		}
		if ($this->groupparm=="02") {
			for ($i=0; $i <6 ; $i++)
			{
				if($this->inputOption1Array[$i]=="99" || $this->inputOption1Array[$i]=="00" ||empty($this->inputOption1Array[$i]))
				{
					$this->inputOption1Array[$i]="0".rand(1,5);
				}
				if($this->inputOption2Array[$i]=="99" || $this->inputOption2Array[$i]=="00" || empty($this->inputOption2Array[$i]))
				{
					$maxNum=6;
					if($this->inputOption1Array[$i]=="02")
						$maxNum=3;
					$this->inputOption2Array[$i]="0".rand(1,$maxNum);

				}
				if ($this->inputOption3Array[$i]=="99" ||$this->inputOption3Array[$i]=="00" || empty($this->inputOption3Array[$i])) {

					if ($this->inputOption1Array[$i]=="02" && ($this->inputOption2Array[$i]=="01" ||$this->inputOption2Array[$i]=="02" ||$this->inputOption2Array[$i]=="03"))
					{
						$this->inputOption3Array[$i]="0".rand(1,5);
					}
				}
				if ($this->inputAbstractArray[$i]=="99" || $this->inputAbstractArray[$i]=="00")
				{

					if ($this->inputOption1Array[$i]=="01" ||$this->inputOption1Array[$i]=="02")
					{
						$this->inputAbstractArray[$i]="0".rand(1,2);
					}

				}
			}

			// echo "<pre>";
			// print_r($this->inputOption1Array);
			// print_r($this->inputOption2Array);
			// print_r($this->inputOption3Array);
			// print_r($this->inputAbstractArray);
			// exit();


		}
		if ($this->groupparm=="03") {

			for ($i=0; $i <6 ; $i++) {

				if($this->inputOption1Array[$i]=="99" || $this->inputOption1Array[$i]=="00" ||empty($this->inputOption1Array[$i]))
				{
					$this->inputOption1Array[$i]="0".rand(1,5);
				}
				if($this->inputOption2Array[$i]=="99" || $this->inputOption2Array[$i]=="00" ||empty($this->inputOption2Array[$i]))
				{
					$maxNum=6;
					if($this->inputOption1Array[$i]=="01")
						$maxNum=3;
					$this->inputOption2Array[$i]="0".rand(1,$maxNum);
				}

				if ($this->inputOption3Array[$i]=="99" ||$this->inputOption3Array[$i]=="00" || empty($this->inputOption3Array[$i])) {

					if ($this->inputOption1Array[$i]=="01" && ($this->inputOption2Array[$i]=="01" ||$this->inputOption2Array[$i]=="02" ||$this->inputOption2Array[$i]=="03"))
					{
						$this->inputOption3Array[$i]="0".rand(1,5);
					}
				}
				if ($this->inputAbstractArray[$i]=="99" || $this->inputAbstractArray[$i]=="00")
				{
					if ($this->inputOption1Array[$i]=="01" && ($this->inputOption2Array[$i]=="01" ||$this->inputOption2Array[$i]=="02" ||$this->inputOption2Array[$i]=="03"))
					{
						$this->inputAbstractArray[$i]="0".rand(1,2);
					}
				}
			}
		}
		LogLine("SetRandomValues - END", $this->GetSelectionsQueryString());
	}

	function make_seed()
	{
		list($usec, $sec) = explode(' ', microtime());
		return $sec + $usec * 1000000;
	}

	function SetDefaults($force)
	{
		if ($this->students == '0' || $force)
			$this->students = '1';

		$i = 0;
		for($i=0;$i<UTILITY::SELECTION_TABLE_LEN; $i++)
		{
			$this->randomNumArray[]  = rand(1, 12);
		}

		if (Count($this->inputSelectArray) == 0 || $force)
		{
			$i = 0;
			for($i=0;$i<UTILITY::SELECTION_TABLE_LEN; $i++)
			{
				$this->inputSelectArray[]  = "00";
				$this->inputOfArray[]  = "00";
				$this->inputOption1Array[]  = "00";
				$this->inputOption2Array[]  = "00";
				$this->inputOption3Array[]  = "00";
				$this->inputAbstractArray[]  = "00";
				$this->inputExtra1Array[]  = "00";
				$this->inputExtra2Array[]  = "00";
			}
		}

		if ($this->groupparm == '1')
			$this->groupparm == '01';
		if ($this->groupparm == '2')
			$this->groupparm == '02';
		if ($this->groupparm == '3')
			$this->groupparm == '03';
	}

	function GetSelectionsQueryString()
	{
		// the order must match the index parm found in index.php.  E.g.
		//
		//  From index.php:
		//
		//   ten_questions_type_listbox('question_a_type', $selectInfo, 2)
		//
		//  So question-a-type is at slot 2.
		//
		// sprintf("%01d", $ix);

		$format = (Utility::SELECTION_KEY_LENGTH == 3 ? "%03d" : "%02d");
		$rt='';
		if($this->groupparm=="01") {

			$rt = sprintf($format, $this->grade);
			$rt = $rt.sprintf($format, $this->groupparm);
			$rt = $rt.sprintf($format, $this->students);

			$i = 0;
			for($i=0; $i<UTILITY::SELECTION_TABLE_LEN; $i++)
			{
				$rt = $rt.sprintf($format, $this->Get_Option_Number($this->inputSelectArray[$i]));
				$rt = $rt.sprintf($format, $this->Get_Option_Number($this->inputOfArray[$i]));
				$rt = $rt.sprintf($format, $this->Get_Option_Number($this->inputOption1Array[$i]));
				$rt = $rt.sprintf($format, $this->Get_Option_Number($this->inputOption2Array[$i]));

				$rt = $rt.sprintf($format, $this->Get_Option_Number($this->inputAbstractArray[$i]));
				$rt = $rt.sprintf($format, $this->Get_Option_Number($this->inputExtra1Array[$i]));
				$rt = $rt.sprintf($format, $this->Get_Option_Number($this->inputExtra2Array[$i]));
			}
		}
		else if($this->groupparm=="02" || $this->groupparm=="03")
		{
			$rt = sprintf($format, $this->grade);
			$rt = $rt.sprintf($format, $this->groupparm);
			$rt = $rt.sprintf($format, $this->students);

			foreach ($this->inputOption1Array as $key => $value) {
				$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			}
			foreach ($this->inputOption2Array as $key => $value) {
				$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			}
			foreach ($this->inputOption3Array as $key => $value) {
				$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			}
			foreach ($this->inputAbstractArray as $key => $value) {
				$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			}
		}

		$rt = $rt.sprintf($format, $this->Get_Option_Number($this->literacyLevel));
		$rt = $rt.sprintf($format, $this->Get_Option_Number($this->topicName));  // only uses 2 bytes

			// $rt = sprintf($format, $this->grade);
			// $rt = $rt.sprintf($format, $this->groupparm);
			// $rt = $rt.sprintf($format, $this->students);

			// foreach ($this->inputOption1Array as $key => $value) {
			// 	$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			// }

			// foreach ($this->inputOption2Array as $key => $value) {
			// 	$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			// }
			// foreach ($this->inputOption3Array as $key => $value) {
			// 	$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			// }
			// foreach ($this->inputAbstractArray as $key => $value) {
			// 	$rt = $rt.sprintf($format, $this->Get_Option_Number($value));
			// }

		LogLine("at select build", $rt);

		// print_r($rt);
		// exit();


		return $rt;
	}




}
?>
<?php

class combine_letters_nums {

	var $mycolumns;

	public function __construct($letters, $numbers, $nobold)
	{
		$temp_numberArray[0] = "";  //to check for duplicate numbers
		$cnt = count($numbers);

		for ($i=0; $i<$cnt;$i++)
		{
			$duplicate_num = in_array($numbers[$i],$temp_numberArray);
			$temp_numberArray[$i] = $numbers[$i];

			if ($duplicate_num & $nobold)
			{
				$key = array_search($numbers[$i], $numbers);
				$temp = new letter_num_pair($numbers[$i], $letters[$key]);
				$this->mycolumns[$letters[$i]] = $temp;
			}
			else
			{
				$temp = new letter_num_pair($numbers[$i], $letters[$i]);
				$this->mycolumns[$letters[$i]] = $temp;
			}
		}
	}
	public function get_array()
	{
		return $this->mycolumns;
	}

	public function get_letter($key)
	{
		if (is_null($key))
			return " ";
		if (!array_key_exists($key, $this->mycolumns))
			return " ";

		$templetter = $this->mycolumns[$key];
		if (is_null($templetter))
			return " ";
		else
		return $templetter->get_letter_val();
	}

	public function get_number($key)
	{
		if (is_null($key))
			return " ";

		if (!array_key_exists($key, $this->mycolumns))
			return " ";

		$tempnumber = $this->mycolumns[$key];
		if (is_null($tempnumber))
			return " ";
		else
			return $tempnumber->get_number_val();
	}

}

?>
<?php

//
// Constants
//
class Utility
{
	const SELECTION_KEY_LENGTH = 2;
	const SELECTION_TABLE_LEN = 3; // No of Sets: There will be 6 for Secondary
	const SELECTION_TABLE_LEN_PRIMARY = 6; // added by Nitesh saturncube
	const SELECTION_TABLE_OFFSET = 7; // No of selection fields / Early primary and Secondary e.g. 2 + 5

	const STANDARD_LINE_LENGTH = 32;
	const STANDARD_LINE_QTY = 4;

	const STANDARD_SETS_PER_ROW = 3;

	const Abstract_List = [    
		'←', '↑', '→', '↓', '⌂', '▲', '►', '▼', '◄', 
		'☺', '☻', '♀', '♂', '♣', '♠', '♥','♦', '♯', 
		 '§','¶', 'Ø', 'Þ',  'Ɣ', 'Ɵ', 'Ʃ', 'Ʉ', 'Ʌ', 
		'ɷ', 'ʘ', 'ʭ', 'Δ', 'Φ', 'Ψ', 'Ω', '֍', '۞', '۩', 'ᴥ',
		'†', '‡', '₼', '₪'    ];


		// PHP program to add 
// 2 fractions 

// ========================================================
// USAGE:
// ========================================================

// Driver Code 
// $num1 = 1; $den1 = 500; 
// $num2 = 2; $den2 = 1500; 
// $den3; $num3; 
//
//  addFraction($num1, $den1, $num2, $den2, $num3, $den3); 
//
// 	echo $num1, "/", $den1, " + ", 
// 		$num2,"/", $den2, " is equal to ", 
// 			$num3, "/", $den3, "\n"; 
			



// Function to return 
// gcd of a and b 
public static function gcd($a, $b) 
{ 
	if ($a == 0) 
		return $b; 
	return Utility::gcd($b % $a, $a); 
} 

// Function to convert the 
// obtained fraction into 
// it's simplest form 
public static function lowest(&$den3, &$num3) 
{ 
	// Finding gcd of both terms 
	$common_factor = Utility::gcd($num3, $den3); 

	// Converting both terms 
	// into simpler terms by 
	// dividing them by common factor 
	
	$den3 = (int) $den3 / $common_factor; 
	$num3 = (int) $num3 / $common_factor; 
} 
}

function utility_get_abstract_item()
{
	return trim(Utility::Abstract_List[rand(0, count(Utility::Abstract_List)-1)]);
}

//
function get_letters_max()
{
	return Utility::STANDARD_LINE_LENGTH * Utility::STANDARD_LINE_QTY;
}
//
function get_select_key_offset($keyname)
{
	// These are all before or after the repeating group
	// ==================================================
	if ($keyname == "grade")
		return 0;
	if ($keyname == "student")
		return Utility::SELECTION_KEY_LENGTH * 2;
	if ($keyname == "primary_selection")
		return Utility::SELECTION_KEY_LENGTH*3;
	if ($keyname == "operation")
		return Utility::SELECTION_KEY_LENGTH*9;
	if ($keyname == "abstract")
		return Utility::SELECTION_KEY_LENGTH*21;
	if ($keyname == "operation_2") 
		return Utility::SELECTION_KEY_LENGTH*15;
	if ($keyname == "groupparm")
		return Utility::SELECTION_KEY_LENGTH;
	if ($keyname == "literacy")
		return get_literacy_offset();
	if ($keyname == "topic")
		return get_topic_offset();

	// These are all between and repeat up to 9
	// ==========================================

	$len = strlen($keyname);
	$grp = (substr($keyname, $len-1, 1)) - 1;  // if zero then no offset !



	// Move to the start of the current group

	$pos = (3 * Utility::SELECTION_KEY_LENGTH) + (Utility::SELECTION_TABLE_OFFSET * Utility::SELECTION_KEY_LENGTH * $grp);

	// We are now at the start of the group so get the element

	$firstPart = substr($keyname, 0, 5);

	// Early primary
	// =========================

	if ($firstPart == "01que")
		return $pos;
	if ($firstPart == "01num")
		return $pos + Utility::SELECTION_KEY_LENGTH;

	// Primary and Secondary
	// =========================
	

	return $pos;
}
//
function get_literacy_offset()
{
	// 3 in the first group
	return (3 * Utility::SELECTION_KEY_LENGTH) + 
		(Utility::SELECTION_TABLE_OFFSET * Utility::SELECTION_KEY_LENGTH * Utility::SELECTION_TABLE_LEN);  
}
//
function get_topic_offset()
{
	return get_literacy_offset() + Utility::SELECTION_KEY_LENGTH;
}
//   Add a key to the Query String
//
///
function add_or_change_parameter($parameter, $value)
{
	$params = array();
	$output = "?";
	$firstRun = true;
	foreach($_GET as $key=>$val)
	{
		if($key != $parameter)
		{
			if(!$firstRun)
			{
				$output .= "&";
			}
			else
			{
				$firstRun = false;
			}
			$output .= $key."=".urlencode($val);
		}
	}
	if(!$firstRun)
		$output .= "&";

	$output .= $parameter."=".urlencode($value);

	LogLine("add_or_change_parameter", $output);
	
	return htmlentities($output);
}

///
//   Return a single key from the Query String
//
///
function is_parameter($parameter)
{
	//$params = array();
	foreach($_GET as $key=>$val)
	{
		if($key == $parameter)
		{
			return true;
		}
	}
	return false;
}

///
//   Return a single key from the Query String
//
///
function get_parameter($parameter)
{
	//$params = array();
	foreach($_GET as $key=>$val)
	{
		if($key == $parameter)
		{
			return (urldecode($val));
		}
	}
	return '';  // the default
}
///
//   Return all relevant stuff from the last POST
//
///
function get_post_parameters()
{
	$params = array();
	$ix = 0;
	file_put_contents("files/test.txt"," \n\r params == \n\r ", FILE_APPEND);

	foreach($_POST as $key=>$val)
	{
		$params[$ix++] = $key;
		$params[$ix++] = $val;

		if(is_array($val)) {

			foreach ($val as $key1 => $value) {
				file_put_contents("files/test.txt"," \n\r ".$key."->".$key1." == ".$value." \n\r ", FILE_APPEND);
			}
		}
		else {
			file_put_contents("files/test.txt"," \n\r ".$key." == ".$val." \n\r ", FILE_APPEND);
		}

	}
	
	return $params;
}
///
//   Return the worksheetname given an id
//
///
function get_worksheet($id, $forLink)
{
	if ($forLink)
		return '../files/worksheet'.$id.'.pdf';
	else
		return 'files/worksheet'.$id.'.pdf';
}

///
//   Return the worksheetname given an id
//
///
function get_view_worksheet($id, $forLink)
{
	if ($forLink)
		return '../files/viewworksheet'.$id.'.pdf';
	else
		return 'files/viewworksheet'.$id.'.pdf';
}

///
//   Return the worksheetname given an id
//
///
function get_print_worksheet($id, $forLink)
{
	if ($forLink)
		return '../files/printworksheet'.$id.'.pdf';
	else
		return 'files/printworksheet'.$id.'.pdf';
}

function get_print_worksheet_link($id, $forLink)
{
	$rt = "<p style="."padding-left:15px;"."> <span class=\"sheet_click\">Scroll the completed document below to check your messages fit and";
	$rt2 = " then <a target='_blank' href=\"".get_print_worksheet($id, $forLink)."\"> Click here</a> to print your student worksheets.  The initial pages will show the messages you used in this set of generated worksheets.</span></p>";
	return $rt.$rt2;
}

function unserialize_handler($errno, $errstr)
{
//	echo "Invalid serialized value.\n";
}


///
//   Clean up the worksheets
//
///
function cleanup_worksheets()
{
	$path = 'files';
	$dh = opendir("$path/");
	while (false !== ($filename = readdir($dh)))
	{
		$files[] = $filename;
	}

	if (count($files) > 520)
	{
		$old = getcwd();
		chdir($path);

		$rt = 0;
		rsort($files);

		set_error_handler('unserialize_handler');

		for($ix=0;$ix<count($files);$ix++)
		{

			if ($files[$ix]=='.' ||
				$files[$ix]=='..' ||
				is_dir($files[$ix]) ||
				strcasecmp($files[$ix],'emptyworksheet.pdf') == 0)
			{
				// skip these
			}
			else
			{
				if ($rt > 10)
				{
					if (is_file($files[$ix]))
					{
						try
						{
							unlink($files[$ix]);
						}
						catch (Exception $e)
						{
						}
					}
				}
				$rt++;
			}
		}

		restore_error_handler();

		chdir($old);
	}
	closedir($dh);
}

///
//   Return the next worksheetnumber
//
///
function get_worksheet_number()
{
	$dh = opendir('files/');
	while (false !== ($filename = readdir($dh)))
	{
		$files[] = $filename;
	}

	rsort($files);

	$rtstart = 1000;

	// Get the highest
	for($ix=0;$ix<count($files);$ix++)
	{
			if ($files[$ix]=='.' ||
				$files[$ix]=='..' ||
				is_dir($files[$ix]) ||
				strcasecmp($files[$ix],'emptyworksheet.pdf') == 0)
			{
				// skip these
			}
			else
			{
				$exit = false;
				if (strlen($files[$ix]) > 4)
				{
					if (substr($files[$ix],strlen($files[$ix])-4) == ".pdf")
					{
						$rt = substr($files[$ix],strlen($files[$ix])-8,4);
					if (is_numeric($rt))
							$exit=true;
					}
				}

				if ($exit)
			{
				return $rt + 1;
			}

			}
	}
	closedir($dh);

	return 	$rtstart + count($files);
}

function formatQuestions ($inputString, $numBoxes=Utility::STANDARD_LINE_LENGTH, $lines=Utility::STANDARD_LINE_QTY) 
{
	LogLine("formatQ ".$numBoxes, $inputString);

	$pos = 0;
	$lineNo = 0;
	$inputStringFinished = false;
	$lineComplete[$lineNo] = $inputString;
	$numBoxesAdd1 = $numBoxes + 1;
	$numBoxesSubtract1 = $numBoxes - 1;

	if (mb_strlen($inputString) < $numBoxesAdd1) {
		$lineComplete[0] = finishLine($inputString, 0, $numBoxes);
		$lineNo = 99;  // dont get into the while loop
	}

	while ($lineNo <= $lines-1):

		LogEntry("START-no-data", $lineNo." ".$inputString);

		if (mb_strlen($inputString) < $numBoxesAdd1) 
		{
			$lineComplete[$lineNo] = finishLine($inputString, $lineNo-1, $numBoxes);
			
		//	LogEntry("linecomplete", $lineNo." ".$lineComplete[$lineNo]);

			break;
		}

		while (mb_strlen($inputString) > $numBoxes):

			if (mb_substr($inputString,$numBoxesSubtract1,1) != " "  &&
				mb_substr($inputString,$numBoxes,1) != " ")
			{
				$pos = mb_strrchr(mb_substr($inputString,0,$numBoxes), " ");
				$countpos = mb_strlen($pos);
				$lineComplete[$lineNo] = mb_substr($inputString,0,$numBoxes-$countpos);
				for ($j=0; $j<mb_strlen($pos); $j++)
					$lineComplete[$lineNo] = $lineComplete[$lineNo].' ';

				//	LogEntry("linecomplete", $lineNo." ".$lineComplete[$lineNo]);

				$inputString = startLine($inputString,$numBoxes-$countpos+1);
			}
			else 
			{
				$lineComplete[$lineNo] = mb_substr($inputString,0,$numBoxes);

			//	LogEntry("linecomplete", $lineNo." ".$lineComplete[$lineNo]);

				$inputString = startLine($inputString,$numBoxes);
			}

			$lineNo ++;

		endwhile;

	endwhile;

	return $lineComplete;
}

function startLine($line, $startpos){
	if (mb_substr($line,$startpos,1) == " ") {
		return mb_substr($line, $startpos + 1);
	}
	else
		return mb_substr($line, $startpos);

}
function finishLine($ln, $lnNo, $boxes){
	$lastline = mb_strlen($ln);
	for ($j=0; $j<$boxes-$lastline; $j++) {
		$ln = $ln.' ';
	}
	return  $ln;

}

function utilGetQuestionIx($pageNum)
{
    return ($pageNum - 1) * 2;
}

function GetCharacterLength($data)
{
	$ix = 0;
	$len = mb_strlen($data);

	for($i=0;$i<$len; $i++)
	{
		$txt = mb_substr($data, $i, 1, "UTF-8"); 
		if ($txt != " ")
		$ix++;
	}
	return $ix;
}


function GetCharacterLengthArray($parts)
{
	$afterLen = 0;
	$count = count($parts);
	for ($i = 0; $i < $count; $i++) 
	{
		$afterLen += GetCharacterLength($parts[$i]);

		LogEntry("XXX".$i, $parts[$i]);
	}
	return $afterLen;
}


function StripLeadingNumber($text)
{
	LogEntry("txt",$text);

	//if (mb_strlen($text < 4))
	//	return $text;

	$first4chars = mb_substr($text,0,4);

	LogEntry("$first4chars",$first4chars);

	$pos = mb_strpos($first4chars, ".");

	LogEntry("pos",$pos);

	if ($pos > -1) 
	{
		return mb_substr($text, $pos+1);
	}
	return $text;
}

function LogStart($text)
{
	file_put_contents("files/test.txt"," ====== [".$text."] ====== \n\r");		
}

function LogEntry($from="null", $text)
{
	if ($from=="null")
		file_put_contents("files/test.txt"," [".$text."] ", FILE_APPEND);		
	else
		file_put_contents("files/test.txt"," [".$from."]-[".$text."]", FILE_APPEND);		
}

function LogLine($from="null", $text)
{
	if ($from=="null")
		file_put_contents("files/test.txt"," \n\r [".$text."] \n\r", FILE_APPEND);		
	else
		file_put_contents("files/test.txt"," \n\r [".$from."]-[".$text."] \n\r", FILE_APPEND);		
}

function IsEarlyPrimary($sessionData)
{
	if (strlen($sessionData > 5))
	{
		$rt = $sessionData[3];
		return ($rt=="01");
	}
	return false;
}

function IsPrimary($sessionData)
{
	if (strlen($sessionData > 5))
	{
		$rt = $sessionData[3];
		return ($rt=="02");
	}
	return false;
}

function isSecondary($sessionData)
{
	if (strlen($sessionData > 5))
	{
		$rt = $sessionData[3];
		return ($rt=="03");
	}
	return false;
}

function GetStaticJsContent($from="null")
{
	if ($from=="null")
	{
		return '
		$("#slideshow > div:gt(0)").hide();
		setInterval(function() {
		$("#slideshow > div:first")
		  .removeClass("hidden")
		  .fadeOut(1000)
		  .next()
		  .fadeIn(1000)
		  .end()
		  .appendTo("#slideshow")
		  .show();
	 	 }, 10000);
	  ';	
	}
}

function GetStaticCssContent($from="null")
{
	if ($from=="null")
	{
		return '
		body{margin: 0;padding: 0;font: 100% Verdana, Arial, Helvetica, sans-serif;font-size: 14px;}
		div#gallery{width: 1000px;margin: 40px auto;text-align: center;}
		div#gallery img{margin: 20px;border: 2px;}
		div#gallery p{color: #004694;}
		div#gallery div.pn{padding: 10px;margin: 0 5px;border-top: 1px #ccc;}
		a{color:#333;}
		a:hover{color:#cc0000;}
		a.sp{padding-right: 40px;}
	   
		
	  #slideshow {
	  margin: 0 auto;
	  position: relative;
	  padding: 10px;
	  /* box-shadow: 0 0 20px rgba(0, 0, 0, 0.4); */
		}
	
	#slideshow > div {
	  position: absolute;
	  top: 10px;
	  left: 10px;
	  right: 10px;
	  bottom: 10px;
	}
	
	div.hidden
	{
	   display: none
	}';
	
	}
	else
		file_put_contents("files/test.txt"," \n\r [".$from."]-[".$text."] \n\r", FILE_APPEND);		
}

/*
	slideshow class, can be used stand-alone
*/
class slideshow
{
	private $files_arr = NULL;
	private $files_h_arr = NULL;
	private $files_m_arr = NULL;
	private $files_l_arr = NULL;
	private $title_a_arr = NULL;  // artist name
	private $title_p_arr = NULL;  // painting name

	private $err = NULL;
	private $wid = 660;
	private $hgt = 660;
	private $sho = 1;
	private $gal = 0;
	private $name = "";
	
	public function __construct(&$err)
	{
		$this->files_arr = array();
		$this->files_h_arr = array();
		$this->files_m_arr = array();
		$this->files_l_arr = array();
		$this->title_a_arr = array();
		$this->title_p_arr = array();
		$this->err = $err;
	}
	public function init($nam)
	{
		// run actions only if img array session var is empty
		// check if image directory exists
		if (strlen($nam) < 1)
		{
			return 'Image Folder must be specified: &nam=';
		}
		if (!$this->dir_exists())
		{
			return 'Error retrieving images, missing directory';
		}

		$this->name = $nam;

		return '';
	}
	public function get_file($ix)
	{    
    	return $this->files_arr[$ix];
	}
	public function get_height($ix)
	{    
    	return $this->files_h_arr[$ix];
	}
	public function get_margin($ix)
	{    
    	return $this->files_m_arr[$ix];
	}	
	public function get_marginl($ix)
	{    
    	return "0"; // $this->files_l_arr[$ix];
	}

    public function get_images($widin, $hgtin, $shoin, $galin)
	{
		// run actions only if img array session var is empty
		if (!is_null($widin))
			$this->wid = $widin;
		if (!is_null($hgtin))
			$this->hgt = $hgtin;
		if (!is_null($shoin) && $shoin == 1)
			$this->sho = $shoin;
		if (!is_null($galin) && $galin == 1)
			$this->gal = $galin;

		if (isset($_SESSION['imgarr']))
		{
			$this->files_arr = $_SESSION['imgarr'];
			$this->files_h_arr = $_SESSION['imgharr'];
			$this->files_m_arr = $_SESSION['imgmarr'];
			$this->files_l_arr = $_SESSION['imglarr'];
			$this->title_a_arr = $_SESSION['imgtaarr'];
			$this->title_p_arr = $_SESSION['imgtparr'];
		}
		else
		{
			if ($dh = opendir(IMGDIR))
			{
				$namesArray = null;

				$namesArray = file(IMGDIR.'ArtistNames_'.$this->name.'.csv',FILE_SKIP_EMPTY_LINES);

				$limit = count($namesArray);

				while (false !== ($file = readdir($dh)))
				{
					if (preg_match('/^.*\.(jpg|jpeg|gif|png)$/i', $file))
					{
						$this->files_arr[] = $file;

						list($width, $height, $type, $attr) = getimagesize(IMGDIR.$file);

						if ($height > $this->hgt)
						{
							$this->files_h_arr[] = 100 * ($this->hgt / $height) ;
							$this->files_m_arr[] = 5;
						}
						else
						{
							$this->files_h_arr[] = 95;
							$this->files_m_arr[] = ($this->hgt - $height) / 2 ;
						}

						if ($width < ($this->wid / 2))
						{
							$this->files_l_arr[] = 50 + ($this->wid / 3);
						}
						else
							$this->files_l_arr[] = 0;

						$t1 = $this->get_before_space($file);

						for ($ix=0; $ix <$limit; $ix++ )
						{
							if ($this->find_artist($namesArray[$ix], $t1))
							{
								$this->title_a_arr[] = $this->get_artist($namesArray[$ix]);
								$this->title_p_arr[] = $this->get_title($namesArray[$ix]);
								break;
							}
						}	
					}
				}				
				closedir($dh);
			}
			$_SESSION['imgarr'] = $this->files_arr;
			$_SESSION['imgharr'] = $this->files_h_arr;
			$_SESSION['imgmarr'] = $this->files_m_arr;
			$_SESSION['imglarr'] = $this->files_l_arr;
			$_SESSION['imgtaarr'] = $this->title_a_arr;
			$_SESSION['imgtparr'] = $this->title_p_arr;
		}
	}

	private function find_artist($text1, $text2)
	{
		$t2 = $this->get_before_space($text1);
		return ($t2 == $text2);
	}


	private function get_before_space($text)
	{
		$len = strlen($text);
		$commas = 0;	
		$start = 0;	
		for ($ix=0; $ix<$len; $ix++)
		{
			if (substr($text,$ix,1) == ',')
				$commas++;
			if (substr($text,$ix,1) == ' ')
				$commas++;

			if ($commas > 0)
			{
				return substr($text, 0, $ix);
			}
		}
		return "";
	}

	private function get_artist($text)
	{
		$len = strlen($text);
		$commas = 0;		
		$start = 0;
		for ($ix=0; $ix<$len; $ix++)
		{
			if (substr($text,$ix,1) == ',')
				$commas++;

			if ($commas == 1 && $start == 0)
				$start = $ix;
			if ($commas == 2)
				return substr($text, $start+1, $ix - $start - 1).' - ';
		}
		return "";
	}
	
	private function get_title($text)
	{
		$len = strlen($text);
		$commas = 0;		
		for ($ix=0; $ix<$len; $ix++)
		{
			if (substr($text,$ix,1) == ',')
				$commas++;

			if ($commas == 2)
				return substr($text, $ix + 1);
		}
		return "";
	}
	
	public function get_show_style()
	{
		if ($this->sho == 0)
			return 'visibility: hidden;';
		else
			return 'width: '.$this->wid.'px; height: '.$this->hgt.'px;';
	}

	public function get_gallery_style()
	{
		if ($this->gal == 0)
			return 'visibility: hidden;';
		else
			return 'width: '.$this->wid.'px; height: '.$this->hgt.'px;';
			
	}

	public function run()
	{
		$curr = 1;
		$last = count($this->files_arr);
		if (isset($_GET['img']))
		{
			if (preg_match('/^[0-9]+$/', $_GET['img'])) $curr = (int)  $_GET['img'];
			if ($curr <= 0 || $curr > $last) $curr = 1;
		}
		if ($curr <= 1)
		{
			$prev = $curr;
			$next = $curr + 1;
		}
		else if ($curr >= $last)
		{
			$prev = $last - 1;
			$next = $last;
		}
		else
		{
			$prev = $curr - 1;
			$next = $curr + 1;
		}
		
		// line below sets the caption name...
		$caption = str_replace('-', ' ', $this->files_arr[$curr - 1]);
		$caption = str_replace('_', ' ', $caption);
		$caption = preg_replace('/\.(jpe?g|gif|png)$/i', '', $caption);
		$caption = ucfirst($caption);

		$t1 = "?img=";
		$t2 = "&nam=".$this->name;


		return array($this->files_arr[$curr - 1], $caption,  $t1.'1'.$t2, $t1.$prev.$t2, $t1.$next.$t2, $t1.$last.$t2);
	}

	public function get_tables()
	{
		$last = count($this->files_arr);
		$rt = "";
		for ($i = 0; $i < $last; $i++) {
			$rt = $rt.$this->get_table($i);
		}
		return $rt;
	}

	public function get_table($ix)
	{
		$t1 = IMGDIR.$this->get_file($ix);
		$t2 = "name - ";
		$t3= "title";
		if (array_key_exists($ix, $this->title_a_arr))
		{
			$t2 = $this->title_a_arr[$ix];
			$t3 = $this->title_p_arr[$ix];
		}

		$cls = 'class="hidden"';
		if ($ix==0)
			$cls="";

		return '
	<div '.$cls.'>
    <table>
	<tr>
    <td >
     <img height="'.$this->get_height($ix).'%" style="margin-top:'.$this->get_margin($ix).'px; margin-left:'.$this->get_marginl($ix).'px;  " src="'.$t1.'">
     </td></tr>
     <tr>
     <td align="center"><p style="font-size: 16px;">'.$t2.'<i>'.$t3.'</i></p></td>
     </tr>
	 </table>
	</div>';

	}

	private function dir_exists()
	{
		return file_exists(IMGDIR);
	}

}

?>
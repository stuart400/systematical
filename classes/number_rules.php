<?php
class number_rules {
	
	var $rule = array();	

		// =========
		// The rule answers may be formatted:
		//
		//  Based on Selection Type Code:
		//
		//	  fractions: e.g.  "1/4"
		//		are also stored as: "1F4"
		//
		//	  surds: e.g.  "2√3"
		//		may also be stored as: "2S3"
		//
		//
		// =========
	
	function set_rule_answers($inputArray, $seltyp, $selnum, $randomNum) 
	{		
//		LogLine("XXXXX set_rule_answers seltype ", $seltyp);

		for($i=0;$i<count($inputArray); $i++)
		{			
			$this->rule[$i] = Math_Logic_Calc_Answer($inputArray[$i], $seltyp, $selnum, $randomNum);		

			LogLine("XXXXX set_rule_answers seltype:".$seltyp, $inputArray[$i]."<>".$this->rule[$i]);
		}
	}

	function get_rule_answers($func) {  
		
		if ($func=='1') // Blank them all
		{
			for($i=0;$i<count($this->rule); $i++)
			{
				$this->rule[$i] = ' ';
			}			
		}
		return $this->rule;   		
	}              
}

?>
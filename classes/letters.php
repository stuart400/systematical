<?php

require_once('BOM/Util.php');
require_once('BOM/Handler.php');
require_once('BOM/StreamFilter.php');

use duncan3dc\Bom\Util;

// TODO:
// NOT unicode.  Only works when letters exist in selected font.

class letters {

    var $arrayOfLettersIn = array();

    var $arrayOfLetters = array(
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.', '?', ',', '!',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        );

	function set_letters($questionString, $totalNum) {

        if (is_null($questionString) or mb_strlen($questionString) < 2)
            $questionString = "abcdefghijklmnopqrstuvwxyz.?,!";

        $scramble = 0;  // 0 for testing

        // Add the question characters to the array

        $string = Util::removeBom($questionString);

        $array = array();

        $strlen = mb_strlen($string); 
        while ($strlen) { 
            $array[] = mb_substr($string,0,1,"UTF-8"); 
            $string = mb_substr($string,1,$strlen,"UTF-8"); 
            $strlen = mb_strlen($string); 
        } 

        for($i=0;$i<Count($array); $i++)
		{
            $txt = $array[$i]; // mb_substr($questionString,$i,1);            

            if (is_null($txt) or $txt==' ' or	$txt == chr(10) or $txt == chr(13))
            {
                $txt=NULL;
            }
            else
            {
                if (!in_array($txt,$this->arrayOfLettersIn))
                {
                    $this->arrayOfLettersIn[] = $txt;
                    LogEntry("LETTER", $txt);
                }
            }
        }

        $ix = 5;
        while (Count($this->arrayOfLettersIn) < $totalNum) 
        { 
            $addNow = $this->arrayOfLetters[$ix];
            if (!in_array($addNow, $this->arrayOfLettersIn))
            {
                $this->arrayOfLettersIn[] = $addNow;
                LogEntry("LETTER2", $addNow);
            }
            $ix++;
        }


        // Pad out the array letters - from the old array

        $randomNumber = rand(100, 200);

        $lim = count($this->arrayOfLettersIn)-1;
        if ($lim < 30) // size of finished array
        {
            $lim2 = count($this->arrayOfLetters)-1;

            for($i=0;$i<$randomNumber; $i++){

                $this->arrayOfLettersIn[] = $this->arrayOfLetters[rand(0, $lim2 )];

                $lim = count($this->arrayOfLettersIn)-1;                

                if ($lim > 30)
                    break;
			}
        }


        // now Scramble them

        $lim2 = count($this->arrayOfLettersIn)-1;
        $randomNumber = rand(100, 200);

		for($i=0;$i<$randomNumber; $i++){
			$randomnum1 = rand(0, $lim2 );
			$randomnum2 = rand(0, $lim2 );

			$tempValue = $this->arrayOfLettersIn[$randomnum1];

			$this->arrayOfLettersIn[$randomnum1] = $this->arrayOfLettersIn[$randomnum2];

            if ($scramble==1)
                $this->arrayOfLettersIn[$randomnum2] = $tempValue;   // TO SCRAMBLE
            else
			    $this->arrayOfLettersIn[$randomnum1] = $tempValue;   // NOT TO SCRAMBLE
			}
	}

	function get_letters($func)
	{
		if ($func=='1')
		{
			for($i=0;$i<count($this->arrayOfLettersIn); $i++)
			{
				$this->arrayOfLettersIn[$i] = ' ';
			}
		}
		return $this->arrayOfLettersIn;
	}
}

?>
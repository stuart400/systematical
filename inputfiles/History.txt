﻿1. Which people first discovered and occupied Australia and how long ago did this occur? 
The Aborigines first arrived in Australia 40 thousand to 60 thousand years ago.

2. What are the roaring forties? What did they have to do with people from Europe discovering Australia?
They are stong winds, which travellers from Europe heading to the East Indies used.Australia was eventually sighted by ships traveling too far east on these “roaring forties”.  

3. Why were the Dutch explorers the first Europeans to sight and land in Australia? 
The Dutch had developed trading routes between Europe and the East Indies. Their shipping routes took them close to Australia. 

4. When was the first recorded European landfall on the Australian continent? Who was this explorer?
In February 1606, Willem Janszoon led a Dutch ship, the Duyfken to land on the western shore of Cape York in Queensland.

5. Explorers from which European country first charted Australia’s coast? What did they call this continent?
They were Dutch explorers and they referred to this area as New Holland.

6. Who was the first European who sighted Tasmania and what year did this occur? What did he call Tasmania?
Abel Tasman was the Dutch explorer who was the first European to sight Tasmania. This occurred in November 1642.He called it Van Diemen’s Land.

7. What was the first European name for Tasmania and when was it so named?
Tasmania was originally called Van Diemen’s Land in 1642 by the Dutch explorer, Abel Tasman.

8. What were the two main reasons for James Cook’s expedition to the Pacific ocean aboard the Endeavour?
He was to observe and make measurements of the 1769 transit of Venus across the Sun, in Tahiti. Such measurements would help scientists determine the distance between the sun and Earth. He was to also seek evidence of the unknown southern land, Terra Australis Incognita.

9. What was the name of the ship James Cook sailed to Australia and in what Australian bay did he first land?
The ship was called HMS Endeavour and it landed in Botany Bay.

10. At which Australian bay did James Cook first land his ship and what did Cook call this place?
It was Botany Bay, though Cook initially called it Sting-Ray Harbour.

11. What year did James Cook land his ship, HMS Endeavour in Australia? Where did he first land?
The year was 1770 and he landed in Botany Bay, which is in NSW.

12. What reasons did the English have in wanting to colonise in Australia?
To help expand their empire within the known world and improve their trading opportunities. With their prisons overflowing due to social issues caused by the Industrial Revolution, they used convict labour to start a colony.

13. How many ships were in the first fleet from England to Australia? What were the main group of passengers?
There were 11 ships and most of the passengers were convicts, shipped from England to help develop a colony in Australia.

14. What year did the first fleet from Great Britain start a colony in Australia? Where did the colony settle?
Although the first fleet arrived at Botany Bay in January 1788, they moved to Port Jackson on 26 January 1788 to start a colony at Sydney Cove. 

15. The date the first fleet landed in Port Jackson has a special name. What is the date and what is the name?
The date is 26 January 1788. The date of 26 January is the national day of Australia and is called Australia Day. It has also known as Invasion Day or Survival Day, particularly by those of Aboriginal heritage.

16. Which Englishman led the first fleet to Australia and what role did he then have when the colony started?
Arthur Philip was responsible for the first fleet arriving in Australia. He became the first governor of the colony of New South Wales?

17. When and where was gold first discovered in Australia? What happened as a result of this discovery?
Gold was first discovered in Bathurst, New South Wales in 1851. It caused a huge influx of new people to quickly emigrate to Australia in search of gold.

18. What was the name of the famous bushranger who dressed in armour of homemade plate metal? How did he die?
Ned Kelly was hung for his crimes in a Melbourne jail in November 1880.

19. Why did many early Australians approve of bushrangers?
They approved of what the bushrangers did, for they too hated the police and the rich land owners from who the bushrangers stole.

20. Who is known as the father of federation? When did Australia's colonies unite to become a federation?
Sir Henry Parkes is known as the father of federation. He pushed for the colonies to unite and become a single country under one federal government. The federation occurred on January 1, 1901.

21. What does the Australian National flag look like and what do the features of it represent?
The Australian flag comprises of the Union Jack, symbolic of our British heritage, the constellation of the Southern Cross, which can only be seen in the southern hemisphere and the seven pointed star which represents States and territories.

22. Between what years was the first world war fought and where did the ANZACs fight and become famous?
WW1 was fought between 1914 and 1918. The ANZACs fought in many places in Europe, but became famous in Gallipoli, Turkey for their bravery and resilience.

23. What is an ANZAC? What is the significance of ANZAC Day? Name two famous ANZACs.
An ANZAC is a member of the Australian and New Zealand Army Corp. In WW1 they were soldiers who landed at Gallipoli, Turkey on 25 April, 1915. Australians remember their soldiers killed in wars on this day.

24. Who was the famous Australian race horse that lived during the Great Depression? Where is his heart? 
Phar Lap was the famous race horse. His enormous heart is displayed at the National Museum. It is almost twice the size of a regular horse’s heart.

25. Who was the famous Australian cricketer of last century and what was his legendary batting average? 
Don Bradman's batting average was 99.94. The ABC uses the address of PO Box 9994 in every Australian capital city in admiration of his skill.

26. When did World war two occur? Who was the army surgeon who saved many soldiers held captive by the Japanese?
WW2 was fought between 1939-1945. Sir Edward “Weary” Dunlop took care of soldiers taken prisoner by the Japanese. He fought for their wellbeing and very often the lives of these men.

27. An Australian female athlete was known as the golden girl. Who was she and where did she win Olympic gold?
Betty Cuthbert won three gold in the 1956 Melbourne games and one medal at the 1964 Tokyo games.

28. Which war was the longest in duration of any war in Australia’s history? When was this involvement?
The Vietnam war. Australia was involved between 1962 and 1973.


29. What does the Aboriginal flag look like and what do the parts of it represent?
a rectangle divided in the middle with black top representing the Aboriginal people of Australia, red bottom representing the red earth and the Aboriginal people’s relationship to the land, and the central yellow circle, representing the sun, which is considered a protector and life giver.

30. Who was the indigenous man who successfully led the campaign to overturn British land title in Australia?
Koiko “Eddie” Mabo worked for ten years for native title rights. Unfortunately he died 5 months before the court announced its historic decision.



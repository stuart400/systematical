﻿1. What is the name of an angle measuring less than ninety degrees?
An acute angle.

2. In which area of maths are letters or symbols used in place of numbers?
Algebra

3. what is the name given to the amount of surface enclosed by a flat shape?
Area

4. what is the maximum volume of fluid that can be held by a container?
The capacity.

5. what is the name of the solid figure with a curved face and a circular base?
A cone

6. what is the name of the solid with six faces, all of which are identical squares?
A cube

7. what is the name of the solid with two circular faces and one curved rectangular face?
A cylinder

8. What is the name given to the dot which helps us see the place value of numbers?
The decimal point.

9. What is the number written below the line in a fraction? It lets us know how many equal parts make the whole.
The denominator.

10. Decreasing, or going from largest to smallest, is in what order?
Descending order.

11.What is the name of a triangle with all three sides and all three angles equal?
An equilateral triangle.

12. What is the name of a whole number that divides exactly into another number?
A factor.

13. What is the name of a polygon that has six sides? Which insect uses the shape when building their nest?
A hexagon and bees, as well as wasps use it when making their nest.

14. What is the name of a triangle in which two of the sides and two of the angles are equal?
An isosceles triangle.

15. What is the prefix that means a thousand? Write down three words which use this prefix.
Kilo.Examples include kilogram, kilometre, kilobyte and kilowatt.

16. If a shape is folded so that one half fits exactly on the other, then the shape shows what kind of symmetry?
Line symmetry.

17. What is the name of a number written as a whole number followed by a fraction? Write your age in this way.
A mixed number. Student to write their age in years like this.

18. What is the name of a flat pattern that can be folded to make a solid. Draw them for a cube and for a cone. 
A net

19. What is the number written above the line in a fraction? It lets us know how many parts of the whole we have.
The numerator.

20. What is the name of an angle that has more than ninety degrees but is less than a straight angle?
An obtuse angle.

21.What is the name of a polygon with eight sides? Which important road sign has this shape?
An octagon.The stop sign has this shape.

22. What is the name of a number or phrase that reads the same forward as backward? Write an example of each.
A palindrome. For example 21512, or the following phrase: A man, a plan, a canal, Panama.

23. What do we call lines which are on the same plane that do not intersect, like train tracks?
Parallel lines.

24. What is the name of a fraction out of a hundred called? Write some examples where these are used.
A percentage. In shops such as 20% discount, or in a spelling test, getting 90% correct.

25. What is the name of the distance around the outside of a shape? Measure this distance of your desk.
The perimeter. Student to also measure the perimeter of their desk.

26. What do we call lines that intersect at ninety degrees? Write down two examples where you see these.
Perpendicular lines, or lines at right angles. Examples include adjacent walls and picture frames.

27. What do we call those numbers that only have two factors, one and itself? What are the first five primes?
A prime number. The first five primes are 2, 3, 5, 7 and 11.

28. what is the mirror image of an object called? What capital letters look the same as their mirror image?
A reflection. The unchanged letters are A, H, I, M, O, T, U, V, W, X and Y.

29. What is the name of a triangle which has no equal sides or angles?
A scalene triangle.

30. what is the amount of space occupied by a solid?
The volume.

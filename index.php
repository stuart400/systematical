<!DOCTYPE html>
<html>
<head>
  <title>The Code Breaker</title>
    <link href="../css/960_24_col.css" rel="stylesheet" type="text/css" />
    <link href="../css/text.css" rel="stylesheet" type="text/css" />
    <link href="../css/codebreaker.css" rel="stylesheet" type="text/css" />
    <link href="../css/style.css" rel="stylesheet" type="text/css" />
</head>
<body>

<?php

include_once('classes/utility.php');
include_once('classes/controls.php');

require('classes/inputManager.php');

// phpinfo();  TEST EDIT for BitBucket

$more = get_parameter('action');
$runID = get_parameter('id');
$selectInfo = get_parameter('select');
$topicChosen = get_parameter('topicChosen');

$topicOrEnter = (!empty($_POST['upload']) );

$currentHeaderLinks = get_header_links("", $runID);
$currentHeaderText = get_header_text($more);

$footertext = get_footer_text();
$currentFileToView = "../files/EmptyWorksheet.pdf";

$groupParmPost = "";

$sequence = get_select_key_offset("groupparm");
$groupparm =  get_default_index($sequence, $selectInfo);



//if (empty(get_parameter('group')) )
//{
// // echo time();
//  phpinfo();
//  exit;
//}

$fileId = "0";

if (is_parameter('fileid'))
  $fileId = get_parameter('fileid');

$inpMng = new InputManager();

$inpMng->Start($runID);		

$inpMng->SetParametersFromGet($runID, $selectInfo, $topicOrEnter, $groupParmPost);

if ($runID > '9')
{
  $currentFileToView = get_view_worksheet($runID,true);
  $currentLink = get_print_worksheet_link($runID,true);
  $currentHeaderText = "";
  $errorInfo = "13";
}
else
{
  $currentLink = "";
  $errorInfo = $inpMng->IsGroupParmSet() ? 12 : 11;
}

// id default is 2 - Create Answer worksheet AND Student worksheet
// id passed to createpdf.php is also used to produce Check Sheets (try 11)
// See comments at the end of this file.

$defaultId = "2";
if (is_parameter('testid'))
  $defaultId = get_parameter('testid');

?>

<div id="content">
<div class="container_24">


<form id="indexform" action="createPDF.php<?php echo add_or_change_parameter('id', $defaultId)?>" method="post">

<div class="grid_5" style="margin-top: 10px;">

<table cellpadding="0" cellspacing="0">
<tr>
<td id="left1">
<table cellpadding="0" cellspacing="0">
<tr class = "tablerow1grade">
  <td align="left" valign="bottom">
   <?php echo grade_listbox('grade', $selectInfo, 0)?> 
  </td>
</tr>
<tr>
  <td align="left" valign="bottom">
  <?php echo group_listbox('groupparm', $selectInfo, 2)?>
  </td>
</tr>
<tr class = "tablerow3grade">
  <td align="left" valign="top">
   <?php echo students_listbox('student', $selectInfo, 4)?>
  </td>
</tr>
</table>

</td>
</tr>

<tr>
<td>
<p></p>
</td>
</tr>

<tr class="custom-tablestep2" >
<td id="left2" <?php echo $inpMng->GetTableRowCLass("left2")?> style="width: 100%;">


  <?php 
  if ($groupparm=="1") { ?>
  <table class="tablestep2" <?php echo $inpMng->GetTableRowStyle()?> >
    <?php echo GetQuestionTable($selectInfo)?>
  </table>
  <?php }
  if ($groupparm=="2") { 
    for ($i=1; $i <=6 ; $i++) { ?>
    <table class="tablestep2" id="<?php echo "step_2_".$i; ?>" data="<?php echo $i; ?>" <?php echo $inpMng->GetTableRowStyle($i)?>  >
      <?php if($i==1){ ?>
      <lable class="tableTitle">STEP 2</label>
      <?php } ?>
      <tr>
        <td colspan="2" >Selecting set : <?php echo $i; ?> of 6</td>
      </tr>
      <?php echo GetQuestionTable($selectInfo,$i)?>
      <tr>
        <?php if($i!=1){ ?>
          <td align="left" <?php if($i==6){ ?> colspan="2" <?php } ?> ><a class="previewDiv" data="<?php echo $i; ?>">Previous</a></td>
        <?php } ?>
        <?php if($i!=6){ ?>
          <td align="right" <?php if($i==1){ ?> colspan="2" <?php } ?> ><a class="nextDiv <?php if($i==1){ ?> nextDivFirst <?php } ?>" data="<?php echo $i; ?>">Next</a></td>
        <?php } ?>
      </tr>
    </table>
    <?php }?>
  <?php }  
  if ($groupparm=="3") { 

    for ($i=1; $i <=6 ; $i++) { ?>
    <table class="tablestep2" id="<?php echo "step_2_".$i; ?>" data="<?php echo $i; ?>" <?php echo $inpMng->GetTableRowStyle($i)?>  >
      <?php if($i==1){ ?>
      <lable class="tableTitle">STEP 2</label>
      <?php } ?>
      <tr>
        <td colspan="2">Selecting set : <?php echo $i; ?> of 6</td>
      </tr>
      <?php echo GetQuestionTable($selectInfo,$i)?>
      <tr>
        <?php if($i!=1){ ?>
          <td align="left" <?php if($i==6){ ?> colspan="2" <?php } ?> ><a class="previewDiv" data="<?php echo $i; ?>">Previous</a></td>
        <?php } ?>
        <?php if($i!=6){ ?>
          <td align="right" <?php if($i==1){ ?> colspan="2" <?php } ?> ><a class="nextDiv <?php if($i==1){ ?> nextDivFirst <?php } ?>" data="<?php echo $i; ?>">Next</a></td>
        <?php } ?>
      </tr>
    </table>
    <?php }?>
  <?php } ?>
</td>
</tr>

<tr>
<td>
<p></p>
</td>
</tr>

<tr>
<td id="left3" <?php echo $inpMng->GetTableRowCLass("left3")?>>
<table id="tablestep3" <?php echo $inpMng->GetTableRowStyle()?>>
<?php if($groupparm>0){ ?>
<lable class="tableTitle">STEP 3</label>
<tr>
  <td>Literacy level</td>
</tr>
<tr>
<td align="left" valign="center">
      <?php echo literacy_level('literacy', $selectInfo)?>
</td>
</tr>
<tr>
  <td>Choose from existing list</td>
</tr>
<tr>
<td align="left" valign="center">
   <?php echo topic_listbox('topic', $selectInfo)?>
</td>
</tr>
<tr>
  <td>OR Enter your messages and student Names </td>
</tr>
<tr>
  <?php if($topicChosen!="FileUpload"){ ?>
    <td align="left" valign="center" >
      <input style="padding-top: 3px; padding-bottom: 3px;" type="submit" name="upload" value="Upload Details" class="uploadsubmit">
    </td>
<?php } ?>
</tr>
<?php } ?>
</table>

</td>
</tr>

<tr>
<td>
<p></p>
</td>
</tr>

<tr>
<td id="left4">
<div class ="button">
<input type="submit" name="<?php if($topicChosen=="FileUpload"){ echo "submitQuestion"; }else{ echo "submitbtn"; } ?>" value="GO!" 
        style="margin-top: 3px; font-size: 27px; border: none; background: transparent; margin-left: 55px; font-weight: bold; font: arial">
</input>
</div>
</td>
</tr>

</table>
</div>
<div class="grid_19" style="margin-top: 15px;"  >

<?php echo $currentHeaderLinks; ?>


<?php echo get_errorMessage($errorInfo);?>
  
<?php echo $currentLink ?>
<?php if($topicChosen=="FileUpload"){ ?>
  <div id="content_pdf">
    <h1  style="width: 846px; margin-left: 2px; border:1px solid black;">
       Enter your questions here. Each question on a new line. Max line length 128 chars.
    </h1> 
    <fieldset>  
      <?php echo get_question_text_control($fileId, Utility::STANDARD_LINE_LENGTH) ?>
    </fieldset>
    <h1  style="width: 846px; margin-left: 2px; border:1px solid black;">
       Enter your Student Names here. Each name on a new line.
    </h1> 
    <fieldset>  
      <?php echo get_student_text_control($fileId) ?>                                
    </fieldset> 
  </div>
<?php  }else{ ?>

  <?php echo $currentHeaderText; ?>
  <div id="content_pdf">
    <iframe src="<?php echo $currentFileToView ?>" width="740" height="510" 
        frameborder="0" scrolling="no" marginheight="30px" marginwidth="30px">
      <p>Your browser does not support iframes.</p>
    </iframe>
  </div>


<?php } ?>
<?php echo $footertext ?>
</div>

</form>

</div>
</div>

<script src="js/jquery.min.js"></script>


</body>
</html>

<script>
function myFunction() {

    var x = document.getElementById("groupparm");
    if (x != null )
    {  
        document.forms["indexform"].submit();    
    }
}

$(document).ready(function() {

  $('body').on('change','#primary_selection',function() {

      var primary_selection=$(this).val();
      var current=$(this);
      current.parent().parent().parent().find('.subOperationalRowvalue').hide();

      current.parent().parent().parent().find('#hiddenOperationField').remove();
      current.parent().parent().parent().find('#hiddenAbstractField').remove();

     
      $.ajax({

        url:'ajax.php',
        data:'type=getSubPrimarySelection&primary_selection='+primary_selection,
        type:'POST',
        success:function(data) {
          
          current.parent().parent().parent().find('.secondOperation').remove();
          current.parent().parent().parent().find('.abstractSelect').remove();
          current.parent().parent().parent().find('.abstractSelectTitle').remove();
          current.parent().parent().parent().find('.secondOperationTitle').remove();
        
          current.parent().parent().parent().find('.OperationRowValue').html(data);
          current.parent().parent().parent().find('.subOperationalRowvalue').empty();
        
        }
      });
  });


  $('body').on('change','#operation',function() {

    var current=$(this);
    var primary_selection=current.parent().parent().parent().parent().find('#primary_selection').val();
    var operation=current.val();
    
    current.parent().parent().parent().find('#hiddenOperationField').remove();
    current.parent().parent().parent().find('#hiddenAbstractField').remove();

    current.parent().parent().parent().parent().find('.subOperationalRowvalue').empty();
    current.parent().parent().parent().parent().find('.secondOperation').remove();
    current.parent().parent().parent().parent().find('.abstractSelect').remove();
    current.parent().parent().parent().parent().find('.abstractSelectTitle').remove();
    current.parent().parent().parent().parent().find('.secondOperationTitle').remove();
    if (primary_selection=="00" && operation=="00") {
    }
    else if(primary_selection=='03' ||primary_selection=='04'||primary_selection=='05')
    {
    }
    else {
        $.ajax({
          url:'ajax.php',
          data:'type=getSelectionFromOperation&primary_selection='+primary_selection+'&operation='+operation,
          type:'POST',
          success:function(data) {
            current.parent().parent().parent().parent().find('.subOperationalRowvalue').show();
            current.parent().parent().parent().parent().find('.subOperationalRowvalue').html(data);
          }
        });
    }

  });

  $('body').on('change','#secondary_selection',function(e) {

    var secondary_selection=$(this).val();
    var current=$(this);

      current.parent().parent().parent().find('.secondOperation').remove();
      current.parent().parent().parent().find('.abstractSelect').remove();
      current.parent().parent().parent().find('.abstractSelectTitle').remove();
      current.parent().parent().parent().find('.secondOperationTitle').remove();

        $.ajax({
          url:'ajax.php',
          data:'type=getSelectionFromSecondarySelection&primary_selection='+secondary_selection,
          type:'POST',
          success:function(data) {
            current.parent().parent().parent().find('.OperationRowValueSecondary').html(data);
            
          }

        });
  });

  $('body').on('change','#operation_secondary',function() {

    var current=$(this);
    var secondary_selection=current.parent().parent().parent().find('#secondary_selection').val();
    var operation_secondary=current.val();

      current.parent().parent().parent().find('.secondOperation').remove();
      current.parent().parent().parent().find('.abstractSelect').remove();
      current.parent().parent().parent().find('.abstractSelectTitle').remove();
      current.parent().parent().parent().find('.secondOperationTitle').remove();

        $.ajax({
          url:'ajax.php',
          data:'type=getSelectionFromSecondaryOptional&primary_selection='+secondary_selection+'&operation_secondary='+operation_secondary,
          type:'POST',
          success:function(data) {
            current.parent().parent().parent().find('.subOperationalRowvalueSecondary').html(data);
            
          }

        });
  });

  $('body').on('click','.nextDiv',function() {

      var current=$(this);
      var current_table=current.attr('data');
      var table_id='#step_2_'+current_table;
      ++current_table;
      var next_table_id='#step_2_'+current_table;
      $(table_id).hide();

      current.parent().parent().parent().find('#hiddenOperationField').remove();
      current.parent().parent().parent().find('#hiddenAbstractField').remove();

      $(next_table_id).show();
  });

  $('body').on('click','.previewDiv',function() {

      var current=$(this);
      var current_table=current.attr('data');
      var table_id='#step_2_'+current_table;
      --current_table;
      var next_table_id='#step_2_'+current_table;
      $(table_id).hide();

      current.parent().parent().parent().find('#hiddenOperationField').remove();
      current.parent().parent().parent().find('#hiddenAbstractField').remove();
      $(next_table_id).show();
  });



});

</script>

<?php

// =========================================================
// Example for TEST Sheets: Showing usage of Select=Parms
// =========================================================

  // if ($runID == "11")  // Early primary settings - ALL
  // {
  // 	 $tests = 16;
	//   $testIx = 11;
  // }
	// else if ($runID == "210") // Primary = One Op
	// {
	// 	$tests = 12;
	// 	$testIx = 2101;
	// }
	// else if ($runID == "221") // Primary = Two Op - Mult
	// {
	// 	$tests = 12;  // 6 x 01 and 6 x 02. e.g.
	// 	$testIx = 220101; // 22010101, 22010201, etc to 22010601. Same for 02 options: 22010102 etc
	// }
	// else if ($runID == "222") // Primary = Two Op - Div
	// {
	// 	$tests = 12;
	// 	$testIx = 220201;
	// }
	// else if ($runID == "223") // Primary = Two Op - SQUARING
	// {
	// 	$tests = 12;
	// 	$testIx = 220301;
	// }
	// else if ($runID == "230") // Primary = Fractions
	// {
	// 	$tests = 6;
	// 	$testIx = 2301;
	// }
	// else if ($runID == "240") // Primary = decimals
	// {
	// 	$tests = 6;
	// 	$testIx = 2401; // e.g. 2401, 2402, etc to 2406
	// }
	// else if ($runID == "250") // Primary = Percentages
	// {
	// 	$tests = 6;
	// 	$testIx = 2501;
	// }

	// // Secondary
	// else if ($runID == "311") // Secondary = INTEGERS + MULT
	// {
	// 	$tests = 12;
	// 	$testIx = 310101;
	// }
	// else if ($runID == "312") // Secondary = INTEGERS + DIV
	// {
	// 	$tests = 12;
	// 	$testIx = 310201;
	// }
	// else if ($runID == "313") // Secondary = INTEGERS + squaring or square-rooting
	// {
	// 	$tests = 12;
	// 	$testIx = 310301;
	// }
	// else if ($runID == "320") // Secondary = FRACTIONS
	// {
	// 	$tests = 6;
	// 	$testIx = 3201;
	// }
	// else if ($runID == "330") // Secondary = DECIMALS
	// {
	// 	$tests = 6;
	// 	$testIx = 3301;
	// }
	// else if ($runID == "340") // Secondary = Percentages
	// {
	// 	$tests = 6;
	// 	$testIx = 3401;
	// }
	// else if ($runID == "350") // Secondary = SURDS
	// {
	// 	$tests = 6;
  // 	$testIx = 3501;
  // }

 ?>

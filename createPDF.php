<?php

require('tools/tfpdf.php');
require('tools/pdfManager.php');

require('classes/inputManager.php');

require_once('classes/BOM/Util.php');
require_once('classes/BOM/Handler.php');
require_once('classes/BOM/StreamFilter.php');

include_once('classes/utility.php');
include_once('classes/math_logic.php');
include_once('classes/math_logic_surds.php');
include_once('classes/number_set.php');
include_once('classes/number_rules.php');
include_once('classes/letters.php');
include_once('classes/combine_letters_nums.php');
include_once('classes/letter_num_pair.php');
include_once('classes/text.php');
include_once('classes/controls.php');

use duncan3dc\Bom\Util;

// **********************************************************
//	Setup the Page
// **********************************************************
$runID = get_parameter('id');
$fileID = get_parameter('fileid');
$select = get_parameter('select');

$debug = false;

$processedUpload = "no";

LogStart(" createpdf ".$select );

$system_timestamp =  time();

$lastfile = "";
$lastFileNumber = "1000";

$uploadedheadingfile = "";  // not using this

$uploadednamesfile = "";  // names uploaded
$uploadedfile = ""; // questions uploaded

$inpMng = new InputManager();
$inpMng->Start($runID);

$topicOrEnter = (!empty($_POST['upload']) );
$topicChosen = get_parameter("topicChosen") ;
if (!empty ($topicChosen))
	$topicOrEnter = true;


//echo "<pre>";
//print_r($_POST);
//exit;


$reloadNumbers =  empty($_POST['submitbtn']) && empty($_POST['submitQuestion']) && empty($_POST['upload']);

// First Set the defaults from the select
$inpMng->SetParametersFromGet($runID, get_parameter("select"), false);

// Now apply the POST updates
$inpMng->SetParametersFromPost($runID, get_post_parameters(), $topicOrEnter, $reloadNumbers);

//if ($reloadNumbers)
//{
//    echo "<pre>";
//    print_r($_POST);
//    exit;

//}

// **********************************************************
//	Branch to Upload files if required
// **********************************************************

$user_selections = $inpMng->GetSelectionsQueryString();

LogLine("AT START (CREATE PDF) - Selections:", $user_selections );

$topicChosen = get_parameter("topicChosen") ;

if (!empty($_POST['submitQuestion']))
{
	$question = $_POST['question'];
	if(!empty($question) && !ctype_cntrl($question))
	{
		$result = WriteQuestionAnswersFile ($system_timestamp, $question, "question");

		$validUpload = VetQuestionAnswersFile ($system_timestamp, $question, "question", 160); // TODO

		if ($result == 5) {
			header('Location: index.php?id='.$runID.'&select='.$user_selections.'&topicChosen='.$topicChosen.'&error='.$result);
			exit;
		}
		else
        {
			$uploadedfile = $result;
            $runID = 9999;  // Ok process uploaded data
        }
	}

	$studentnames = $_POST['studentnames'];
	if(!empty($studentnames) && !ctype_cntrl($studentnames))
	{
		$result = WriteQuestionAnswersFile($system_timestamp, $studentnames, "name");
		if ($result != 0) {
			header('Location: index.php?id='.$runID.'&select='.$user_selections.'&topicChosen='.$topicChosen.'&error='.$result);
			exit;
		}
		else
		{
			$uploadednamesfile = $result;
		}
	}

    //$result = UploadFile($system_timestamp);
    //if ($result != 0)
    //{
    //}
    //else
    //{
    //    $uploadedheadingfile = 'a'.$system_timestamp.$_FILES['uploaded_file']['name'];
    //    // also in $upload_path
    //}
}

if (!empty($_POST['morehelp']))
{
	$topicChosen = "morehelp";
    LogLine("(CREATE PDF) - ", $topicChosen  );
	header('Location: index.php?id='.$runID.'&select='.$user_selections.'&action='.$topicChosen);
	exit;
}

// Not A test run- No valid selections yet
if ($runID < 10 &&
	(!$inpMng->IsGroupParmSet() || (empty($_POST['upload']) && empty($_POST['submitbtn'])) ))
{
	$topicChosen = "no valid selections";
    LogLine("(CREATE PDF) - ", $topicChosen  );
	header('Location: index.php?select='.$user_selections);
	exit;
}

//echo "<pre>";
//print_r($_POST);
//exit;

if (!empty($_POST['upload'])) {

	$topicChosen = "FileUpload";
    LogLine("(CREATE PDF) - ", $topicChosen  );
	header('Location: index.php?id='.$runID.'&select='.$user_selections.'&topicChosen='.$topicChosen);
	exit;
}

$system_timestamp = $_SERVER['REQUEST_TIME'];


$validUpload = 1; // ok

//phpinfo();
//exit;

if (!empty($_POST['submitOriginal']))
{//do something here;
}

if ($validUpload == 0)
{
	$topicChosen = "FileUpload2";
    LogLine("(CREATE PDF) - ", $topicChosen  );
	header('Location: index.php?id='.$runID.'&select='.$user_selections.'&topicChosen='.$topicChosen.'&fileid='.$system_timestamp);
	exit;
}
// **********************************************************
//	Produce the files
// **********************************************************

$inpMng->SetNames($uploadednamesfile);

while (true)
{
	if ($debug && $runID=='3')
	{
		$user_selections = $inpMng->GetSelectionsQueryString();
		header('Location: index.php?id='.$lastFileNumber.'&select='.$user_selections);
		break;
	}

	LogLine("START runid", $runID );

    $pdf = new pdfManager('L','mm','A4');

	$grade = '06';
	$setQty = $inpMng->IsEarlyPrimary() ? 3 : 6;
	$setLen = 5;

    if ($runID == 9999)
        $runID = 2;

	if ($runID > 10 ) // TestCases
	{
		PerformTestCases($pdf, $inpMng, $runID, $grade, $setQty, $setLen);
		break;
	}

	$inpMng->CondenseAllInputs();

	$inpMng->SetRandomNumbers($setQty);  // Top line Random Nums

	$inpMng->ChooseAbstractCharacters();

	$pdf->SetInputFieldsforHeaders(
		$inpMng->inputSelectArray,
		$inpMng->inputOfArray,
		$inpMng->randomNumArray,
		$inpMng->inputAbstractCharacterArray,
		$setQty);

	//	END INPUT TEXT MESSAGE LINES
	// **********************************************************

	$QuestionAnswerArray = $inpMng->GetQuestionsAnswers($uploadedfile, $inpMng->GetNoOfPages());

	$pdf->SetGrade($inpMng->GetGrade());

	if ($runID=='1')  // Produce a blank worksheet
	{
		$pdf->StartNewPage($runID, $inpMng->GetPageNumber() , "Student: . . . . ", $uploadedheadingfile, $setQty, $setLen);

		$pdf->PrintWorksheetPage($QuestionAnswerArray,
			$inpMng->inputSelectArray,
			$inpMng->inputOfArray,
			$inpMng->randomNumArray,
			$inpMng->inputAbstractCharacterArray);

		$pdf->Output("files\EmptyWorksheet2.pdf", 'F');
		$pdf->Close();
		break;
	}
	else if ($runID=='2')   // Produce a viewable worksheet (Show answers. No Summary Page)
	{
        while ($inpMng->IsMorePages() == true)
        {
			$pdf->StartNewPage($runID, $inpMng->GetPageNumber(), $inpMng->GetStudentName(), $uploadedheadingfile, $setQty, $setLen);

			$pdf->PrintWorksheetPage($QuestionAnswerArray,
				$inpMng->inputSelectArray,
				$inpMng->inputOfArray,
				$inpMng->randomNumArray,
				$inpMng->inputAbstractCharacterArray);
        }

        cleanup_worksheets();

        $lastFileNumber = get_worksheet_number()+1;
        $lastfile = get_view_worksheet($lastFileNumber,false);

		$pdf->Output($lastfile, 'F');
		$pdf->Close();

		$runID='3';
		$inpMng->ResetParameters($runID);

		$inpMng->RestoreAllInputs();
		continue;
	}
	else if ($runID=='3')  // Produce a printable worksheet (NO Answers. YES, Create a Summary Page)
	{
		$pdf->PrintSummaryPage(
				$inpMng->GetNoOfPages(), $QuestionAnswerArray,
				$inpMng->namesArray, $uploadedheadingfile);

		while ($inpMng->IsMorePages() == true)
		{
			$pdf->StartNewPage($runID, $inpMng->GetPageNumber(), $inpMng->GetStudentName(), $uploadedheadingfile, $setQty, $setLen);

			$pdf->PrintWorksheetPage($QuestionAnswerArray,
				$inpMng->inputSelectArray,
				$inpMng->inputOfArray,
				$inpMng->randomNumArray,
				$inpMng->inputAbstractCharacterArray);
		}

		$lastPrintfile = get_print_worksheet($lastFileNumber,false);
		$pdf->Output($lastPrintfile, 'F');
		$pdf->Close();

		$inpMng->RestoreAllInputs();

		$user_selections = $inpMng->GetSelectionsQueryString();
		header('Location: index.php?id='.$lastFileNumber.'&select='.$user_selections);
		break;
	}
}

function VetQuestionAnswersFile($system_timestamp,  $question, $runtype, $validLen)
{
	$data = GetFileText($system_timestamp, $runtype);

	foreach ($data as $line_num => $line) {

		$line = Util::removeBom($line);

		if (!is_null($line) && mb_strlen($line) > 2)
		{
			$line = StripLeadingNumber($line);

			if (mb_strlen($line) > $validLen)
				return 0;

			// Check for a word break;
			if (mb_strlen($line) > $validLen / 2 )
			{
				$befLen = GetCharacterLength($line);
				$parts = formatQuestions ($line);
				$afterLen = GetCharacterLengthArray($parts);

				LogLine("XXX-LEN-TEST-XXX".$befLen, $afterLen);

				return ($befLen > $afterLen ? 0 : 1);
			}
		}
	}

	return 1;
}

function WriteQuestionAnswersFile($system_timestamp,  $question, $runtype){

	$doublequote = "\"";
	$singlequote = "'";
	$backslash = "\\";

	$doublequotebs = $backslash.$doublequote;
	$singlequotebs = $backslash.$singlequote;
	$backslashbs = $backslash.$backslash;

	$question = str_replace($doublequotebs,$doublequote,$question);
	$question = str_replace($singlequotebs,$singlequote,$question);
	$question = str_replace($backslashbs,$backslash,$question);

	$curDir = getcwd();
	$myFile = $curDir.'/uploads/'.$system_timestamp.$runtype.'_up.txt';

    $fh = fopen($myFile, 'w');

                      //added code
	$savepos = 0;
	$lineno = 0;
	$n = 1;
	$h = 0;
	for ($h=0;$h<mb_strlen($question); $h=$h+1)
	{
		$temp[$h] = mb_substr($question,$h,1);
	//	if ($temp[$h] == chr(10) or $temp[$h] == chr(13)) {
		if ($temp[$h] == chr(10)) {
			$questionline[$lineno] = mb_substr($question,$savepos,$h - $savepos);
			$stringData = $questionline[$lineno].PHP_EOL;

            fwrite($fh, $stringData);  //write the question

			if ($runtype=="question")
			{
			//	$stringData = "Answer".$n.PHP_EOL;
			//	$n = $n + 1;   //testing
			//	fwrite($fh, $stringData);  //write the answer
			}

			$savepos = $h + 1;
			$lineno = $lineno + 1;
		}
		//if ($temp[$h] == chr(13)) {$d = "carriage return";}
	}
	$stringData = mb_substr($question,$savepos,$h - $savepos).PHP_EOL;

    fwrite($fh, $stringData);  //write the question

	if ($runtype=="question")
	{
	//	$stringData = "Answer".$n.PHP_EOL;
	//	fwrite($fh, $stringData);  //write the answer
	}

//not sure if next 3 lines are needed
	$lastFileNumber = get_parameter('fileid');
	$runID = get_parameter('id');
	$user_selections = get_parameter('select');

    fwrite($fh, "\xEF\xBB\xBF" . "\r\n");  // force UTF8
	fclose($fh);

	return $myFile;
};

function UploadFile($system_timestamp){
	//$allowed_filetypes = array('.txt','.doc'); // These will be the types of file that will pass the validation.

	$allowed_filetypes = array('.png', '.gif', '.giff', '.jpg', '.tiff', '.tif', '.bmp');

	$max_filesize = 5242880; // Maximum filesize in BYTES (currently 05MB).

	$filename = $_FILES['uploaded_file']['name'];
	$old = getcwd();

	$ext = strtolower( substr($filename, strpos($filename,'.'), strlen($filename)-1));


	// Check if the filetype is allowed, if not inform the user.
	if($filename == "")
		return 1;

	if(!in_array($ext,$allowed_filetypes))
		return 2;

	// Now check the filesize, if it is too large inform the user.
	if(filesize($_FILES['uploaded_file']['tmp_name']) > $max_filesize)
		return 3;

	// ADD TIMESTAMP TO THE FILE NAME
	$filename = 'a'.$system_timestamp.$filename;
	// The place the files will be uploaded to (currently a 'files' directory).
	$upload_path = $old.'/uploads/'. $filename;
	if (move_uploaded_file($_FILES['uploaded_file']['tmp_name'], $upload_path)) {

		return 0;
	}

	return 4;
};

function PerformTestCases($pdf, $inpMng, $runID, $grade, $setQty, $setLen)
{
	$inpMng->SetDefaults(true);

	$user_selections = "010101000000000000000000000000000000000000000099";

	// First Set the defaults from the select
	$inpMng->SetParametersFromGet($runID, $user_selections, false);

	LogStart("START TEST CREATE PDF", $user_selections );

	$inpMng->SetNames("");

	$GLOBALS['testcase'] = true; // Show codes

	$grp = "0".substr($runID,0,1);

	// if ($runID == "11")  // Early primary settings - ALL
	$tests = 16;
	$testIx = $runID;

	if ($runID == "21" || $runID == "210") // Primary = One Op
	{
		$tests = 12;
		$testIx = 2101;
	}
	else if ($runID == "221") // Primary = Two Op - Mult
	{
		$tests = 12;  // 6 x 01 and 6 x 02. e.g.
		$testIx = 220101; // 22010101, 22010201, etc to 22010601. Same for 02 options: 22010102 etc
	}
	else if ($runID == "222") // Primary = Two Op - Div
	{
		$tests = 12;
		$testIx = 220201;
	}
	else if ($runID == "223") // Primary = Two Op - SQUARING
	{
		$tests = 12;
		$testIx = 220301;
	}
	else if ($runID == "230") // Primary = Fractions
	{
		$tests = 6;
		$testIx = 2301;
	}
	else if ($runID == "240") // Primary = decimals
	{
		$tests = 6;
		$testIx = 2401; // e.g. 2401, 2402, etc to 2406
	}
	else if ($runID == "250") // Primary = Percentages
	{
		$tests = 6;
		$testIx = 2501;
	}

	// Secondary
	else if ($runID == "311") // Secondary = INTEGERS + MULT
	{
		$tests = 12;
		$testIx = 310101;
	}
	else if ($runID == "312") // Secondary = INTEGERS + DIV
	{
		$tests = 12;
		$testIx = 310201;
	}
	else if ($runID == "313") // Secondary = INTEGERS + squaring or square-rooting
	{
		$tests = 12;
		$testIx = 310301;
	}
	else if ($runID == "320") // Secondary = FRACTIONS
	{
		$tests = 6;
		$testIx = 3201;
	}
	else if ($runID == "330") // Secondary = DECIMALS
	{
		$tests = 6;
		$testIx = 3301;
	}
	else if ($runID == "340") // Secondary = Percentages
	{
		$tests = 6;
		$testIx = 3401;
	}
	else if ($runID == "350") // Secondary = SURDS
	{
		$tests = 6;
		$testIx = 3501;
	}

	$inpMng->topicName = "0";
	$inpMng->groupparm = $grp;

	$TestFile = "-Check-Sheet: ".$runID." ".($grp=="01" ? "-early-primary" : ($grp=="02" ? "primary" : "Secondary"));

	$QuestionAnswerArray = $inpMng->GetQuestionsAnswers("", $inpMng->GetNoOfPages());

	$pdf->SetGrade($grade);

	// Produce a viewable worksheet (Show answers.)
	// Notes on Class Changes:
	// Bug in : SetTestRandomValues($ix)
	// Change: SetTestRandomValues($ix) so that only inputSelectArray is used by all groups

	$newPage = true;
	$lineIndex = 0;

	for ($i = 0; $i < $tests  ; $i++)
	{
		if ($newPage)
		{
			$pdf->StartNewPage($runID, $inpMng->GetPageNumber(), "-TEST - SHEET -", "", $setQty, $setLen);
			$lineIndex = 0;
			$newPage = false;

			$pdf->SetY(12);
			$pdf->SetX(100);
			$pdf->Cell(100, 10 ,"-- TEST -- ".$TestFile." -- TEST -- " ,0,0,'C');
		}

		$inpMng->ChooseAbstractCharacters();

		$inpMng->SetTestRandomValues($testIx); // look out for bug here

		for ($iy = 0; $iy < 3  ; $iy++)
		{
			LogLine("Sel:", $inpMng->inputSelectArray[$iy]);
			LogLine("SelOf:", $inpMng->inputOfArray[$iy]);
		}

		$pdf->SetInputFieldsforHeaders(
			$inpMng->inputSelectArray,
			$inpMng->inputOfArray,
			$inpMng->randomNumArray,
			$inpMng->inputAbstractCharacterArray,
			$setQty);

		$pdf->PrintWorksheetPageMathsTest($QuestionAnswerArray,
			$inpMng->inputSelectArray,
			$inpMng->inputOfArray,
			$inpMng->randomNumArray,
			$inpMng->inputAbstractCharacterArray,
			$lineIndex);

		if (($i+1) % 4 == 0 )
			$newPage = true;

		$lineIndex++;
		$testIx++;
	}

	cleanup_worksheets();

	$lastFileNumber = get_worksheet_number()+1;
	$lastfile = get_view_worksheet($lastFileNumber,false);

	$pdf->Output($lastfile, 'F');
	$pdf->Close();

	$inpMng->ResetParameters($runID);

	header('Location: index.php?id='.$lastFileNumber.'&select=0000000000');

};

?>
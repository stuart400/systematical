<?php 

include 'classes/controls.php';
$type=$_REQUEST['type'];

if ($type=='getSubPrimarySelection') {
	
	echo getOperationalData("operation",'',0,true,$_REQUEST['primary_selection']);
}
if ($type=='getSelectionFromOperation') {
	echo getOperationalSelectionData('operation_2','',0,true,$_REQUEST['primary_selection'],true,$_REQUEST['operation']);
}
if ($type=="getSelectionFromSecondarySelection") {
	echo getSecondaryOperationalData('operation','',0,true,$_REQUEST['primary_selection']);
}
if ($type=="getSelectionFromSecondaryOptional") {
	echo getOperationalSelectionDataSecondary('operation_2','',0,true,$_REQUEST['primary_selection'],true,$_REQUEST['operation_secondary']);
}

?>